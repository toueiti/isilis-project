<?php

namespace AgenceBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;


class FilterCampaignType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('banque', EntityType::class, array(
            'class' => 'BackBundle:Bank',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('b')
                    ->where('b.active = 1');
            },
            'choice_label' => 'name',
            'placeholder' => 'Nom de la banque'
        ));
    }

    public function gatName()
    {
        return 'filter_campaign_form';
    }
}