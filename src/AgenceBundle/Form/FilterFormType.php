<?php

namespace AgenceBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FilterFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bank', EntityType::class, array(
                'class' => 'BackBundle:Bank',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('b')
                        ->where('b.active = 1');
                },
                'choice_label' => 'name',
                'placeholder' => 'Nom de la banque',
            ))
            ->add('caisse', EntityType::class, array(
                'class' => 'BackBundle:Caisse',
                'query_builder' => function (EntityRepository $er) use ( $options ) {
                    return $er->createQueryBuilder('c')
                        ->where('c.active = 1')
                        ->andWhere('c.agence = '.$options['data']['agence']);
                },
                'choice_label' => 'name',
                'placeholder' => 'Nom de la caisse'
            ));
        // ->add('annee', ChoiceType::class,
        //  array(
        // 'choices' => $options['data']['years'],
        //'placeholder' => 'année',
        //  'required' => FALSE)
        //)
    }

    public function gatName()
    {
        return 'filter_form_customer';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'years' => array(),
            'agence' => array()
        ));
    }
}