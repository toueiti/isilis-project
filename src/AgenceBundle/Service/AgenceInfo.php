<?php

namespace AgenceBundle\Service;

/**
 * Description of AgenceInfo
 *
 * @author hp
 */
class AgenceInfo 
{
    protected $security;


    public function __construct($security) {
        $this->security = $security;
    }
    
    public function getAgence()
    {
        $token = $this->security->getToken();
        $Roles = $token->getUser()->getRoles();
        if (in_array("ROLE_AGENCE_COLLABORATEUR", $Roles)){
            $user = $token->getUser();
            $collab = $user->getAgenceCollaborateur();
            return $collab->getAgence();
        }
        $contact = $token->getUser()->getAgenceContact();
        return $contact->getAgence();
    }
    
    public function getMessagesByAgence($agence, $em) {
        $result = $em->getRepository(\BackBundle\Entity\Message::class)->getMessagesByAgence($agence->getId());
        $output = array();
        $nbmsg = 0;
        foreach ($result as $item){
            $row = array(
                'operation'=>$item[0],
                'nbmsg'=>$item['nbmsg']
            );
            $nbmsg += $item['nbmsg'];
            $output [] = $row;
        }
        return array(
            'operations'=>$output,
            'nbmsg'=>$nbmsg
        );
    }
}
