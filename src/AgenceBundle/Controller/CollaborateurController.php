<?php

namespace AgenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\AgenceCollaborateur;
use UserBundle\Form\AgenceCollaborateurType;

class CollaborateurController extends Controller
{

    /**
     * @Route("/collaborateur", name="agence_collaborateur")
     */
    public function indexAction()
    {
        $em             = $this->getDoctrine()->getManager();
        $agence         = $this->get('agence.agence.info')->getAgence();
        $collaborateurs = $em->getRepository(AgenceCollaborateur::class)->findByAgence($agence);
        return $this->render('agence/collaborateur/index.html.twig',
                array(
                'collaborateurs' => $collaborateurs
        ));
    }

    /**
     * @Route("/collaborateur/new", name="agence_collaborateur_new")
     */
    public function newAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $agence = $this->get('agence.agence.info')->getAgence();
        $collab = new AgenceCollaborateur();
        $collab->setAgence($agence);
        $form   = $this->createForm(AgenceCollaborateurType::class, $collab);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em        = $this->getDoctrine()->getManager();
                $user      = $collab->getUser();
                $validator = $this->get('validator');
                $errors    = $validator->validate($user);
                if (count($errors) > 0) {
                    $this->get('session')->getFlashBag()->add('error',
                        'Email déjà utilisé !');
                } else {
                    $user->setUsername($user->getEmail());
                    $password = $this->get('utils.utils')->generatePassword(8);
                    $user->setPlainPassword($password);
                    $user->setEnabled(true);
                    $user->addRole('ROLE_AGENCE');
                    $user->addRole('ROLE_AGENCE_COLLABORATEUR');
                    try {
                        $em->persist($user);
                        $collab->setUser($user);
                        $em->persist($collab);
                        $em->flush();
                    } catch (Exception $exc) {
                        echo $exc->getTraceAsString();
                    }
                    /*                     * *************** Email send ********************* */
                    $message = (new \Swift_Message('ISILIS Plateforme'))
                        ->setFrom($this->container->getParameter('mailer_from'))
                        ->setTo($user->getEmail())
                        ->setBcc('toueiti@yahoo.fr')
                        ->setBody(
                        $this->container->get('templating')->render(
                            'back/email/contact_registre.twig',
                            array('user' => $user, 'password' => $password, 'contact' => $collab)
                        ), 'text/html'
                    );
                    $this->container->get('mailer')->send($message);
                    /*                     * ******************** end email ****************** */
                    $this->get('session')->getFlashBag()->add('success',
                        'Collaborateur ajoutée avec succès.');
                    return $this->redirect($this->generateUrl('agence_collaborateur'));
                }
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('agence/collaborateur/edit.html.twig',
                array(
                'form' => $form->createView(),
                'collab' => $collab
        ));
    }

    /**
     * @Route("/collaborateur/edit/{id}", name="agence_collaborateur_edit")
     */
    public function editAction(Request $request, AgenceCollaborateur $collab)
    {
        $em     = $this->getDoctrine()->getManager();
        $agence = $this->get('agence.agence.info')->getAgence();
        $collab->setAgence($agence);
        $form   = $this->createForm(AgenceCollaborateurType::class, $collab);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    $em->persist($collab);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success',
                        'Collaborateur mis à jour avec succès.');
                    return $this->redirect($this->generateUrl('agence_collaborateur'));
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('agence/collaborateur/edit.html.twig',
                array(
                'form' => $form->createView(),
                'collab' => $collab
        ));
    }

    /**
     * @Route("/collaborateur/view/{id}", name="agence_collaborateur_view")
     */
    public function viewAction(Request $request, AgenceCollaborateur $collab)
    {

        return $this->render('agence/collaborateur/view.html.twig',
                array(
                'collab' => $collab
        ));
    }

    /**
     * @Route("/collaborateur/update/{id}", name="agence_collaborateur_update")
     */
    public function updateAction(AgenceCollaborateur $collab)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $collab->getUser();
        $user->setEnabled(!$user->isEnabled());
        $em->persist($user);
        $em->flush();
        return $this->redirect($this->generateUrl('agence_collaborateur'));
    }

    /**
     * @Route("/collaborateur/delete/{id}", name="agence_collaborateur_delete")
     */
    public function deleteAction(AgenceCollaborateur $collab)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $collab->getUser();
        $em->remove($collab);
        //$em->remove($user);
        $em->flush();
        return $this->redirect($this->generateUrl('agence_collaborateur'));
    }
}