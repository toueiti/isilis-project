<?php

namespace AgenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AgenceBundle\Form\searchFormType;

/**
 * @Route("/search")
 */
class SearchController extends Controller
{

    public function searchFormAction(Request $request)
    {
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);
        return $this->render('agence/search/form.html.twig',
                array('search_form' => $form->createView()));
    }

    /**
     * @Route("/", name="agency_search")
     */
    public function searchAction(Request $request)
    {
        $form       = $request->get('search_form');
        $name       = trim($form['name']);
        $operations = $this->get('search.manager')->getSearchResultByCurrentAgency($name);
        if (empty($operations)) {
            $this->get('session')->getFlashBag()->add('success',
                'Aucun résultat correspond a votre recherche.');
        }
        return $this->render('agence/search/search.html.twig',
                array(
                'operations' => $operations,
                'search_form' => $form
        ));
    }
}