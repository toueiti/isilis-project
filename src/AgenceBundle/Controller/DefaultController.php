<?php

namespace AgenceBundle\Controller;

use AgenceBundle\Form\AgenceType;
use BackBundle\Entity\Tchat;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;



class DefaultController extends Controller
{

    /**
     * @Route("/", name="agence_home")
     */
    public function indexAction()
    {
        $banks = $this->get('operation.manager')->filterOpertaionInActivationByCurrentAgency($formData = null);
        return $this->render('agence/default/index.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/info", name="agence_info")
     */
    public function infoAction(Request $request)
    {
        $agence = $this->get('agence.agence.info')->getAgence();
        $form = $this->createForm(AgenceType::class, $agence);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($agence);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success',
                    'Mise à jour avec succès.');
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
        return $this->render('agence/default/info.html.twig',
            array(
                'agence' => $agence,
                'form' => $form->createView()
            ));
    }

    
}