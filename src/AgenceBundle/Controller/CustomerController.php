<?php

namespace AgenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AgenceBundle\Form\FilterFormType;
use BackBundle\Entity\Operation;
use BackBundle\Entity\Caisse;

/**
 * @Route("/customer")
 */
class CustomerController extends Controller
{

    /**
     * @Route("/", name="index_custumer")
     */
    public function indextAction(Request $request)
    {
        $formData = $request->get('filter_form');
        $banks    = $this->get('campaign.manager')->filterCustomerList($formData);
        return $this->render('agence/customer/index.html.twig',
                array(
                'banks' => $banks,
                'filter_form_customer' => $formData
        ));
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @Route("/filter", options={"expose"=true},name="agence_customer_filter")
     */
    public function FormFilterAction(Request $request)
    {
        $em    = $this->getDoctrine()->getManager();
        $years = $em->getRepository(Operation::class)->getYears();
        $agence = $this->container->get('agence.agence.info')->getAgence();
        $form  = $this->createForm(FilterFormType::class,
            array(
            'years' => $years,
            'agence' => $agence->getId()
            ));
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $formData = $request->get('filter_form');
            $banks    = $this->get('campaign.manager')->filterCustomerList($formData);
            return $this->render('agence/customer/result.html.twig',
                    array(
                    'banks' => $banks,
            ));
        }
        return $this->render('agence/customer/form.html.twig',
                array(
                'filter_form_customer' => $form->createView()
        ));
    }
}