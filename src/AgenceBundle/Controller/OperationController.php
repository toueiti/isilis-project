<?php

namespace AgenceBundle\Controller;

use AgenceBundle\Form\FormOperationType;
use BackBundle\Entity\Operation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/operation")
 */
class OperationController extends Controller
{

    /**
     *
     * @param Request $request
     * @return type
     * @Route("/filter/{status}", name="agence_operation_filter")
     */
    public function FormFilterAction(Request $request, $status = null)
    {
        $em = $this->getDoctrine()->getManager();
        $agence = $this->container->get('agence.agence.info')->getAgence();
        $years = $em->getRepository(Operation::class)->getYears();
        $form = $this->createForm(FormOperationType::class, array(
            'years' => $years,
            'agence' => $agence->getId(),
            'ids'=>$this->getBanks()
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $formData = $request->get('form_operation');
            if ($status == 'done') {
                $banks = $this->get('operation.manager')->filterOpertaionDoneByCurentAgency($formData);
                return $this->render('agence/operation/done_result.html.twig', array(
                    'banks' => $banks,
                ));
            } else {
                $banks = $this->get('operation.manager')->filterOpertaionInActivationByCurrentAgency($formData);
                return $this->render('agence/operation/validation_result.html.twig', array(
                    'banks' => $banks,
                ));
            }
        }
        return $this->render('agence/operation/form_filter.html.twig', array(
            'filter_operation_filter' => $form->createView(),
            'status' => $status
        ));
    }

    public function getBanks(){
        $banks = $this->get('operation.manager')->filterOpertaionInActivationByCurrentAgency(null);
        $ids  = array();
        foreach ($banks as $key => $bank){
           $ids[$key] = $bank['bank_data']->getId();
         }
        return $ids;
    }

    /**
     * @Route("/validation", name="operation_validation_index")
     */
    public function operationValidationAction(Request $request)
    {
        $this->getBanks();
        $formData = $request->get('form_operation');
        $banks = $this->get('operation.manager')->filterOpertaionInActivationByCurrentAgency($formData);
        return $this->render('agence/operation/validation.html.twig', array(
            'banks' => $banks
        ));
    }

    /**
     * @Route("/bank/{id}", name="get_caisse", options={"expose"=true})
     */
    public function getCaisseAction(Request $request,$id)
    {

        $em = $this->getDoctrine()->getManager();
        $caisses = $em->getRepository("BackBundle:Caisse")->findBy(array('bank'=>$id,'active'=> 1));

        $caisseList = array(array());
        if ($caisses) {
            foreach ($caisses as $i => $caisse) {
                $caisseList[$i]['label'] = $caisse->getId();
                $caisseList[$i]['value'] = $caisse->getName();
            }
        } else {
            $caisseList = 0;
        }
        return new Response(json_encode($caisseList), 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/done", name="operation_done_index")
     */
    public function operationDoneAction(Request $request)
    {
        $formData = $request->get('form_operation');
        $banks = $this->get('operation.manager')->filterOpertaionDoneByCurentAgency($formData);
        return $this->render('agence/operation/done.html.twig', array(
            'banks' => $banks
        ));
    }

    /**
     * @Route("/detail/{id}", name="agence_operation_detail")
     */
    public function detailAction(Operation $operation)
    {
        $em = $this->getDoctrine()->getManager();
        //$messages = $operation->getMessages();
        $messages = $em->getRepository(\BackBundle\Entity\Message::class)->getAgenceMessagesByOperation($operation);
        foreach ($messages as $message) {
            if (!$message->getSeenAgence()) {
                $message->setSeenAgence(true);
                $em->persist($message);
            }
        }
        try {
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return $this->render('agence/operation/detail.html.twig', array(
            'operation' => $operation,
            'messages' => $messages
        ));
    }

}
