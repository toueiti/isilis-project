<?php

namespace AgenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Entity\Operation;
use BackBundle\Entity\Message;
use CaisseBundle\Form\MessageType;

/** 
 * @author slim
 * @Route("/message")
 */
class MessageController extends Controller
{
    /**
     * @Route("/new/{id}", name="agence_message_new")
     */
    public function newAction(Request $request, Operation $operation) 
    {
        $message = new Message();
        $message->setSource($message::AGENCE_SEND);
        $message->setDest(Message::ADMIN_RECIV);
        $message->setValideAgence(TRUE);
        $message->setSeenAgence(true);
        $message->setOperation($operation);
        $message->setUser($this->getUser());
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $pieces = explode(',', $request->get('piece'));
                foreach ($pieces as $idpiece) {
                    $jointe = $em->find(\BackBundle\Entity\Jointe::class,
                        $idpiece);
                    if (is_object($jointe)) {
                        $jointe->setMessage($message);
                        $em->persist($jointe);
                    }
                }
                try {
                $em->persist($message);
                $em->flush();
                $this->container->get('message.manager')->sendNotif($message);
                $this->get('session')->getFlashBag()->add('success', 'Message envoyé avec succès.');
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
            }
            else {
                $this->get('session')->getFlashBag()->add('error', 'Erreur ! message non envoyé !');
            }
//            $referer = $request->headers->get('referer');
//            return $this->redirect($referer);
            return $this->redirect($this->generateUrl('agence_operation_detail', array(
                'id'=>$operation->getId()
            )));
        }
        return $this->render('agence/message/new.html.twig', array(
            'form' => $form->createView(),
            'operation'=>$operation
        ));
    }
}
