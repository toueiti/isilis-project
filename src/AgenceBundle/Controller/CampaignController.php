<?php

namespace AgenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AgenceBundle\Form\FilterCampaignType;
use BackBundle\Entity\Campaign;

/**
 * @Route("/compagne")
 */
class CampaignController extends Controller
{

    public function FormFilterAction(Request $request, $status = 'active')
    {
        $form = $this->createForm(FilterCampaignType::class);
        $form->handleRequest($request);
        return $this->render('agence/campaign/form_filter.html.twig',
                array(
                    'form_campaign_filter' => $form->createView(),
                    'status' => $status
        ));
    }

    /**
     * @Route("/active", name="campaign_active_index")
     */
    public function campaignActiveAction(Request $request)
    {
        $formData = $request->get('filter_campaign');
        $banks    = $this->get('campaign.manager')->FilterActiveCompaignByCurrentAgency($formData);
        return $this->render('agence/campaign/active.html.twig',
                array(
                'banks' => $banks,
                'form_campaign_filter' => $formData
        ));
    }

    /**
     * @Route("/bank_camp/{id}", options={"expose"=true}, name="filter_campaign")
     */
    public function filterAction(\BackBundle\Entity\Bank $bank)
    {
        $formData = array('banque' => $bank->getId());
        $banks    = $this->get('campaign.manager')->FilterActiveCompaignByCurrentAgency($formData);
        return $this->render('agence/campaign/filter_result.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/bank_camp_done/{id}", options={"expose"=true}, name="filter_campaign_done")
     */
    public function filterDoneAction(\BackBundle\Entity\Bank $bank)
    {
        $formData = array('banque' => $bank->getId());
        $banks    = $this->get('campaign.manager')->filterDoneCompaignByCurrentAgency($formData);
        return $this->render('agence/campaign/filter_result.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/done", name="campaign_done_index")
     */
    public function campaignDoneAction(Request $request)
    {
        $formData = $request->get('filter_campaign');
        $banks    = $this->get('campaign.manager')->filterDoneCompaignByCurrentAgency($formData);
        return $this->render('agence/campaign/done.html.twig',
                array(
                'banks' => $banks
        ));
    }

    /**
     * @Route("/detail/{id}", options={"expose"=true}, name="agence_campaign_detail")
     */
    public function detailAction(Campaign $campagn)
    {
        return $this->render('agence/campaign/detail.html.twig',
                array(
                'campaign' => $campagn
        ));
    }
}