<?php

namespace UserBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use BackBundle\Entity\Message;

/**
 * Description of AppExtension
 *
 * @author slim
 */
class AppExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    private $container;
    protected $security;

    public function __construct(Container $container, $security)
    {
        $this->container = $container;
        $this->security  = $security;
    }

    public function getGlobals()
    {
        $token = $this->security->getToken();
        if($token){
            if(is_object($token->getUser())){
                $Roles = $token->getUser()->getRoles();
                if (in_array("ROLE_CAISSE", $Roles)) {
                    $caisse        = $this->container->get('caisse.caisse.info')->getCaisse();
                    $em            = $this->container->get('doctrine.orm.entity_manager');
                    $newOperations = $this->container->get('caisse.caisse.info')->getMessagesByCaisse($caisse,
                        $em);

                    return array(
                        'caisse' => $caisse,
                        'new_oprations' => $newOperations['operations'],
                        'nbmsg' => $newOperations['nbmsg']
                    );
                }
                if (in_array("ROLE_AGENCE", $Roles)) {
                    $agence        = $this->container->get('agence.agence.info')->getAgence();
                    $em            = $this->container->get('doctrine.orm.entity_manager');
                    $newOperations = $this->container->get('agence.agence.info')->getMessagesByAgence($agence,
                        $em);
                    return array(
                        'agence' => $agence,
                        'new_oprations' => $newOperations['operations'],
                        'nbmsg' => $newOperations['nbmsg']
                    );
                }
                if (in_array("ROLE_ADMIN", $Roles) || in_array("ROLE_SUPER_ADMIN", $Roles) ){
                    $newOperations = $this->container->get('message.manager')->getNewMessages(null);
                    return array(
                        'new_oprations' => $newOperations['operations'],
                        'nbmsg' => $newOperations['nbmsg']
                    );
                }
                if (in_array("ROLE_COMERCIAL", $Roles)){
                    $newOperations = $this->container->get('message.manager')->getNewMessages($token->getUser()->getCommercial()->getId());
                    return array(
                        'new_oprations' => $newOperations['operations'],
                        'nbmsg' => $newOperations['nbmsg']
                    );
                }
            }
        }
        return array(
        );
    }

    public function getName()
    {
        return 'isilis_extention';
    }
}