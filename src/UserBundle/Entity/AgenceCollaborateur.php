<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AgenceCollaborateur
 *
 * @ORM\Table(name="agence_collaborateur")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\AgenceCollaborateurRepository")
 */
class AgenceCollaborateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="string", length=255)
     */
    private $fonction;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity="BackBundle\Entity\Agence", inversedBy="collaborateurs")
     * @ORM\JoinColumn(name="agence_id", referencedColumnName="id")
     */
    protected $agence;
    
    /**
    * @ORM\OneToOne(targetEntity="User", inversedBy="agenceCollaborateur", cascade={"persist"})
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
    */
    protected $user;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return AgenceCollaborateur
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fonction.
     *
     * @param string $fonction
     *
     * @return AgenceCollaborateur
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction.
     *
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return AgenceCollaborateur
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set agence.
     *
     * @param \BackBundle\Entity\Agence|null $agence
     *
     * @return AgenceCollaborateur
     */
    public function setAgence(\BackBundle\Entity\Agence $agence = null)
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * Get agence.
     *
     * @return \BackBundle\Entity\Agence|null
     */
    public function getAgence()
    {
        return $this->agence;
    }

    /**
     * Set user.
     *
     * @param \UserBundle\Entity\User|null $user
     *
     * @return AgenceCollaborateur
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \UserBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
