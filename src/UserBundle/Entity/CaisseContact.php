<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CaisseContact
 *
 * @ORM\Table(name="caisse_contact")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\CaisseContactRepository")
 */
class CaisseContact {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="string", length=255)
     */
    private $fonction;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\OneToOne(targetEntity="BackBundle\Entity\Caisse", mappedBy="contact")
     * 
     */
    protected $caisse;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="caisseContact", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return CaisseContact
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set fonction.
     *
     * @param string $fonction
     *
     * @return CaisseContact
     */
    public function setFonction($fonction) {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction.
     *
     * @return string
     */
    public function getFonction() {
        return $this->fonction;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return CaisseContact
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Constructor
     */
    public function __construct() {
        
    }

    /**
     * Set user.
     *
     * @param \UserBundle\Entity\User|null $user
     *
     * @return CaisseContact
     */
    public function setUser(\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \UserBundle\Entity\User|null
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set caisse.
     *
     * @param \BackBundle\Entity\Caisse|null $caisse
     *
     * @return CaisseContact
     */
    public function setCaisse(\BackBundle\Entity\Caisse $caisse = null) {
        $this->caisse = $caisse;

        return $this;
    }

    /**
     * Get caisse.
     *
     * @return \BackBundle\Entity\Caisse|null
     */
    public function getCaisse() {
        return $this->caisse;
    }

}
