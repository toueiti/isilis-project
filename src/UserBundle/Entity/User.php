<?php
// src/AppBundle/Entity/User.php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("email", message="Email déjà utilisé")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    /**
     * @ORM\OneToOne(targetEntity="AgenceContact", mappedBy="user", cascade={"persist"})
     */
    protected $agenceContact;

    /**
     * @ORM\OneToOne(targetEntity="CaisseContact", mappedBy="user", cascade={"persist"})
     */
    protected $caisseContact;

    /**
     * @ORM\OneToOne(targetEntity="Commercial", mappedBy="user", cascade={"persist"})
     */
    protected $commercial;

    /**
     * @ORM\OneToOne(targetEntity="CaisseCollaborateur", mappedBy="user", cascade={"persist"})
     */
    protected $caisseCollaborateur;

    /**
     * @ORM\OneToOne(targetEntity="AgenceCollaborateur", mappedBy="user", cascade={"persist"})
     */
    protected $agenceCollaborateur;

    /**
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\Message", mappedBy="user", cascade={"persist"})
     * 
     */
    protected $messages;

    /**
     * @ORM\OneToMany(targetEntity="ConnectionHistory", mappedBy="user")
     */
    private $history;

    /**
     * Set agenceContact.
     *
     * @param \UserBundle\Entity\AgenceContact|null $agenceContact
     *
     * @return User
     */
    public function setAgenceContact(\UserBundle\Entity\AgenceContact $agenceContact
    = null)
    {
        $this->agenceContact = $agenceContact;

        return $this;
    }

    /**
     * Get agenceContact.
     *
     * @return \UserBundle\Entity\AgenceContact|null
     */
    public function getAgenceContact()
    {
        return $this->agenceContact;
    }

    /**
     * Set caisseContact.
     *
     * @param \UserBundle\Entity\CaisseContact|null $caisseContact
     *
     * @return User
     */
    public function setCaisseContact(\UserBundle\Entity\CaisseContact $caisseContact
    = null)
    {
        $this->caisseContact = $caisseContact;

        return $this;
    }

    /**
     * Get caisseContact.
     *
     * @return \UserBundle\Entity\CaisseContact|null
     */
    public function getCaisseContact()
    {
        return $this->caisseContact;
    }

    /**
     * Set commercial.
     *
     * @param \UserBundle\Entity\Commercial|null $commercial
     *
     * @return User
     */
    public function setCommercial(\UserBundle\Entity\Commercial $commercial = null)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial.
     *
     * @return \UserBundle\Entity\Commercial|null
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * Set caisseCollaborateur.
     *
     * @param \UserBundle\Entity\CaisseCollaborateur|null $caisseCollaborateur
     *
     * @return User
     */
    public function setCaisseCollaborateur(\UserBundle\Entity\CaisseCollaborateur $caisseCollaborateur
    = null)
    {
        $this->caisseCollaborateur = $caisseCollaborateur;

        return $this;
    }

    /**
     * Get caisseCollaborateur.
     *
     * @return \UserBundle\Entity\CaisseCollaborateur|null
     */
    public function getCaisseCollaborateur()
    {
        return $this->caisseCollaborateur;
    }

    /**
     * Add message.
     *
     * @param \BackBundle\Entity\Message $message
     *
     * @return User
     */
    public function addMessage(\BackBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message.
     *
     * @param \BackBundle\Entity\Message $message
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMessage(\BackBundle\Entity\Message $message)
    {
        return $this->messages->removeElement($message);
    }

    /**
     * Get messages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Add history.
     *
     * @param \UserBundle\Entity\ConnectionHistory $history
     *
     * @return User
     */
    public function addHistory(\UserBundle\Entity\ConnectionHistory $history)
    {
        $this->history[] = $history;

        return $this;
    }

    /**
     * Remove history.
     *
     * @param \UserBundle\Entity\ConnectionHistory $history
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeHistory(\UserBundle\Entity\ConnectionHistory $history)
    {
        return $this->history->removeElement($history);
    }

    /**
     * Get history.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set agenceCollaborateur.
     *
     * @param \UserBundle\Entity\AgenceCollaborateur|null $agenceCollaborateur
     *
     * @return User
     */
    public function setAgenceCollaborateur(\UserBundle\Entity\AgenceCollaborateur $agenceCollaborateur = null)
    {
        $this->agenceCollaborateur = $agenceCollaborateur;

        return $this;
    }

    /**
     * Get agenceCollaborateur.
     *
     * @return \UserBundle\Entity\AgenceCollaborateur|null
     */
    public function getAgenceCollaborateur()
    {
        return $this->agenceCollaborateur;
    }
}
