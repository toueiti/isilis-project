<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Commercial
 *
 * @ORM\Table(name="commercial")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\CommercialRepository")
 */
class Commercial {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="fonction", type="string", length=255)
     */
    private $fonction;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="del", type="boolean", nullable=true)
     */
    private $del = false;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="commercial", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\Caisse", mappedBy="commercial")
     */
    private $caisse;

    /**
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\Bank", mappedBy="commercial")
     */
    private $bank;

    /**
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\Agence", mappedBy="commercial")
     */
    private $agence;

    /**
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\Campaign", mappedBy="commercial")
     */
    private $campaign;

    /**
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\ObjetCompaign", mappedBy="commercial")
     */
    private $objectCampaign;

    /**
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\ObjetOperation", mappedBy="commercial")
     */
    private $objetOperation;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Commercial
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set fonction.
     *
     * @param string $fonction
     *
     * @return Commercial
     */
    public function setFonction($fonction) {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction.
     *
     * @return string
     */
    public function getFonction() {
        return $this->fonction;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return Commercial
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set user.
     *
     * @param \UserBundle\Entity\User|null $user
     *
     * @return Commercial
     */
    public function setUser(\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \UserBundle\Entity\User|null
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set bank.
     *
     * @param \BackBundle\Entity\Bank|null $bank
     *
     * @return Commercial
     */
    public function setBank(\BackBundle\Entity\Bank $bank = null) {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank.
     *
     * @return \BackBundle\Entity\Bank|null
     */
    public function getBank() {
        return $this->bank;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bank = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bank.
     *
     * @param \BackBundle\Entity\Bank $bank
     *
     * @return Commercial
     */
    public function addBank(\BackBundle\Entity\Bank $bank)
    {
        $this->bank[] = $bank;

        return $this;
    }

    /**
     * Remove bank.
     *
     * @param \BackBundle\Entity\Bank $bank
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBank(\BackBundle\Entity\Bank $bank)
    {
        return $this->bank->removeElement($bank);
    }

    /**
     * Set del.
     *
     * @param bool|null $del
     *
     * @return Commercial
     */
    public function setDel($del = null)
    {
        $this->del = $del;

        return $this;
    }

    /**
     * Get del.
     *
     * @return bool|null
     */
    public function getDel()
    {
        return $this->del;
    }

    /**
     * Add caisse.
     *
     * @param \BackBundle\Entity\Caisse $caisse
     *
     * @return Commercial
     */
    public function addCaisse(\BackBundle\Entity\Caisse $caisse)
    {
        $this->caisse[] = $caisse;

        return $this;
    }

    /**
     * Remove caisse.
     *
     * @param \BackBundle\Entity\Caisse $caisse
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCaisse(\BackBundle\Entity\Caisse $caisse)
    {
        return $this->caisse->removeElement($caisse);
    }

    /**
     * Get caisse.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCaisse()
    {
        return $this->caisse;
    }

    /**
     * Add agence.
     *
     * @param \BackBundle\Entity\Agence $agence
     *
     * @return Commercial
     */
    public function addAgence(\BackBundle\Entity\Agence $agence)
    {
        $this->agence[] = $agence;

        return $this;
    }

    /**
     * Remove agence.
     *
     * @param \BackBundle\Entity\Agence $agence
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAgence(\BackBundle\Entity\Agence $agence)
    {
        return $this->agence->removeElement($agence);
    }

    /**
     * Get agence.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgence()
    {
        return $this->agence;
    }

    /**
     * Add campaign.
     *
     * @param \BackBundle\Entity\Campaign $campaign
     *
     * @return Commercial
     */
    public function addCampaign(\BackBundle\Entity\Campaign $campaign)
    {
        $this->campaign[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign.
     *
     * @param \BackBundle\Entity\Campaign $campaign
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCampaign(\BackBundle\Entity\Campaign $campaign)
    {
        return $this->campaign->removeElement($campaign);
    }

    /**
     * Get campaign.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @return mixed
     */
    public function getObjectCampaign()
    {
        return $this->objectCampaign;
    }

    /**
     * @param mixed $objectCampaign
     */
    public function setObjectCampaign($objectCampaign)
    {
        $this->objectCampaign = $objectCampaign;
    }

    /**
     * @return mixed
     */
    public function getObjetOperation()
    {
        return $this->objetOperation;
    }

    /**
     * @param mixed $objetOperation
     */
    public function setObjetOperation($objetOperation)
    {
        $this->objetOperation = $objetOperation;
    }


}
