<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("users")
 */
class ConnectionHistoryController extends Controller
{

    /**
     * @Route("/connection/history/show", name="connection_history_show")
     */
    public function connectionHistoryShowAction(Request $request)
    {
        $dates = array('startdate' => $request->get('startdate'), 'enddate' => $request->get('enddate'));
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $users = $this->get('connection.history.manager')->filterConnectionHistory($dates, $this->getUser()->getCommercial()->getId());
        }else{
            $users = $this->get('connection.history.manager')->filterConnectionHistory($dates, null);
        }
        return $this->render('back/users/history_show.html.twig',
                array('users' => $users));
    }

    /**
     * @Route("/agence/history/show", name="agence_history_show")
     */
    public function agenceHistoryShowAction(Request $request)
    {
        $dates = array('startdate' => $request->get('startdate'), 'enddate' => $request->get('enddate'));
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $users = $this->get('connection.history.manager')->filterConnectionHistory($dates, $this->getUser()->getCommercial()->getId());
        }else{
            $users = $this->get('connection.history.manager')->filterConnectionHistory($dates, null);
        }
        return $this->render('back/users/agnece_history_show.html.twig',
                array('users' => $users));
    }

    /**
     * @Route("/caisse/history/show", name="caisse_history_show")
     */
    public function caisseHistoryShowAction(Request $request)
    {
        $dates = array('startdate' => $request->get('startdate'), 'enddate' => $request->get('enddate'));
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $users = $this->get('connection.history.manager')->filterConnectionHistory($dates, $this->getUser()->getCommercial()->getId());
        }else{
            $users = $this->get('connection.history.manager')->filterConnectionHistory($dates, null);
        }
        return $this->render('back/users/caisse_history_show.html.twig',
                array('users' => $users));
    }
}