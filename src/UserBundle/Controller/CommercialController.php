<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UserBundle\Entity\Commercial;
use UserBundle\Form\CommercialType;

/**
 * @Route("/commercial")
 */
class CommercialController extends Controller
{

    /**
     * @Route("/", name="commercial")
     */
    public function indexAction()
    {
        $commercials = $this->getDoctrine()->getManager()->getRepository(Commercial::class)->findByDel(0);
        return $this->render('back/commercial/index.html.twig',
                array(
                'commercials' => $commercials
        ));
    }

    /**
     * @Route("/new", name="commercial_new")
     */
    public function newAction(Request $request)
    {
        $commercial = new Commercial();
        $form       = $this->createForm(CommercialType::class, $commercial);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $user      = $commercial->getUser();
                $validator = $this->get('validator');
                $errors    = $validator->validate($user);
                if (count($errors) > 0) {
                    $this->get('session')->getFlashBag()->add('error',
                        'Email déjà utilisé !');
                } else {
                    $this->get('commercial.manager')->createCommercial($commercial);
                    $this->get('session')->getFlashBag()->add('success',
                        'Votre inscription est confirmée.<br />
Vous allez recevoir un email accompagné de votre identifiant et votre mot de passe de connexion.<br />
L\'équipe ISILIS
');
                    return $this->redirect($this->generateUrl('commercial'));
                }
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/commercial/new.html.twig',
                array(
                'form' => $form->createView(),
                'commercial' => $commercial
        ));
    }

    /**
     * @Route("/{id}/edit", name="commercial_edit")
     */
    public function editAction(Request $request, Commercial $commercial)
    {
        $form = $this->createForm(CommercialType::class, $commercial);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('commercial.manager')->editCommercial($commercial);
                $this->get('session')->getFlashBag()->add('success',
                    'Fiche du commercial modifiée');
                return $this->redirect($this->generateUrl('commercial'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/commercial/edit.html.twig',
                array(
                'form' => $form->createView(),
                'commercial' => $commercial
        ));
    }

    /**
     * @Route("/delete/{id}", name="commercial_delete")
     */
    public function deleteAction(Commercial $commercial)
    {
        $em = $this->getDoctrine()->getManager();
        $commercial->setDel(TRUE);
        $em->persist($commercial);
        $em->flush();
        return $this->redirect($this->generateUrl('commercial'));
    }
}