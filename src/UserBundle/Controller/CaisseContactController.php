<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\CaisseContact;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/")
 */
class CaisseContactController extends Controller
{

    /**
     * @Route("/back/contact", name="caisse_contact")
     */
    public function indexAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('caisse.manager')->FilterCaisseContact($this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('caisse.manager')->FilterCaisseContact(null);
        }
        return $this->render('/back/caisse-contact/index.html.twig',
                array(
                'banks' => $banks
        ));
    }

    /**
     * @Route("/back/parameter-notif", name="parametre_notif", options={"expose"=true})
     */
    public function notifAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('caisse.manager')->FilterCaisseContact($this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('caisse.manager')->FilterCaisseContact(null);
        }
        return $this->render('/back/notif-parameter/index.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/back/parameter-notif-active/{id}", name="parametre_notif_active", options={"expose"=true})
     */
    public function notifActiveAction( Request $request,$id)
    {
            $em = $this->getDoctrine()->getManager();
            $casse = $em->getRepository('BackBundle:Caisse')->find($id);
            if($casse) {
                $casse->setIsNotif(true);
                $em->persist($casse);
                $em->flush();
                $response = new Response();
                $response->setStatusCode(Response::HTTP_OK);
                $response->setContent('1');
            }else{
                $response = new Response();
                $response->setStatusCode(Response::HTTP_OK);
                $response->setContent('0');
            }
        return $response;
    }

    /**
     * @Route("/back/parameter-notif-desactive/{id}", name="parametre_notif_desacive", options={"expose"=true})
     */
    public function notifdesactiveAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $casse = $em->getRepository('BackBundle:Caisse')->find($id);
        if($casse) {
            $casse->setIsNotif(false);
            $em->persist($casse);
            $em->flush();
            $response = new Response();
            $response->setStatusCode(Response::HTTP_OK);
            $response->setContent('1');
        }else{
            $response = new Response();
            $response->setStatusCode(Response::HTTP_OK);
            $response->setContent('0');
        }
        return $response;
    }

    /**
     * @Route("/back/contact/export", name="export_contact", options={"expose"=true})
     */
    public function exportAction(){
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('caisse.manager')->FilterCaisseContact($this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('caisse.manager')->FilterCaisseContact(null);
        }
        if(!empty($banks)){
            $contenu = '';
            foreach ($banks as $bank){
                $contenu .='Nom bank ;Email;Télephone;'."\r\n";
                $bankobject = $bank['bank_data'];
                $contenu .= $bankobject->getName().';'.$bankobject->getContactEmail().';'.$bankobject->getContactPhone().';'."\r\n";
                $contenu .=' ; ; ;'."\r\n";
                $caisse_data = $bank['caisse_data'];
                $contenu .= ' ; Nom caisse ;Email;Télephone;'."\r\n";
                foreach ($caisse_data as $caisse){
                    $contenu .= ' ; '.$caisse->getName().';'.$caisse->getContact()->getUser()->getEmail().';'.$caisse->getContact()->getPhone().';'."\r\n";
                }
                $contenu .=' ; ; ;'."\r\n";
            }

            $file= __DIR__."/../../../web/uploads/contact.csv";
            $fp=fopen($file,"w" ); // ouverture du fichier
            fputs($fp,utf8_encode($contenu)); // enregistrement des données ds le fichier
            fclose($fp);

            //readfile($file);
            // unlink($file);
            $response = new Response();;
            $response->setContent(file_get_contents($file));
            $response->headers->set('Content-Type', 'application/force-download');
            $response->headers->set('Content-disposition', 'filename= contact.csv');
            $response->setContent(utf8_decode($contenu));
            $response->setStatusCode(200);
            return $response;
        }
    }
}