<?php

namespace UserBundle\Service;

use Symfony\Component\Routing\RouterInterface,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface,
    Symfony\Component\Security\Core\Authentication\Token\TokenInterface,
    Doctrine\ORM\EntityManager,
    Doctrine\ORM\EntityManagerInterface,
    Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected $router;
    private $container;

    public function __construct(RouterInterface $router, Container $container)
    {
        $this->router    = $router;
        $this->container = $container;
    }

    public function onAuthenticationSuccess(Request $request,
                                            TokenInterface $token)
    {
        try {
            $em                = $this->container->get('doctrine')->getManager();
            $Roles             = $token->getUser()->getRoles();
            $connectionHistory = new \UserBundle\Entity\ConnectionHistory();
            $connectionHistory->setUser($token->getUser());
            $em->persist($connectionHistory);
            $em->flush();
            if ((in_array("ROLE_ADMIN", $Roles)) || (in_array("ROLE_SUPER_ADMIN",
                    $Roles)) || (in_array("ROLE_COMERCIAL",
                    $Roles))) {
                return new RedirectResponse($this->router->generate('back_home'));
            }

            if (in_array("ROLE_AGENCE", $Roles)) {
                return new RedirectResponse($this->router->generate('index_custumer'));
            }

            if (in_array("ROLE_CAISSE", $Roles)) {
                return new RedirectResponse($this->router->generate('caisse_home'));
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}