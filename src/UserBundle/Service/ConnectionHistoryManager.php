<?php

namespace UserBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use UserBundle\Entity\ConnectionHistory;

class ConnectionHistoryManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function filterConnectionHistory($dates, $commercial)
    {
        $users                = array();
        $users['caise_data']  = null;
        $users['agence_data'] = null;
      if(!is_null($commercial)){
          $connectionHitory = $this->container->get('doctrine')->getManager()->getRepository(ConnectionHistory::class)->getUsers($dates, $commercial);
          foreach ($connectionHitory as $history) {
              if (in_array('ROLE_CAISSE', $history->getUser()->getRoles())) {
                  $users['caise_data'][] = $history;
              } else if (in_array('ROLE_AGENCE', $history->getUser()->getRoles())) {
                  $users['agence_data'][] = $history;
              }
          }
      }else{
          $connectionHitory = $this->container->get('doctrine')->getManager()->getRepository(ConnectionHistory::class)->getUsers($dates, $commercial);
          foreach ($connectionHitory as $history) {
              if (in_array('ROLE_CAISSE', $history->getUser()->getRoles())) {
                  $users['caise_data'][] = $history;
              } else if (in_array('ROLE_AGENCE', $history->getUser()->getRoles())) {
                  $users['agence_data'][] = $history;
              }
          }
      }
        return $users;
    }
}