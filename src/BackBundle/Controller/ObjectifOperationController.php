<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BackBundle\Entity\ObjetOperation;
use BackBundle\Form\ObjetOperationType;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * @Route("/objectif/operation")
 */
class ObjectifOperationController extends Controller
{

    /**
     * @Route("/", name="index_objectif_operation")
     */
    public function indexAction()
    {

            $objectif_operation = $this->getDoctrine()->getManager()->getRepository(ObjetOperation::class)->findBy(
                array('is_ative' => 1,
                ));
        return $this->render('/back/objectif-operation/index.html.twig',
                array(
                'objectif_operation' => $objectif_operation
        ));
    }
    /**
     * @Route("delete/{id}", name="objet_operation_delete")
     */
    public function objetOperationDelete(Request $request, ObjetOperation $objetoperation ){
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $objetoperation->setIsActive(0);

        $em->persist($objetoperation);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',
            'objectif campagne supprimées.');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/new", name="objectif_operation_new")
     */
    public function newAction(Request $request)
    {
        $objectif_operation = new ObjetOperation();
        $form               = $this->createForm(ObjetOperationType::class,
            $objectif_operation);
//        if($this->isGranted('ROLE_COMERCIAL')){
//            $objectif_operation->setCommercial($this->getUser()->getCommercial());
//        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('objectif.operation.manager')->createObjectifOperation($objectif_operation);
                $this->get('session')->getFlashBag()->add('success',
                    'objectif opération ajoutée avec succès.');
                return $this->redirect($this->generateUrl('index_objectif_operation'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire.');
            }
        }
        return $this->render('/back/objectif-operation/new.html.twig',
                array(
                'form' => $form->createView(),
                'objectif_operation' => $objectif_operation
        ));
    }

    /**
     * @Route("/{id}/edit", name="objet_operation_edit")
     */
    public function editAction(Request $request,
                               ObjetOperation $objectif_operation)
    {
        $form = $this->createForm(ObjetOperationType::class, $objectif_operation);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('objectif.operation.manager')->editObjectifOperation($objectif_operation);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès.');
                return $this->redirect($this->generateUrl('index_objectif_operation'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire.');
            }
        }
        return $this->render('/back/objectif-operation/edit.html.twig',
                array(
                'form' => $form->createView(),
                'objectif_operation' => $objectif_operation
        ));
    }
}