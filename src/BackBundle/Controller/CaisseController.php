<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Entity\Caisse;
use BackBundle\Form\CaisseType;
use BackBundle\Entity\VisualPrint;
use BackBundle\Entity\VisualWeb;

/**
 * Description of CaisseController
 * @Route("/caisse")
 * @author slim toueiti
 */
class CaisseController extends Controller
{

    /**
     * @Route("/", name="caisse")
     */
    public function indexAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('caisse.manager')->filtreAgenceByCaisse($this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('caisse.manager')->filtreAgenceByCaisse(null);
        }
        return $this->render('back/caisse/index.html.twig',
                array(
                'banks' => $banks
        ));
    }

    /**
     * @Route("/{id}/susupend/", name="caisse_suspendu")
     */
    public function inactiveAction(Caisse $caisse)
    {
        $em = $this->getDoctrine()->getManager();
        $caisse->setActive(Caisse::STATUS_SUSPENDU);
        $user = $caisse->getContact()->getUser();
        $user->setEnabled(false);
        $em->flush();
        $em->flush($caisse);
        $this->get('session')->getFlashBag()->add('success', 'Caisse archivée.');
        return $this->redirect($this->generateUrl('caisse'));
    }

    /**
     * @Route("/{id}/activate", name="caisse_activate")
     */
    public function activateAction(Caisse $caisse)
    {
        $em = $this->getDoctrine()->getManager();
        $caisse->setActive(Caisse::STATUS_ACTIVE);
        $user = $caisse->getContact()->getUser();
        $user->setEnabled(false);
        $em->flush();       
        $this->get('session')->getFlashBag()->add('success', 'Caisse activée.');
        return $this->redirect($this->generateUrl('caisse'));
    }

    /**
     * @Route("/new", name="caisse_new")
     */
    public function newAction(Request $request)
    {
        $caisse = new Caisse();
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $caisse->setCommercial($this->getUser()->getCommercial());
        }
        $caisse->addVisualWeb(new VisualWeb());
        $caisse->addVisualPrint(new VisualPrint());
        $form   = $this->createForm(CaisseType::class, $caisse);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $contact   = $caisse->getContact();
                $user      = $contact->getUser();
                $validator = $this->get('validator');
                $errors    = $validator->validate($user);
                if (count($errors) > 0) {
                    $this->get('session')->getFlashBag()->add('error',
                        'Email déjà utilisé !');
                } else {
                    $this->get('caisse.manager')->createCaisse($caisse);
                    $this->get('session')->getFlashBag()->add('success',
                        'Votre inscription est confirmée.<br />
Vous allez recevoir un email accompagné de votre identifiant et votre mot de passe de connexion.<br />
L\'équipe ISILIS
');
                    return $this->redirect($this->generateUrl('caisse'));
                }
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/caisse/new.html.twig',
                array(
                'form' => $form->createView()
        ));
    }

    /**
     * @Route("/{id}/edit", name="caisse_edit")
     */
    public function editAction(Request $request, Caisse $caisse)
    {
        $form = $this->createForm(CaisseType::class, $caisse);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('caisse.manager')->editCaisse($caisse);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès.');
                return $this->redirect($this->generateUrl('caisse'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/caisse/edit.html.twig',
                array(
                'form' => $form->createView(),
                'caisse' => $caisse
        ));
    }

    /**
     * @Route("/{id}/view", name="caisse_view")
     */
    public function viewAction(Caisse $caisse)
    {
        $active_operation        = $this->getDoctrine()->getManager()->getRepository(\BackBundle\Entity\Operation::class)->filterActiveOpertaionByCaisse($caisse->getId());
        $done_operation          = $this->getDoctrine()->getManager()->getRepository(\BackBundle\Entity\Operation::class)->filterDoneOpertaionByCaisse($caisse->getId());
        $in_activation_operation = $this->getDoctrine()->getManager()->getRepository(\BackBundle\Entity\Operation::class)->filterInActivationOpertaionByCaisse($caisse->getId());
        return $this->render('back/caisse/view.html.twig',
                array(
                'caisse' => $caisse,
                'active_operation' => $active_operation,
                'done_operation' => $done_operation,
                'in_activation_operation' => $in_activation_operation
        ));
    }

    /**
     * @Route("/detail/show", name="caisse_show")
     */
    public function showAction(Request $request)
    {
        $dates   = array('startdate' => $request->get('startdate'), 'enddate' => $request->get('enddate'));
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $caisses = $this->get('filter.manager')->FilterStatisticsCaisses($dates, $this->getUser()->getId());
        }else{
            $caisses = $this->get('filter.manager')->FilterStatisticsCaisses($dates, null);
        }
        return $this->render('back/caisse/show.html.twig',
                array(
                'caisses' => $caisses
        ));
    }

    /**
     * @Route("/index/bank/{id}", options={"expose"=true}, name="caisse_list_bank")
     */
    public function bankAction(\BackBundle\Entity\Bank $bank)
    {
        return $this->render('back/caisse/bank.html.twig',
                array(
                'caisses' => $bank->getCaisses()
        ));
    }
}