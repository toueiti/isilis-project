<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BackBundle\Entity\VisualWeb;
use BackBundle\Form\VisualWebType;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * @Route("/visualweb")
 */
class VisualWebController extends Controller
{
    /**
     * @Route("/new", options={"expose"=true}, name="vweb_new")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $visuelWeb = new VisualWeb();
        $formVweb = $this->createForm(VisualWebType::class, $visuelWeb);
        $formVweb->handleRequest($request);

        $em->persist($visuelWeb);
        $em->flush();
        return new Response($visuelWeb->getId());
        //return new Response(0);
    }

    /**
     * @Route("/delete/{id}", options={"expose"=true}, name="vweb_delete")
     */
    public function deleteAction(VisualWeb $visuelWeb)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($visuelWeb);
        $em->flush();
        return new Response(1);
    }

    /**
     * @Route("delete_vweb/{id}", name="delete_vweb")
     */
    public function delete_vwebAction(Request $request, VisualWeb $visuelWeb)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $em->remove($visuelWeb);
        $em->flush();
        return new RedirectResponse($referer);
    }

}
