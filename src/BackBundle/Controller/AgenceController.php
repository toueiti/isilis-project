<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Agence;
use BackBundle\Form\AgenceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/agence")
 */
class AgenceController extends Controller
{

    /**
     * @Route("/", name="agence")
     */
    public function indexAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $agences = $this->getDoctrine()->getManager()->getRepository(Agence::class)->findBy(
                array('commercial' => $this->getUser()->getCommercial()->getId()
                ));
        }else{
            $agences = $this->getDoctrine()->getManager()->getRepository(Agence::class)->findAll();
        }
        return $this->render('back/agence/index.html.twig',
            array(
                'agences' => $agences
            ));
    }

    /**
     * @Route("/archive", name="archive_agence")
     */
    public function archiveAction()
    {
        $agences = $this->getDoctrine()->getManager()->getRepository(Agence::class)->findAll();
        return $this->render('back/agence/index.html.twig',
            array(
                'agences' => $agences
            ));
    }

    /**
     * @Route("/new", options={"expose"=true}, name="agence_new")
     */
    public function newAction(Request $request)
    {
        $agence = new Agence();
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $agence->setCommercial($this->getUser()->getCommercial());
        }
        $form = $this->createForm(AgenceType::class, $agence);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $contact = $agence->getContact();
                $user = $contact->getUser();
                $validator = $this->get('validator');
                $errors = $validator->validate($user);
                if (count($errors) > 0) {
                    $this->get('session')->getFlashBag()->add('error',
                        'Email déjà utilisé !');
                } else {
                    $this->get('agence.manager')->createAgence($agence);
                    $this->get('session')->getFlashBag()->add('success',
                        'Votre inscription est confirmée.<br />
Vous allez recevoir un email accompagné de votre identifiant et votre mot de passe de connexion.<br />
L\'équipe ISILIS');
                    return $this->redirect($this->generateUrl('agence'));
                }
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/agence/new.html.twig',
            array(
                'form' => $form->createView(),
                'agence' => $agence
            ));
    }

    /**
     * @Route("/{id}/edit", name="edit_agence")
     */
    public function editAction(Agence $agence, Request $request)
    {
        $form = $this->createForm(AgenceType::class, $agence);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('agence.manager')->editAgence($agence);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès');
                return $this->redirect($this->generateUrl('agence'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/agence/edit.html.twig',
            array(
                'form' => $form->createView(),
                'agence' => $agence
            ));
    }

    /**
     * @Route("/{id}/inactive", name="inactive_agence")
     * @Method({"GET", "POST"})
     */
    public function inactiveAction(Agence $agence)
    {
        $em = $this->getDoctrine()->getManager();
        $agence->setActive(Agence::STATUS_INACTIVE);
        $user = $agence->getContact()->getUser();
        $user->setEnabled(false);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Agence de com suspendu.');
        // $em->persist($agence);
        $em->flush();
        return $this->redirect($this->generateUrl('agence'));
    }

    /**
     * @Route("/id/delete/", name="delete_agence")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Agence $agence)
    {
        $em = $this->getDoctrine()->getManager();
        $agence->setActive(Agence::STATUS_ARCHIVE);
        $this->get('session')->getFlashBag()->add('success', "Agence de com supprimée");
        $em->flush();
        return $this->redirect($this->generateUrl('agence'));
    }

    /**
     * @Route("/{id}/activate", name="activate_agence")
     * @Method({"GET", "POST"})
     */
    public function activateAction(Agence $agence)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $agence->getContact()->getUser();
        $user->setEnabled(true);
        $em->flush();
        $agence->setActive(Agence::STATUS_ACTIVE);
        $this->get('session')->getFlashBag()->add('success',
            "Agence de com actvée.");
        // $em->persist($agence);
        $em->flush();
        return $this->redirect($this->generateUrl('agence'));
    }
}