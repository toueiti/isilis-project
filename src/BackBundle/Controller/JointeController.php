<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BackBundle\Entity\Jointe;

/**
 * @author slim
 * @Route("/jointe")
 */
class JointeController extends Controller
{

    /**
     * @Route("/new", name="jointe_new")
     */
    public function newAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em         = $this->getDoctrine()->getManager();
            $files      = $request->files;
            $attachfile = $files->get('myfile');
            $jointe     = new jointe();
            $jointe->setFile($attachfile);
            $jointe->upload();
            $em->persist($jointe);
            $em->flush();
            $output     = array(
                'id' => $jointe->getId(),
                'name' => $jointe->getName()
            );
            return new Response(json_encode($output));
        }
    }

    /**
     * @Route("/delete", name="jointe_delete")
     */
    public function deleteAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $id     = ($request->get('id') !== NULL) ? $request->get('id') : 0;
        $jointe = $em->find(Jointe::class, $id);
        try {
            $em->remove($jointe);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return new Response(1);
    }
}