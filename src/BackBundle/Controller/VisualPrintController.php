<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BackBundle\Entity\VisualPrint;
use BackBundle\Form\VisualPrintType;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * @Route("/vprint")
 */
class VisualPrintController extends Controller
{
    /**
     * @Route("/new", options={"expose"=true}, name="vprint_new")
     */
    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $visuelPrint = new VisualPrint();
        $formVweb = $this->createForm(VisualPrintType::class, $visuelPrint);
        $formVweb->handleRequest($request);
        
        $em->persist($visuelPrint);
        $em->flush();
        return new Response($visuelPrint->getId());        
        //return new Response(0);
    }
    
    /**
     * @Route("/delete/{id}", options={"expose"=true}, name="vprint_delete")
     */
    public function deleteAction(VisualPrint $visuelPrint) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($visuelPrint);
        $em->flush();
        return new Response(1);
    }

    /**
     * @Route("/delete_vprint/{id}", name="delete_vprint")
     */
    public function delete_vprintAction(Request $request, VisualPrint $visuelPrint)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $em->remove($visuelPrint);
        $em->flush();
        return new RedirectResponse($referer);
    }
}
