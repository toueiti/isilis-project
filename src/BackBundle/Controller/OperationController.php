<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Message;
use BackBundle\Entity\Operation;
use BackBundle\Entity\VisualPrint;
use BackBundle\Entity\VisualWeb;
use BackBundle\Form\MessageType;
use BackBundle\Form\OperationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * @Route("/operation")
 *
 */
class OperationController extends Controller
{

    /**
     * @Route("/validation", name="operation_validation")
     */
    public function validationAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('operation.manager')->filterOpertaionInActivation($id
                = null, $this->getUser()->getCommercial()->getId());
        } else {
            $banks = $this->get('operation.manager')->filterOpertaionInActivation($id
                = null, null);
        }
        return $this->render('back/operation/validation.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/active", name="operation_active")
     */
    public function activeAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('operation.manager')->filterOpertaionActive($id = null,
                $this->getUser()->getCommercial()->getId() );
        }else{
            $banks = $this->get('operation.manager')->filterOpertaionActive($id = null, null);
        }
        return $this->render('back/operation/active.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/done", name="operation_done")
     */
    public function doneAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('operation.manager')->filterOpertaionDone($id = null,
                $this->getUser()->getCommercial()->getId() );
        }else{
            $banks = $this->get('operation.manager')->filterOpertaionDone($id = null, null);
        }
        return $this->render('back/operation/done.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/bank/active/{id}", options={"expose"=true}, name="filter_active_operation")
     */
    public function bankOperationActiveAction(\BackBundle\Entity\Bank $bank,
                                              Request $request)
    {
        $id = $bank->getId();
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('operation.manager')->filterOpertaionActive($id,$this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('operation.manager')->filterOpertaionActive($id, null);
        }
        return $this->render('back/operation/filter/active.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/bank/done/{id}", options={"expose"=true}, name="filter_done_operation")
     */
    public function bankOperationDoneAction(\BackBundle\Entity\Bank $bank,
                                            Request $request)
    {
        $id = $bank->getId();
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('operation.manager')->filterOpertaionDone($id, $this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('operation.manager')->filterOpertaionDone($id, null);
        }
        return $this->render('back/operation/filter/done.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/bank/activation/{id}", options={"expose"=true}, name="filter_activation_operation")
     */
    public function bankOperationActivationAction(\BackBundle\Entity\Bank $bank,
                                                  Request $request)
    {
        $id = $bank->getId();
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('operation.manager')->filterOpertaionInActivation($id,  $this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('operation.manager')->filterOpertaionInActivation($id, null);
        }
        return $this->render('back/operation/filter/validation.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/new", name="operation_new")
     */
    public function newAction(Request $request)
    {
        $operation = new Operation();
        $operation->addVisualPrint(new VisualPrint());
        $operation->addVisualWeb(new VisualWeb());
        //$form = $this->createForm(OperationType::class, $operation);
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $commercial_id = $this->getUser()->getCommercial()->getId();
            $form = $this->createForm(OperationType::class, $operation,array('commercial_id'=> $commercial_id));
        }else{
            $form = $this->createForm(OperationType::class, $operation,array('commercial_id'=> 0));
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('operation.manager')->createOperation($operation);
                $this->get('session')->getFlashBag()->add('success',
                    'Opération ajoutée avec succès.');
                return $this->redirect($this->generateUrl('operation_validation'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/operation/new.html.twig',
            array(
                'form' => $form->createView()
            ));
    }

    /**
     * @Route("/{id}/edit", name="operation_edit")
     */
    public function editAction(Request $request, Operation $operation)
    {
        //$form = $this->createForm(OperationType::class, $operation);
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $commercial_id = $this->getUser()->getCommercial()->getId();
            $form = $this->createForm(OperationType::class, $operation,array('commercial_id'=> $commercial_id,'id_bank'=>$operation->getBank()->getId()));
        }else{
            $form = $this->createForm(OperationType::class, $operation,array('commercial_id'=> 0,'id_bank'=>$operation->getBank()->getId()));
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('operation.manager')->editOperation($operation);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès.');
                $referer = $request->headers->get('referer');
                return new RedirectResponse($referer);
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !' . $form->getErrors());
            }
        }
        return $this->render('back/operation/edit.html.twig',
            array(
                'form' => $form->createView(),
                'operation' => $operation
            ));
    }

    /**
     * @Route("/{id}/activate", name="operation_activate")
     */
    public function activateAction(request $request, Operation $opertaion)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $opertaion->setActive(Operation::STATUS_ACTIVE);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',
            'Opération activè.');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/{id}/suspendu", name="operation_suspend")
     */
    public function suspendAction(request $request, Operation $opertaion)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $opertaion->setActive(Operation::STATUS_IN_VALIDATION);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',
            'Opération desactivé.');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/{id}/archive", name="operation_archive")
     */
    public function successAction(request $request, Operation $opertaion)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $opertaion->setActive(Operation::STATUS_ARCHIVE);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',
            'Opération supprimée.');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/{id}/reactive", name="operation_reactive")
     */
    public function reactiveAction(Request $request, Operation $operation)
    {
        if (count($operation->getVisualPrints()) === 0) {
            $operation->addVisualPrint(new VisualPrint());
        }
        if (count($operation->getVisualWebs()) === 0) {
            $operation->addVisualWeb(new VisualWeb());
        }
        $operation->setEndDate(null);
        $operation->setStartDate(null);
        $operation->setStartDateOnline(null);
        $operation->setEndDateOnline(null);
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $commercial_id = $this->getUser()->getCommercial()->getId();
            $form = $this->createForm(OperationType::class, $operation,array('commercial_id'=> $commercial_id,'id_bank'=>$operation->getBank()->getId()));
        }else{
            $form = $this->createForm(OperationType::class, $operation,array('commercial_id'=> 0,'id_bank'=>$operation->getBank()->getId()));
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('operation.manager')->editOperation($operation);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès.');
                return $this->redirect($this->generateUrl('operation_new'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/operation/reactive.html.twig',
            array(
                'form' => $form->createView(),
                'operation' => $operation
            ));
    }

    /**
     * @Route("/detail/{id}", name="operation_detail")
     */
    public function detailAction(Operation $operation)
    {
        $em = $this->getDoctrine()->getManager();
        $caisseMessages = $em->getRepository(Message::class)->getCaisseMessagesByOperation($operation);
        $agenceMessages = $em->getRepository(Message::class)->getAgenceMessagesByOperation($operation);
        foreach ($caisseMessages as $item) {
            if (!$item->getSeenAdmin()) {
                $item->setSeenAdmin(true);
                $em->persist($item);
            }
            try {
                $em->flush();
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
        foreach ($agenceMessages as $item) {
            if (!$item->getSeenAdmin()) {
                $item->setSeenAdmin(true);
                $em->persist($item);
            }
            try {
                $em->flush();
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
        $message = new Message();
        $message->setSource($message->getADMIN_SEND());
        $message->setOperation($operation);
        $message->setUser($this->getUser());
        $formCaisse = $this->createForm(MessageType::class, $message);
        $formAgence = $this->createForm(MessageType::class, $message);
        return $this->render('back/operation/detail.html.twig',
            array(
                'operation' => $operation,
                'caisse_msg' => $caisseMessages,
                'agence_msg' => $agenceMessages,
                'form_caisse' => $formCaisse->createView(),
                'form_agence' => $formAgence->createView()
            ));
    }

    /**
     * @Route("/show", name="operation_show")
     */
    public function showAction(Request $request)
    {
        $dates = array('startdate' => $request->get('startdate'), 'enddate' => $request->get('enddate'));
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $operations = $this->get('filter.manager')->FilterStatisticsOperations($dates, $this->getUser()->getCommercial()->getId());
        }else{
            $operations = $this->get('filter.manager')->FilterStatisticsOperations($dates, null);
        }
        return $this->render('back/operation/show.html.twig',
            array(
                'operations' => $operations
            ));
    }

    /**
     * @Route("/ordred/detail/show", name="operation_commandees_show")
     */
    public function showOrdredAction(Request $request)
    {
        $dates = array('startdate' => $request->get('startdate'), 'enddate' => $request->get('enddate'));
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $ordred_operation = $this->get('filter.manager')->FilterStatisticsOperationOrdred($dates, $status = $request->get('status'), $this->getUser()->getCommercial()->getId());
        }else{
            $ordred_operation = $this->get('filter.manager')->FilterStatisticsOperationOrdred($dates, $status = $request->get('status'), null);
        }
        if ($request->get('status') == 'active') {
            return $this->render('back/operation/show_ordred.html.twig',
                array(
                    'ordred_operation' => $ordred_operation,
                ));
        } else if($request->get('status') == 'done'){
            return $this->render('back/operation/show_ordred_done.html.twig',
                array(
                    'ordred_operation' => $ordred_operation,
                ));
        }

    }
}