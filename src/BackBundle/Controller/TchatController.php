<?php
/**
 * Created by PhpStorm.
 * User: kifcode
 * Date: 03/07/2018
 * Time: 10:56
 */

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class TchatController extends Controller
{

    /**
     * @Route("/tchat", name="tchat")
     */
    public function newAction(Request $request)
    {
        $referer = $request->headers->get('referer');
        $campany = $request->get('campany');
        $name = $request->get('name');
        $firstname = $request->get('firstname');
        $email  = $request->get('email');
        $object = $request->get('object');
        $message = $request->get('message');

        $message = (new \Swift_Message('ISILIS Plateforme'))
            ->setFrom($email)
            ->setTo('info@dot2com.com')
            ->setBcc('toueiti@yahoo.fr')
            ->setBody(
                $this->container->get('templating')->render(
                    'back/tchat/tchat.html.twig'
                ), 'text/html'
            );
       // $this->get('mailer')->send($message);

        return new RedirectResponse($referer);
    }

}