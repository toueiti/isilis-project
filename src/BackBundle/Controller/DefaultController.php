<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Form\FilterFormType;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="back_home")
     */
    public function indexAction(Request $request)
    {
        $commercial_id = null;
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $commercial_id = $this->getUser()->getCommercial()->getId();
        }
        $formData = $request->get('filter_form');
        if (is_null($formData['startdate']) && is_null($formData['enddate'])) {
            $serializer         = new Serializer(array(new DateTimeNormalizer()));
            $dates['startdate'] = $serializer->normalize(new \DateTime('now -1 month'),
                NULL, array(DateTimeNormalizer::FORMAT_KEY => 'Y/m/d'));
            $dates['enddate']   = $serializer->normalize(new \DateTime('now +1 day'),
                NULL, array(DateTimeNormalizer::FORMAT_KEY => 'Y/m/d'));
            $banks              = $this->get('filter.manager')->FilterStatisticsBanks($dates, $commercial_id);
            $caisses            = $this->get('filter.manager')->FilterStatisticsCaisses($dates, $commercial_id);
            $campaigns          = $this->get('filter.manager')->FilterStatisticsCampaigns($dates, $commercial_id);
            $operations         = $this->get('filter.manager')->FilterStatisticsOperations($dates, $commercial_id);
            $users              = $this->get('connection.history.manager')->filterConnectionHistory($dates, $commercial_id);

            $ordred_operation_active = $this->get('filter.manager')->FilterStatisticsOperationOrdred($dates, $status = 'active', $commercial_id);
            $ordred_operation_done = $this->get('filter.manager')->FilterStatisticsOperationOrdred($dates, $status = 'done', $commercial_id);
        } else {
            $start_tmp_dates  = explode('/', $formData['startdate']);
            $stardate         = $start_tmp_dates[2].'/'.$start_tmp_dates[1].'/'.$start_tmp_dates[0];
            $end_tmp_dates    = explode('/', $formData['enddate']);
            $enddate          = $end_tmp_dates[2].'/'.$end_tmp_dates[1].'/'.$end_tmp_dates[0];
            $enddate =  \DateTime::createFromFormat('Y/m/d', $enddate)->modify('+1 day')->format('Y/m/d');
            $dates            = array('startdate' => $stardate, 'enddate' => $enddate);

            $banks            = $this->get('filter.manager')->FilterStatisticsBanks($dates, $commercial_id);
            $caisses          = $this->get('filter.manager')->FilterStatisticsCaisses($dates, $commercial_id);
            $campaigns        = $this->get('filter.manager')->FilterStatisticsCampaigns($dates, $commercial_id);
            $operations       = $this->get('filter.manager')->FilterStatisticsOperations($dates, $commercial_id);
            $users            = $this->get('connection.history.manager')->filterConnectionHistory($dates, $commercial_id);
            $ordred_operation_active = $this->get('filter.manager')->FilterStatisticsOperationOrdred($dates, $status = 'active', $commercial_id);
            $ordred_operation_done = $this->get('filter.manager')->FilterStatisticsOperationOrdred($dates, $status = 'done', $commercial_id);
        }
        return $this->render('back/filter/filter.html.twig',
                array(
                'filter_form' => $formData,
                'banks' => $banks,
                'caisses' => $caisses,
                'campaigns' => $campaigns,
                'operations' => $operations,
                'users' => $users,
                'dates' => $dates,
                'ordred_operation' => $ordred_operation_active,
                 'done_ordred_operation'=>$ordred_operation_done
        ));
    }

    public function filterFormAction(Request $request)
    {
        $form = $this->createForm(FilterFormType::class);
        $form->handleRequest($request);
        return $this->render('back/filter/form.html.twig',
                array('filter_form' => $form->createView()));
    }
}