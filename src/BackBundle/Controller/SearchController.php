<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use BackBundle\Form\SearchFormType;

/**
 * @Route("/search")
 */
class SearchController extends Controller
{

    public function searchFormAction(Request $request)
    {
        $name = $request->get('search_form');
        $form = $this->createForm(SearchFormType::class,
            array(
            'data' => $name
        ));
        $form->handleRequest($request);
        return $this->render('back/search/form.html.twig',
                array('search_form' => $form->createView()));
    }

    /**
     * @Route("/result/", name="search")
     */
    public function searchAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $form       = $request->get('search_form');
        $name       = trim($form['name']);
        if($this->isGranted('ROLE_COMERCIAL')){
            $operations = $this->get('search.manager')->getSearchResult($name, $this->getUser()->getCommercial()->getId());
            $campaign   = $em->getRepository(\BackBundle\Entity\Campaign::class)->getCampaigns($name, $this->getUser()->getCommercial()->getId());
        }else{
            $operations = $this->get('search.manager')->getSearchResult($name, null);
            $campaign   = $em->getRepository(\BackBundle\Entity\Campaign::class)->getCampaigns($name, null);
        }

        if (empty($operations)) {
            $this->get('session')->getFlashBag()->add('success',
                'Aucun résultat correspond a votre recherche.');
        }
        return $this->render('back/search/search.html.twig',
                array(
                'operations' => $operations,
                'search_form' => $form,
                'camp' => $campaign
        ));
    }
}