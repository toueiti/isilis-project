<?php

namespace BackBundle\Controller;

use BackBundle\Entity\ObjetCompaign;
use BackBundle\Form\ObjetCompaignType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/objectif/campaign")
 */
class ObjectifCampaignController extends Controller
{

    /**
     * @Route("/", name="index_object_campaign")
     */
    public function indexAction()
    {
            $objectif_campaign = $this->getDoctrine()->getManager()->getRepository(ObjetCompaign::class)->findBy(
                array('is_ative' => 1));
        return $this->render('/back/objectif-campaign/index.html.twig',
            array(
                'objectif_campaign' => $objectif_campaign
            ));
    }

    /**
     * @Route("delete/{id}", name="objet_campaign_delete")
     */
    public function deleteObjetCampaignAction(Request $request, ObjetCompaign $objetCompaign)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $objetCompaign->setIsActive(0);
        $em->persist($objetCompaign);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',
            'objectif campagne supprimées.');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/new", name="objet_campaign_new")
     */
    public function newAction(Request $request)
    {
        $objectif_campaign = new ObjetCompaign();
        $form = $this->createForm(ObjetCompaignType::class,
            $objectif_campaign);
//        if($this->isGranted('ROLE_COMERCIAL')) {
//            $objectif_campaign->setCommercial($this->getUser()->getCommercial());
//        }
            $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('objectif.campaign.manager')->createObjectifCampaign($objectif_campaign);
                $this->get('session')->getFlashBag()->add('success',
                    'objectif campagne ajoutée avec succès.');
                return $this->redirect($this->generateUrl('index_object_campaign'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire.');
            }
        }
        return $this->render('/back/objectif-campaign/new.html.twig',
            array(
                'form' => $form->createView(),
                'objectif_campaign' => $objectif_campaign
            ));
    }

    /**
     * @Route("/{id}/edit", name="objet_campaign_edit")
     */
    public function editAction(Request $request,
                               ObjetCompaign $objectif_campaign)
    {
        $form = $this->createForm(ObjetCompaignType::class, $objectif_campaign);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('objectif.campaign.manager')->editObjectifCampaign($objectif_campaign);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès.');
                return $this->redirect($this->generateUrl('index_object_campaign'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire.');
            }
        }
        return $this->render('/back/objectif-campaign/edit.html.twig',
            array(
                'form' => $form->createView(),
                'objectif_campaign' => $objectif_campaign
            ));
    }
}