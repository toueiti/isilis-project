<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Bank;
use BackBundle\Entity\VisualPrint;
use BackBundle\Entity\VisualWeb;
use BackBundle\Form\BankType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use BackBundle\Entity\Caisse;

/**
 * @Route("/banque")
 */
class BankController extends Controller
{

    /**
     * @Route("/", name="bank")
     */
    public function indexAction()
    {
        $em    = $this->getDoctrine()->getManager();
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $em->getRepository(Bank::class)->findBy(array('commercial'=>$this->getUser()->getCommercial()));
        }else{
            $banks = $em->getRepository(Bank::class)->findAll();
        }
        return $this->render('back/bank/index.html.twig',
                array(
                'banks' => $banks
        ));
    }

    /**
     * @Route("/new", name="bank_new")
     */
    public function newAction(Request $request)
    {
        $bank = new Bank();
        $bank->addVisualWeb(new VisualWeb());
        $bank->addVisualPrint(new VisualPrint());
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $bank->setCommercial($this->getUser()->getCommercial());
        }
        $form = $this->createForm(BankType::class, $bank);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('bank.manager')->createBank($bank);
                $this->get('session')->getFlashBag()->add('success',
                    'Banque ajoutée avec succès.');
                return $this->redirect($this->generateUrl('bank'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/bank/new.html.twig',
                array(
                'form' => $form->createView()
        ));
    }

    /**
     * @Route("/{id}/edit", name="edit_bank")
     */
    public function editAction(Bank $bank, Request $request)
    {
        $form = $this->createForm(BankType::class, $bank);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('bank.manager')->editBank($bank);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès.');
                return $this->redirect($this->generateUrl('bank'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/bank/edit.html.twig',
                array(
                'form' => $form->createView(),
                'bank' => $bank
        ));
    }

    /**
     * @Route("/{id}/inactive", name="inactive_bank")
     * @Method({"GET", "POST"})
     */
    public function inactiveAction(Bank $bank)
    {
        $em = $this->getDoctrine()->getManager();
        $bank->setActive(Bank::STATUS_INACTIVE);
        foreach ($bank->getCaisses() as $caisse) {
            $caisse->setActive(Caisse::STATUS_SUSPENDU);
            $user = $caisse->getContact()->getUser();
            $user->setEnabled(false);
            $em->persist($caisse);
            $em->persist($user);
        }
        $em->persist($bank);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Banque desactivée.');
        return $this->redirect($this->generateUrl('bank'));
    }

    /**
     * @Route("/{id}/activate", name="activate_bank")
     * @Method({"GET", "POST"})
     */
    public function activeAction(Bank $bank)
    {
        $em = $this->getDoctrine()->getManager();
        $bank->setActive(Bank::STATUS_ACTIVE);
        foreach ($bank->getCaisses() as $caisse) {
            $caisse->setActive(Caisse::STATUS_ACTIVE);
            $user = $caisse->getContact()->getUser();
            $user->setEnabled(true);
            $em->persist($caisse);
            $em->persist($user);
        }
        $em->persist($bank);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Banque activée.');
        return $this->redirect($this->generateUrl('bank'));
    }

    /**
     * @Route("/{id}/archive", name="archive_bank")
     * @Method({"GET", "POST"})
     */
    public function archiveAction(Bank $bank)
    {
        $em = $this->getDoctrine()->getManager();
        $bank->setActive(Bank::STATUS_ARCHIVE);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Banque archivée.');
        return $this->redirect($this->generateUrl('bank'));
    }

    /**
     * @Route("/detail/show", name="bank_show")
     */
    public function ShowAction(Request $request)
    {
        $dates = array('startdate' => $request->get('startdate'), 'enddate' => $request->get('enddate'));
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('filter.manager')->FilterStatisticsBanks($dates, $this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('filter.manager')->FilterStatisticsBanks($dates, null);
        }
        return $this->render('back/bank/show.html.twig',
                array(
                'banks' => $banks,
                'filter_form' => $dates
        ));
    }
}