<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Bank;
use BackBundle\Entity\Campaign;
use BackBundle\Entity\VisualPrint;
use BackBundle\Entity\VisualWeb;
use BackBundle\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * @Route("/campaign")
 */
class CampaignController extends Controller
{

    /**
     * @Route("/active", name="campaign_active")
     */
    public function indexAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('campaign.manager')->FilterActiveCompaign($id = null, $this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('campaign.manager')->FilterActiveCompaign($id = null, null);
        }
        return $this->render('back/campaign/active.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/done", name="campaign_done")
     */
    public function CompaignDoneAction()
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('campaign.manager')->filterDoneCompaign($id = null, $this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('campaign.manager')->filterDoneCompaign($id = null, null);
        }
        return $this->render('back/campaign/done.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/bank/{id}", options={"expose"=true}, name="filter_active_campaign")
     */
    public function campaignBankAction(Bank $bank, Request $request)
    {
        $id = $request->get('id');
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('campaign.manager')->FilterActiveCompaign($id,  $this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('campaign.manager')->FilterActiveCompaign($id, null);
        }
        return $this->render('back/campaign/filter_active.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/bank/done/{id}", options={"expose"=true}, name="filter_done_campaign")
     */
    public function doneCampaignBankAction(Bank $bank, Request $request)
    {
        $id = $request->get('id');
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $banks = $this->get('campaign.manager')->filterDoneCompaign($id, $this->getUser()->getCommercial()->getId());
        }else{
            $banks = $this->get('campaign.manager')->filterDoneCompaign($id, null);
        }
        return $this->render('back/campaign/filter_done.html.twig',
            array(
                'banks' => $banks
            ));
    }

    /**
     * @Route("/new", name="new_campaign")
     */
    public function newAction(Request $request)
    {
        $campaign = new Campaign();
        $campaign->addVisualWeb(new VisualWeb());
        $campaign->addVisualPrint(new VisualPrint());
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $commercial_id = $this->getUser()->getCommercial()->getId();
            $form = $this->createForm(CampaignType::class, $campaign,array('commercial_id'=> $commercial_id));
        }else{
            $form = $this->createForm(CampaignType::class, $campaign,array('commercial_id'=> 0));
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('campaign.manager')->createCampaign($campaign);
                $this->get('session')->getFlashBag()->add('success',
                    'Campagne ajoutée avec succès.');
                return $this->redirect($this->generateUrl('new_campaign'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/campaign/new.html.twig',
            array(
                'form' => $form->createView()
            ));
    }

    /**
     * @Route("/{id}/edit", name="edit_campaign")
     */
    public function editAction(Request $request, Campaign $campaign)
    {
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $commercial_id = $this->getUser()->getCommercial()->getId();
            $form = $this->createForm(CampaignType::class, $campaign,array('commercial_id'=> $commercial_id));
        }else{
            $form = $this->createForm(CampaignType::class, $campaign,array('commercial_id'=> 0));
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('campaign.manager')->editCampaign($campaign);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès.');
                return $this->redirect($this->generateUrl('campaign_active'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/campaign/edit.html.twig',
            array(
                'form' => $form->createView(),
                'campaign' => $campaign
            ));
    }

    /**
     * @Route("/{id}/activer", name="active_campaign")
     * @Method({"GET"})
     */
    public function activeAction(request $request, Campaign $campaign)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $campaign->setActive(Campaign::COMPAIGN_STATUS_ACTIVE);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',
            'Campagne activées.');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/{id}/archive", name="archive_campaign")
     * @Method({"GET"})
     */
    public function archiveAction(request $request, Campaign $campaign)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $campaign->setActive(Campaign::COMPAIGN_STATUS_ARCHIVE);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',
            'Campagne supprimée.');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/{id}/suspendu", name="susupend_campaign")
     * @Method({"GET"})
     */
    public function suspendAction(request $request, Campaign $campaign)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $campaign->setActive(Campaign::COMPAIGN_STATUS_DONE);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Campagne suspendu.');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/{id}/recative", name="reactive_campaign")
     * @Method({"GET"})
     */
    public function ractiveAction(Request $request, Campaign $campaign)
    {
        $campaign->setStartDate(NULL);
        $campaign->setEndDate(NULL);
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->get('campaign.manager')->editCampaign($campaign);
                $this->get('session')->getFlashBag()->add('success',
                    'Mise a jour avec succès.');
                return $this->redirect($this->generateUrl('campaign_active'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('back/campaign/reactive.html.twig',
            array(
                'form' => $form->createView(),
                'campaign' => $campaign
            ));
    }

    /**
     * @Route("/detail/show", name="campaign_show")
     */
    public function showAction(Request $request)
    {
        $dates = array('startdate' => $request->get('startdate'), 'enddate' => $request->get('enddate'));
        if ($this->isGranted('ROLE_COMERCIAL')) {
            $campaigns = $this->get('filter.manager')->FilterStatisticsCampaigns($dates, $this->getUser()->getCommercial()->getId());
        }else{
            $campaigns = $this->get('filter.manager')->FilterStatisticsCampaigns($dates, null);
        }
        return $this->render('back/campaign/show.html.twig',
            array(
                'campaigns' => $campaigns
            ));
    }

    /**
     * @Route("/index/bank/{id}", options={"expose"=true}, name="campaign_list_bank")
     */
    public function bankAction(\BackBundle\Entity\Bank $bank)
    {
        return $this->render('back/campaign/bank.html.twig',
            array(
                'campaigns' => $bank->getCampaign()
            ));
    }
}