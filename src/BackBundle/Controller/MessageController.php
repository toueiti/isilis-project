<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Entity\Operation;
use BackBundle\Entity\Message;
use BackBundle\Form\MessageType;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author slim
 * @Route("/message")
 */
class MessageController extends Controller
{

    /**
     * @Route("/new/{id}/{dest}", name="message_new")
     */
    public function newAction(Request $request, Operation $operation, $dest)
    {
        $message = new Message();
        $message->setSource($message->getADMIN_SEND());
        $message->setDest($dest);
        if ($dest == Message::CAISSE_RECIV) {
            $message->setValideCaisse(TRUE);
        }
        if ($dest == Message::AGENCE_RECIV) {
            $message->setValideAgence(TRUE);
        }
        $message->setOperation($operation);
        $message->setUser($this->getUser());
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    $em     = $this->getDoctrine()->getManager();
                    $pieces = explode(',', $request->get('piece'));
                    foreach ($pieces as $idpiece) {
                        $jointe = $em->find(\BackBundle\Entity\Jointe::class,
                            $idpiece);
                        if (is_object($jointe)) {
                            $jointe->setMessage($message);
                            $em->persist($jointe);
                        }
                    }
                    $em->persist($message);
                    $em->flush();
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
                $this->container->get('message.manager')->sendNotif($message);
                $referer = $request->headers->get('referer');
                return $this->redirect($referer);
//                return $this->render('back/message/new.html.twig', array(
//                    'operation'=>$operation,
//                    'message'=>$message
//                ));
            } else {
                return new Response('error');
            }
//            $referer = $request->headers->get('referer');
//            return $this->redirect($referer);
        }
        return new Response('Erreur ! 404 !');
    }

    /**
     * @Route("/valide/{id}/{dest}", name="message_validate")
     */
    public function validAction(Request $request, Message $message, $dest)
    {
        if ($dest == Message::CAISSE_RECIV) {
            $message->setValideCaisse(TRUE);
            $message->setValidatedAt(new \DateTime('now'));
        }
        if ($dest == Message::AGENCE_RECIV) {
            $message->setValideAgence(TRUE);
            $message->setValidatedAt(new \DateTime('now'));
        }
        $em = $this->getDoctrine()->getManager();
        try {
            $em->persist($message);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * @Route("/delete/{id}", name="message_delete")
     */
    public function deleteAction(Request $request, Message $message)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            foreach ($message->getJointes() as $item) {
                $em->remove($item);
            }
            $em->remove($message);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * @Route("/edit/{id}", name="message_edit", options={"expose"=true})
     */
    public function editAction(Request $request, Message $message)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        if($message){
            $message->setBody($data['messageBody']);
            $em->persist($message);
            $em->flush();
        }
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }
}