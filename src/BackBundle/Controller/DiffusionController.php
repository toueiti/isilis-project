<?php

namespace BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Entity\Diffusion;
use BackBundle\Form\DiffusionType;

/**
 * @Route("/diffusion")
 */
class DiffusionController extends Controller
{

    /**
     * @Route("/", name="diffusion")
     */
    public function indexAction()
    {
        $em        = $this->getDoctrine()->getManager();
        $list      = $em->getRepository(Diffusion::class)->findAll();
        $diffusion = new Diffusion();
        $form      = $this->createForm(DiffusionType::class, $diffusion);
        return $this->render('back/diffusion/index.html.twig',
                array(
                'list' => $list,
                'form' => $form->createView(),
                'diffusion' => $diffusion
        ));
    }

    /**
     * @Route("/new", name="diffusion_new")
     */
    public function newAction(Request $request)
    {
        $diffusion = new Diffusion();
        $form      = $this->createForm(DiffusionType::class, $diffusion);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($diffusion);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success',
                    'Email ajouté avec succès.');
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->redirect($this->generateUrl('diffusion'));
    }

    /**
     * @Route("/delete/{id}", name="diffusion_delete")
     */
    public function deleteAction(Diffusion $diffusion)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($diffusion);
        $em->flush();
        return $this->redirect($this->generateUrl('diffusion'));
    }
}