<?php

namespace BackBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use BackBundle\Entity\ObjetOperation;

class ObjectifOperationManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function createObjectifOperation($objectifOperation)
    {
        try {
            $em = $this->container->get('doctrine.orm.entity_manager');
            $objectifOperation->setIsActive(1);
            $em->persist($objectifOperation);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function editObjectifOperation($objectifOperation)
    {
        try {
            $em = $this->container->get('doctrine.orm.entity_manager');
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}