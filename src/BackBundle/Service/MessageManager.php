<?php

namespace BackBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Description of MessageManager
 *
 * @author slim
 */
class MessageManager 
{
    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }
    
    public function getNewMessages($commercial) {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $result = $em->getRepository(\BackBundle\Entity\Message::class)->getNewMessages($commercial);
        $output = array();
        $nbmsg = 0;
        foreach ($result as $item){
            $row = array(
                'operation'=>$item[0],
                'nbmsg'=>$item['nbmsg']
            );
            $nbmsg += $item['nbmsg'];
            $output [] = $row;
        }
        return array(
            'operations'=>$output,
            'nbmsg'=>$nbmsg
        );
    }

    /**
     * envoyer notification
     */
    public function sendNotif(\BackBundle\Entity\Message $msg)
    {
        $caisse = $msg->getOperation()->getCaisse();
        $commercial = $caisse->getCommercial();
        $user = $commercial->getUser();
        if($caisse->getIsNotif() == false){
            /************************************** */
            $message = (new \Swift_Message('ISILIS Plateforme'))
                ->setFrom($this->container->getParameter('mailer_from'))
                ->setTo($user->getEmail())
                ->setBcc('toueiti@yahoo.fr')
                ->setBody(
                    $this->container->get('templating')->render(
                        'back/email/notfi_commercial.twig',
                        array('contact' => $commercial,
                            'caisse'=>$caisse,
                            'msg'=>$msg)
                    ), 'text/html'
                );
            //$this->container->get('mailer')->send($message);
            /*         * ******************** end email ****************** */
        }
    }
}
