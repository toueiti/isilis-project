<?php

namespace BackBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use BackBundle\Entity\Caisse;

class CaisseManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function createCaisse(Caisse $caisse)
    {
        $em       = $this->container->get('doctrine.orm.entity_manager');
        $contact  = $caisse->getContact();
        $user     = $contact->getUser();
        $user->setUsername($user->getEmail());
        $password = $this->container->get('utils.utils')->generatePassword(8);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole('ROLE_CAISSE');
        $contact->setUser($user);
//        foreach ($caisse->getVisualWebs() as $vweb) {
//            $vweb->setCaisse($caisse);
//            $em->persist($vweb);
//        }
//        foreach ($caisse->getVisualPrints() as $vprint) {
//            $vprint->setCaisse($caisse);
//            $em->persist($vprint);
//        }
        try {
            $caisse->upload();
            $em->persist($user);
            $em->persist($contact);
            $caisse->setActive(Caisse::STATUS_ACTIVE);
            $em->persist($caisse);
            $em->flush();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        /*         * **************** Email send ********************* */
        $message = (new \Swift_Message('ISILIS Plateforme'))
            ->setFrom($this->container->getParameter('mailer_from'))
            ->setTo($user->getEmail())
            ->setBcc('toueiti@yahoo.fr')
            ->setBody(
            $this->container->get('templating')->render(
                'back/email/contact_registre.twig',
                array('user' => $user, 'password' => $password, 'contact' => $contact)
            ), 'text/html'
        );
        $this->container->get('mailer')->send($message);
        /*         * ******************** end email ****************** */
    }

    public function editCaisse(Caisse $caisse)
    {
        $em       = $this->container->get('doctrine.orm.entity_manager');
        $contact  = $caisse->getContact();
        $user     = $contact->getUser();
        $user->setUsername($user->getEmail());
        $password = $this->container->get('utils.utils')->generatePassword(8);
       // $user->setPlainPassword($password);
        $user->setEnabled(true);
        $contact->setUser($user);
//        foreach ($caisse->getVisualWebs() as $vweb) {
//            $vweb->setCaisse($caisse);
//            $em->persist($vweb);
//        }
//        foreach ($caisse->getVisualPrints() as $vprint) {
//            $vprint->setCaisse($caisse);
//            $em->persist($vprint);
//        }
        try {
            $caisse->upload();
            $em->persist($user);
            $em->persist($contact);
            $caisse->setActive(Caisse::STATUS_ACTIVE);
            $em->persist($caisse);
            $em->flush();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        /*         * **************** Email send ********************* */
        $message = (new \Swift_Message('ISILIS Plateforme'))
            ->setFrom('info@dot2com.com')
            ->setTo($user->getEmail())
            ->setBcc('toueiti@yahoo.fr')
            ->setBody(
            $this->container->get('templating')->render(
                'back/email/contact_registre.twig',
                array('user' => $user, 'password' => $password, 'contact' => $contact)
            ), 'text/html'
        );
        //$this->container->get('mailer')->send($message);
        /*         * ******************** end email ****************** */
    }

    public function filtreAgenceByCaisse($commercial)
    {
        $banks   = array();
        $agences = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Agence::class)->getAgences($commercial);
        foreach ($agences as $agence) {
            foreach ($agence->getCaisse() as $caisses) {
                $banks[$caisses->getBank()->getId()]['bank_data']     = $caisses->getBank();
                $banks[$caisses->getBank()->getId()]['caisse_data'][] = $caisses;
                foreach ($caisses as $caisse) {
                    $banks[$caisse->getBank()->getId()]['agence_data'][] = $caisse->getAgence();
                }
            }
        }
        return $banks;
    }

    public function FilterCaisseContact($commercial)
    {
        $banks   = array();
        $ciasses = $this->container->get('doctrine')->getManager()->getRepository(Caisse::class)->getCaisseContact($commercial);
        foreach ($ciasses as $caisse) {
            $banks[$caisse->getBank()->getId()]['bank_data']     = $caisse->getBank();
            $banks[$caisse->getBank()->getId()]['caisse_data'][] = $caisse;
        }
        return $banks;
    }
}