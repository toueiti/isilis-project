<?php

namespace BackBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use BackBundle\Entity\ObjetCompaign;

class ObjectifCampaignManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function createObjectifCampaign(ObjetCompaign $objetctif_campaign)
    {
        try {
            $em = $this->container->get('doctrine.orm.entity_manager');
            $objetctif_campaign->setIsActive(1);
            $em->persist($objetctif_campaign);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function editObjectifCampaign(ObjetCompaign $objetctif_campaign)
    {
        try {
            $em = $this->container->get('doctrine.orm.entity_manager');
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}