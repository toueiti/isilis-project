<?php

namespace BackBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use BackBundle\Entity\Bank;

class BankManager {

    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function createBank(Bank $bank) {
        $bank->upload();
        $bank->uploadchart();
        foreach ($bank->getVisualWebs() as $vweb) {
            $vweb->setBank($bank);
            $this->em->persist($vweb);
        }
        foreach ($bank->getVisualPrints() as $vprint) {
            $vprint->setBank($bank);
            $this->em->persist($vprint);
        }
        $bank->setActive(Bank::STATUS_ACTIVE);
        $this->em->persist($bank);
        $this->em->flush();
    }

    public function editBank(Bank $bank) {
        $bank->upload();
        $bank->uploadchart();
        foreach ($bank->getVisualWebs() as $vweb) {
            $vweb->setBank($bank);
            $this->em->persist($vweb);
        }
        foreach ($bank->getVisualPrints() as $vprint) {
            $vprint->setBank($bank);
            $this->em->persist($vprint);
        }
        $this->em->persist($bank);
        $this->em->flush();
    }

}
