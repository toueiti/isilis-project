<?php

namespace BackBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use UserBundle\Entity\Commercial;

class CommercialManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function createCommercial(Commercial $commercial)
    {
        try {
            $em       = $this->container->get('doctrine.orm.entity_manager');
            $user     = $commercial->getUser();
            $user->setUsername($user->getEmail());
            $password = $this->container->get('utils.utils')->generatePassword(8);
            $user->setPlainPassword($password);
            $user->setEnabled(true);
            $user->addRole('ROLE_COMERCIAL');
            $em->persist($user);
            $em->persist($commercial);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $message = (new \Swift_Message('ISILIS Plateforme'))
            ->setFrom($this->container->getParameter('mailer_from'))
            ->setTo($user->getEmail())
            ->setBcc('toueiti@yahoo.fr')
            ->setBody(
            $this->container->get('templating')->render(
                'back/email/contact_registre.twig',
                array('user' => $user, 'password' => $password, 'contact' => $commercial)
            ), 'text/html'
        );
        //$this->container->get('mailer')->send($message);
        /*         * ******************** end email ****************** */
    }

    public function editCommercial(Commercial $commercial)
    {
        try {
            $em       = $this->container->get('doctrine.orm.entity_manager');
            $user     = $commercial->getUser();
            $user->setUsername($user->getEmail());
            $user->setEnabled(true);
            $user->addRole('ROLE_COMERCIAL');
            $em->persist($commercial);
            $em->flush();
            $em->persist($user);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}