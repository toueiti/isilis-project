<?php

namespace BackBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use BackBundle\Entity\Agence;

/**
 * 
 */
class AgenceManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function createAgence(Agence $agence)
    {
        $em       = $this->container->get('doctrine.orm.entity_manager');
        $contact  = $agence->getContact();
        $user     = $contact->getUser();
        $user->setUsername($user->getEmail());
        $password = $this->container->get('utils.utils')->generatePassword(8);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole('ROLE_AGENCE');
        $em->persist($user);
        $contact->setUser($user);
        $em->persist($contact);
        $agence->upload();
        $agence->setActive(Agence::STATUS_ACTIVE);
        $em->persist($agence);
        $em->flush();
        /*         * **************** Email send ********************* */
        $message  = (new \Swift_Message('ISILIS Plateforme'))
            ->setFrom($this->container->getParameter('mailer_from'))
            ->setTo($user->getEmail())
            ->setBcc('toueiti@yahoo.fr')
            ->setBody(
            $this->container->get('templating')->render(
                'back/email/contact_registre.twig',
                array('user' => $user, 'password' => $password, 'contact' => $contact)
            ), 'text/html'
        );
      //  $this->container->get('mailer')->send($message);
        /*         * ******************** end email ****************** */
    }

    public function editAgence(Agence $agence)
    {
        $em       = $this->container->get('doctrine.orm.entity_manager');
        $contact  = $agence->getContact();
        $user     = $contact->getUser();
        $user->setUsername($user->getEmail());
        $password = $this->container->get('utils.utils')->generatePassword(8);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole('ROLE_AGENCE');
        $em->persist($user);
        $contact->setUser($user);
        $em->persist($contact);
        $agence->upload();
        $em->persist($agence);
        $em->flush();
        /*         * **************** Email send ********************* */
        $message  = (new \Swift_Message('ISILIS Plateforme'))
            ->setFrom('info@dot2com.com')
            ->setTo($user->getEmail())
            ->setBcc('toueiti@yahoo.fr')
            ->setBody(
            $this->container->get('templating')->render(
                'back/email/contact_registre.twig',
                array('user' => $user, 'password' => $password, 'contact' => $contact)
            ), 'text/html'
        );
    }
}