<?php

namespace BackBundle\Service;

use BackBundle\Entity\Caisse;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FilterManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function FilterStatisticsOperations($dates, $commercial)
    {
        try {
            $banks = array();
            $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getStaticsOperations($dates, $commercial);
            foreach ($operations as $operation) {
                $banks[$operation->getBank()->getId()]['bank_data'] = $operation->getBank();
                $banks[$operation->getBank()->getId()]['operation_data'][] = $operation;
            }
            return $banks;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function FilterStatisticsCampaigns($dates, $commercial)
    {
        try {
            $banks = array();
            $campaigns = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Campaign::class)->getStaticsCampaigns($dates, $commercial);
            foreach ($campaigns as $campaign) {
                $banks[$campaign->getBank()->getId()]['bank_data'] = $campaign->getBank();
                $banks[$campaign->getBank()->getId()]['campaign_data'][] = $campaign;
            }
            return $banks;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function FilterStatisticsBanks($dates, $commercial)
    {
        try {
            return $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Bank::class)->getStaticsBanks($dates, $commercial);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function FilterStatisticsCaisses($dates, $commercial)
    {
        $banks = array();
        $agences = $this->container->get('doctrine')->getManager()->getRepository(Caisse::class)->getStaticsCaisses($dates, $commercial);
        foreach ($agences as $agence) {
            foreach ($agence->getCaisse() as $caisses) {
                $banks[$caisses->getBank()->getId()]['bank_data'] = $caisses->getBank();
                $banks[$caisses->getBank()->getId()]['caisse_data'][] = $caisses;
                foreach ($caisses as $caisse) {
                    $banks[$caisse->getBank()->getId()]['agence_data'][] = $caisse->getAgence();
                }
            }
        }
       
        //dump($banks);die();
        return $banks;
    }

    public function FilterStatisticsOperationOrdred($dates, $status, $commercial)
    {
        try {
            $opts = array();
            $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getStaticsOrdredOperation($dates, $status, $commercial);
            foreach ($operations as $operation) {
                $opts[$operation->getBank()->getId()]['bank_data'] = $operation->getBank();
                $opts[$operation->getBank()->getId()]['opt_data'][] = $operation;
                $opts[$operation->getBank()->getId()]['caisse_data'] = $operation->getCaisse();
            }
            return $opts;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}