<?php

namespace BackBundle\Service;

use BackBundle\Entity\Campaign;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CampaignManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function createCampaign(Campaign $campaign)
    {
        try {
            $em = $this->container->get('doctrine.orm.entity_manager');
            foreach ($campaign->getVisualWebs() as $vweb) {
                $vweb->setCampaign($campaign);
                $vweb->upload();
                $em->persist($vweb);
            }
            foreach ($campaign->getVisualPrints() as $vprint) {
                $vprint->setCampaign($campaign);
                $vprint->upload();
                $em->persist($vprint);
            }
            $campaign->setActive(1);
            $campaign->upload();
            $em->persist($campaign);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function editCampaign(Campaign $campaign)
    {
        try {
            $em = $this->container->get('doctrine.orm.entity_manager');
            foreach ($campaign->getVisualWebs() as $vweb) {
                if (!is_null($vweb->getType()) || !is_null($vweb->getLargeur()) || !is_null($vweb->getHauteur()) || !is_null($vweb->getUnite()) || !is_null($vweb->getPath())) {
                    $vweb->setCampaign($campaign);
                    $vweb->upload();
                    $em->persist($vweb);
                }
            }
            foreach ($campaign->getVisualPrints() as $vprint) {
                if (!is_null($vprint->getType()) || !is_null($vprint->getLargeur()) || !is_null($vprint->getHauteur()) || !is_null($vprint->getUnite()) || !is_null($vprint->getPath())) {
                    $vprint->setCampaign($campaign);
                    $vprint->upload();
                    $em->persist($vprint);
                }
            }
            $campaign->upload();
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function FilterActiveCompaign($id, $commercial)
    {
        try {
            $banks = array();
            $campaigns = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Campaign::class)->getActiveCompaign($id, $commercial);
            foreach ($campaigns as $campaign) {
                $banks[$campaign->getBank()->getId()]['bank_data'] = $campaign->getBank();
                $banks[$campaign->getBank()->getId()]['campaign_data'][] = $campaign;
            }
            return $banks;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function filterCustomerList($formData)
    {
        $banks = array();
        $agence = $this->container->get('agence.agence.info')->getAgence();
        $operations_invalidation = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getCustumerList($agence,
            $formData);
        $operations_done = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getCustumerList($agence,
            $formData, 'done');

        foreach ($operations_invalidation as $operation) {
            if ($operation[0]) {
                $banks[$operation['bank_id']]['bank_name'] = $operation['bank_name'];
                $banks[$operation['bank_id']]['bank_icon'] = $operation['bank_icon'];
                $banks[$operation['bank_id']]['caisses'][$operation['caisse_id']]['caisse_name'] = $operation['caisse_name'];
                $banks[$operation['bank_id']]['caisses'][$operation['caisse_id']]['operation_in_validation_count'] = $operation['number'];
            }
        }
        foreach ($operations_done as $operation) {
            if ($operation[0]) {
                $banks[$operation['bank_id']]['bank_name'] = $operation['bank_name'];
                $banks[$operation['bank_id']]['bank_icon'] = $operation['bank_icon'];
                $banks[$operation['bank_id']]['caisses'][$operation['caisse_id']]['caisse_name'] = $operation['caisse_name'];
                $banks[$operation['bank_id']]['caisses'][$operation['caisse_id']]['operation_done_count'] = $operation['number'];
            }
        }
        return $banks;
    }

    public function filterDoneCompaign($id, $commercial)
    {
        try {
            $banks = array();
            $campaigns = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Campaign::class)->getDoneCompaign($id, $commercial);
            foreach ($campaigns as $campaign) {
                $banks[$campaign->getBank()->getId()]['bank_data'] = $campaign->getBank();
                $banks[$campaign->getBank()->getId()]['campaign_data'][] = $campaign;
            }
            return $banks;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function FilterActiveCompaignByCurrentAgency($formData)
    {
        try {
            $banks = array();
            $agence = $this->container->get('agence.agence.info')->getAgence();
            $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getActiveCompaignByCurrentAgency($agence,
                $formData);
            foreach ($operations as $operation) {
                if (isset($banks[$operation->getBank()->getId()]['campaigns'][$operation->getCampaign()->getId()]['count']))
                    $banks[$operation->getBank()->getId()]['campaigns'][$operation->getCampaign()->getId()]['count']++;
                else
                    $banks[$operation->getBank()->getId()]['campaigns'][$operation->getCampaign()->getId()]['count']
                        = 1;
                $banks[$operation->getBank()->getId()]['bank_data'] = $operation->getBank();
                $banks[$operation->getBank()->getId()]['campaigns'][$operation->getCampaign()->getId()]['campaign_data']
                    = $operation->getCampaign();
            }

            return $banks;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function filterDoneCompaignByCurrentAgency($formData)
    {
        try {
            $banks = array();
            $agence = $this->container->get('agence.agence.info')->getAgence();
            $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getDoneCompaignByCurrentAgency($agence,
                $formData);
            foreach ($operations as $operation) {
                if (isset($banks[$operation->getBank()->getId()]['campaigns'][$operation->getCampaign()->getId()]['count']))
                    $banks[$operation->getBank()->getId()]['campaigns'][$operation->getCampaign()->getId()]['count']++;
                else
                    $banks[$operation->getBank()->getId()]['campaigns'][$operation->getCampaign()->getId()]['count']
                        = 1;
                $banks[$operation->getBank()->getId()]['bank_data'] = $operation->getBank();
                $banks[$operation->getBank()->getId()]['campaigns'][$operation->getCampaign()->getId()]['campaign_data']
                    = $operation->getCampaign();
            }
            return $banks;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}