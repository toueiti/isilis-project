<?php

namespace BackBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use BackBundle\Entity\Operation;

class SearchManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getSearchResult($name, $commercial)
    {
        try {
            $campaigns = array();
            if ($name === "") {
                return;
            }
            $operations    = $this->container->get('doctrine')->getManager()->getRepository(Operation::class)->getOperations($name, $commercial);
           // $all_campaigns = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Campaign::class)->getCampaigns($name);
            foreach ($operations as $operation) {
                $campaigns[$operation->getCampaign()->getId()]['campaign_data']    = $operation->getCampaign();
                $campaigns[$operation->getCampaign()->getId()]['operation_data'][]
                    = $operation;
            }
            return $campaigns;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function getSearchResultByCurrentAgency($name)
    {
        try {
            $campaigns = array();
            if ($name === "") {
                return;
            }
            $agence     = $this->container->get('agence.agence.info')->getAgence();
            $operations = $this->container->get('doctrine')->getManager()->getRepository(Operation::class)->getOperationsByCurrentAgency($name,
                $agence);
            foreach ($operations as $operation) {
                $campaigns[$operation->getCampaign()->getId()]['campaign_data']    = $operation->getCampaign();
                $campaigns[$operation->getCampaign()->getId()]['operation_data'][]
                    = $operation;
            }
            return $campaigns;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}