<?php

namespace BackBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use BackBundle\Entity\Operation;

/**
 * Description of OperationManager
 *
 */
class OperationManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function createOperation(Operation $operation)
    {
        try {
            $em = $this->container->get('doctrine.orm.entity_manager');
            foreach ($operation->getVisualWebs() as $vweb) {
                if (!is_null($vweb->getType()) && !is_null($vweb->getLargeur()) && !is_null($vweb->getHauteur())
                    && !is_null($vweb->getUnite()) && !is_null($vweb->getFile())) {
                    $vweb->setOperation($operation);
                    $vweb->upload();
                    $em->persist($vweb);
                }
            }
            foreach ($operation->getVisualPrints() as $vprint) {
                if (!is_null($vprint->getType()) && !is_null($vprint->getLargeur()) && !is_null($vprint->getHauteur())
                    && !is_null($vprint->getUnite()) && !is_null($vprint->getFile())) {
                    $vprint->setOperation($operation);
                    $vprint->upload();
                    $em->persist($vprint);
                }
            }
            $operation->setActive(Operation::STATUS_IN_VALIDATION);
            $operation->setAgence($operation->getCaisse()->getAgence());
            $em->persist($vprint);
            $em->persist($vweb);
            $em->persist($operation);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function editOperation(Operation $operation)
    {
        try {
            $em = $this->container->get('doctrine.orm.entity_manager');
            foreach ($operation->getVisualWebs() as $vweb) {
                if(!is_null($vweb->getType()) || !is_null($vweb->getLargeur()) || !is_null($vweb->getHauteur()) || !is_null($vweb->getUnite()) || !is_null($vweb->getPath())){
                    $vweb->setOperation($operation);
                    $em->persist($vweb);
                    $vweb->upload();
            }
            }
            foreach ($operation->getVisualPrints() as $vprint) {
                if (!is_null($vprint->getType()) || !is_null($vprint->getLargeur()) || !is_null($vprint->getHauteur()) || !is_null($vprint->getUnite()) || !is_null($vprint->getPath())) {
                    $vprint->setOperation($operation);
                    $em->persist($vprint);
                    $vprint->upload();
                }
            }
            $em->persist($operation);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function filterOpertaionInActivation($id, $commercial)
    {
        $banks      = array();
        $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getOpertaionInValidation($id, $commercial);
        foreach ($operations as $operation) {
            $banks[$operation->getBank()->getId()]['bank_data']        = $operation->getBank();
            $banks[$operation->getBank()->getId()]['operation_data'][] = $operation;
        }
        return $banks;
    }

    public function filterOpertaionActive($id, $commercial)
    {
        $banks      = array();
        $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getOpertaionActive($id, $commercial);
        foreach ($operations as $operation) {
            $banks[$operation->getBank()->getId()]['bank_data']        = $operation->getBank();
            $banks[$operation->getBank()->getId()]['operation_data'][] = $operation;
        }
        return $banks;
    }

    public function filterOpertaionDone($id, $commercial)
    {
        $banks      = array();
        $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getOpertaionDone($id, $commercial);
        foreach ($operations as $operation) {
            $banks[$operation->getBank()->getId()]['bank_data']        = $operation->getBank();
            $banks[$operation->getBank()->getId()]['operation_data'][] = $operation;
        }
        return $banks;
    }

    public function filterOpertaionInActivationByCurrentAgency($formData)
    {
        $banks      = array();
        $agence     = $this->container->get('agence.agence.info')->getAgence();
        $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getOpertaionInValidationByCurrentAgency($agence,
            $formData);
        foreach ($operations as $operation) {
            $banks[$operation->getBank()->getId()]['bank_data']        = $operation->getBank();
            $banks[$operation->getBank()->getId()]['operation_data'][] = $operation;
        }
        return $banks;
    }

    public function filterOpertaionDoneByCurentAgency($formData)
    {
        $banks      = array();
        $agence     = $this->container->get('agence.agence.info')->getAgence();
        $operations = $this->container->get('doctrine')->getManager()->getRepository(\BackBundle\Entity\Operation::class)->getOpertaionDoneByCurrentAgency($agence,
            $formData);
        foreach ($operations as $operation) {
            $banks[$operation->getBank()->getId()]['bank_data']        = $operation->getBank();
            $banks[$operation->getBank()->getId()]['operation_data'][] = $operation;
        }
        return $banks;
    }
}