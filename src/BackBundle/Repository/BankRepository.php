<?php

namespace BackBundle\Repository;

/**
 * BankRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BankRepository extends \Doctrine\ORM\EntityRepository
{

    public function getStaticsBanks($dates, $commercial)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('b')->from('BackBundle:Bank', 'b')
            ->where(" b.creatAt >= '" . $dates['startdate'] . "' AND b.creatAt <= '" . $dates['enddate'] . "' ")
            ->andWhere('b.active = :active');
        $qb->setParameter('active', 1);
        if(!is_null($commercial)){
            $qb->andWhere('b.commercial = :comemrcial_id');
            $qb->setParameter('comemrcial_id', $commercial);
        }
        return $qb->getQuery()->getResult();
    }
}