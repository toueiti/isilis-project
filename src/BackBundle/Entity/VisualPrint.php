<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * VisualPrint
 *
 * @ORM\Table(name="visual_print")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\VisualPrintRepository")
 */
class VisualPrint
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="largeur", type="integer", nullable=true)
     */
    private $largeur;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="hauteur", type="integer", nullable=true)
     */
    private $hauteur;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="unite", type="string", length=10, nullable=true)
     */
    private $unite;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", length=255, nullable=true, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="Caisse", inversedBy="visualPrints", cascade={"persist"})
     * @ORM\JoinColumn(name="caisse_id", referencedColumnName="id", nullable=true)
     */
    protected $caisse;

    /**
     * @ORM\ManyToOne(targetEntity="Bank", inversedBy="visualPrints", cascade={"persist"})
     * @ORM\JoinColumn(name="bank_id", referencedColumnName="id", nullable=true)
     */
    protected $bank;

    /**
     * @ORM\ManyToOne(targetEntity="Operation", inversedBy="visualPrints", cascade={"persist"})
     * @ORM\JoinColumn(name="operation_id", referencedColumnName="id", nullable=true)
     */
    protected $operation;

    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="visualPrints", cascade={"persist"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $campaign;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return VisualPrint
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set largeur.
     *
     * @param int $largeur
     *
     * @return VisualPrint
     */
    public function setLargeur($largeur)
    {
        $this->largeur = $largeur;

        return $this;
    }

    /**
     * Get largeur.
     *
     * @return int
     */
    public function getLargeur()
    {
        return $this->largeur;
    }

    /**
     * Set hauteur.
     *
     * @param int $hauteur
     *
     * @return VisualPrint
     */
    public function setHauteur($hauteur)
    {
        $this->hauteur = $hauteur;

        return $this;
    }

    /**
     * Get hauteur.
     *
     * @return int
     */
    public function getHauteur()
    {
        return $this->hauteur;
    }

    /**
     * Set unite.
     *
     * @param string $unite
     *
     * @return VisualPrint
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite.
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set caisse.
     *
     * @param \BackBundle\Entity\Caisse|null $caisse
     *
     * @return VisualPrint
     */
    public function setCaisse(\BackBundle\Entity\Caisse $caisse = null)
    {
        $this->caisse = $caisse;

        return $this;
    }

    /**
     * Get caisse.
     *
     * @return \BackBundle\Entity\Caisse|null
     */
    public function getCaisse()
    {
        return $this->caisse;
    }

    /**
     * Set bank.
     *
     * @param \BackBundle\Entity\Bank|null $bank
     *
     * @return VisualPrint
     */
    public function setBank(\BackBundle\Entity\Bank $bank = null)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank.
     *
     * @return \BackBundle\Entity\Bank|null
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set operation.
     *
     * @param \BackBundle\Entity\Operation|null $operation
     *
     * @return VisualPrint
     */
    public function setOperation(\BackBundle\Entity\Operation $operation = null)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation.
     *
     * @return \BackBundle\Entity\Operation|null
     */
    public function getOperation()
    {
        return $this->operation;
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/visuels';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $filename = date("Ymd_His").'_'.$this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(), $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Set path.
     *
     * @param string|null $path
     *
     * @return VisualPrint
     */
    public function setPath($path = null)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string|null
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set campaign.
     *
     * @param \BackBundle\Entity\Campaign|null $campaign
     *
     * @return VisualPrint
     */
    public function setCampaign(\BackBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign.
     *
     * @return \BackBundle\Entity\Campaign|null
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}