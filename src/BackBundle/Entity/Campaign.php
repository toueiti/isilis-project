<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Companion
 *
 * @ORM\Table(name="campaign")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\CampaignRepository")
 */
class Campaign
{
    const COMPAIGN_STATUS_DONE     = 0;
    const COMPAIGN_STATUS_ACTIVE   = 1;
    const COMPAIGN_STATUS_ARCHIVE = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="cibleCom", type="string", length=255)
     */
    private $cibleCom;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="cibleIlig", type="string", length=255)
     */
    private $cibleIlig;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="modeTrans", type="string", length=255)
     */
    private $modeTrans;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="amount", type="string", length=255, nullable=true)
     */
    private $amount;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="object_vol", type="string", length=255)
     */
    private $object_vol;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="permanent", type="boolean")
     */
    private $permanent;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="start_date", type="date")
     */
    private $startDate;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="end_date", type="date")
     */
    private $endDate;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="string", length=255)
     */
    private $active;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="Bank", inversedBy="campaign")
     * @ORM\JoinColumn(name="bank_id", referencedColumnName="id")
     */
    private $bank;

    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="campaign")
     */
    private $operation;

    /**
     * @ORM\ManyToOne(targetEntity="ObjetCompaign", inversedBy="campaigns", cascade={"persist"})
     * @ORM\JoinColumn(name="objet_id", referencedColumnName="id", nullable=true)
     */
    protected $objet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creatAt", type="date", nullable=true)
     */
    protected $creatAt;

    /**
     * @ORM\OneToMany(targetEntity="VisualPrint", mappedBy="campaign", cascade={"persist"})
     * 
     */
    protected $visualPrints;

    /**
     * @ORM\OneToMany(targetEntity="VisualWeb", mappedBy="campaign", cascade={"persist"})
     * 
     */
    protected $visualWebs;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Commercial", inversedBy="campaign")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id")
     */
    private $commercial;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get COMPAIGN_STATUS_DONE.
     */
    public function getCOMPAIGN_STATUS_DONE()
    {
        return self::COMPAIGN_STATUS_DONE;
    }

    /**
     * Get COMPAIGN_STATUS_ACTIVE.
     */
    public function getCOMPAIGN_STATUS_ACTIVE()
    {
        return self::COMPAIGN_STATUS_ACTIVE;
    }

    /**
     * Get COMPAIGN_STATUS_SUSPENDU.
     */
    public function getCOMPAIGN_STATUS_SUSPENDU()
    {
        return self::COMPAIGN_STATUS_SUSPENDU;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Companion
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cibleCom.
     *
     * @param string $cibleCom
     *
     * @return Companion
     */
    public function setCibleCom($cibleCom)
    {
        $this->cibleCom = $cibleCom;

        return $this;
    }

    /**
     * Get cibleCom.
     *
     * @return string
     */
    public function getCibleCom()
    {
        return $this->cibleCom;
    }

    /**
     * Set cibleIlig.
     *
     * @param string $cibleIlig
     *
     * @return Companion
     */
    public function setCibleIlig($cibleIlig)
    {
        $this->cibleIlig = $cibleIlig;

        return $this;
    }

    /**
     * Get cibleIlig.
     *
     * @return string
     */
    public function getCibleIlig()
    {
        return $this->cibleIlig;
    }

    /**
     * Set modeTrans.
     *
     * @param string $modeTrans
     *
     * @return Companion
     */
    public function setModeTrans($modeTrans)
    {
        $this->modeTrans = $modeTrans;

        return $this;
    }

    /**
     * Get modeTrans.
     *
     * @return string
     */
    public function getModeTrans()
    {
        return $this->modeTrans;
    }

    /**
     * Set amount.
     *
     * @param float $amount
     *
     * @return Companion
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Companion
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set comment.
     *
     * @param string $comment
     *
     * @return Companion
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set permanent.
     *
     * @param bool $permanent
     *
     * @return Companion
     */
    public function setPermanent($permanent)
    {
        $this->permanent = $permanent;

        return $this;
    }

    /**
     * Get permanent.
     *
     * @return bool
     */
    public function getPermanent()
    {
        return $this->permanent;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Companion
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return Companion
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set startDateOnline.
     *
     * @param \DateTime $startDateOnline
     *
     * @return Companion
     */
    public function setStartDateOnline($startDateOnline)
    {
        $this->startDateOnline = $startDateOnline;

        return $this;
    }

    /**
     * Get startDateOnline.
     *
     * @return \DateTime
     */
    public function getStartDateOnline()
    {
        return $this->startDateOnline;
    }

    /**
     * Set endDateOnline.
     *
     * @param \DateTime $endDateOnline
     *
     * @return Companion
     */
    public function setEndDateOnline($endDateOnline)
    {
        $this->endDateOnline = $endDateOnline;

        return $this;
    }

    /**
     * Get endDateOnline.
     *
     * @return \DateTime
     */
    public function getEndDateOnline()
    {
        return $this->endDateOnline;
    }

    /**
     * Set active.
     *
     * @param string $active
     *
     * @return Campaign
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set objectVol.
     *
     * @param string $objectVol
     *
     * @return Campaign
     */
    public function setObjectVol($objectVol)
    {
        $this->object_vol = $objectVol;

        return $this;
    }

    /**
     * Get objectVol.
     *
     * @return string
     */
    public function getObjectVol()
    {
        return $this->object_vol;
    }

    /**
     * Set bank.
     *
     * @param \BackBundle\Entity\Bank|null $bank
     *
     * @return Campaign
     */
    public function setBank(\BackBundle\Entity\Bank $bank = null)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank.
     *
     * @return \BackBundle\Entity\Bank|null
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->operation       = new ArrayCollection();
        $this->creatAt         = new \DateTime();
        $this->startDate       = new \DateTime();
        $this->endDate         = new \DateTime();
        $this->startDateOnline = new \DateTime();
        $this->endDateOnline   = new \DateTime();
    }

    /**
     * Add operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return Campaign
     */
    public function addOperation(\BackBundle\Entity\Operation $operation)
    {
        $this->operation[] = $operation;

        return $this;
    }

    /**
     * Remove operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOperation(\BackBundle\Entity\Operation $operation)
    {
        return $this->operation->removeElement($operation);
    }

    /**
     * Get operation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set objet.
     *
     * @param \BackBundle\Entity\ObjetCompaign|null $objet
     *
     * @return Campaign
     */
    public function setObjet(\BackBundle\Entity\ObjetCompaign $objet = null)
    {
        $this->objet = $objet;

        return $this;
    }

    /**
     * Get objet.
     *
     * @return \BackBundle\Entity\ObjetCompaign|null
     */
    public function getObjet()
    {
        return $this->objet;
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/visuels';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $filename = date("Ymd_His").'_'.$this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(), $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return Campaign
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set creatAt.
     *
     * @param \DateTime $creatAt
     *
     * @return Campaign
     */
    public function setCreatAt($creatAt)
    {
        $this->creatAt = $creatAt;

        return $this;
    }

    /**
     * Get creatAt.
     *
     * @return \DateTime
     */
    public function getCreatAt()
    {
        return $this->creatAt;
    }

    /**
     * Add visualPrint.
     *
     * @param \BackBundle\Entity\VisualPrint $visualPrint
     *
     * @return Campaign
     */
    public function addVisualPrint(\BackBundle\Entity\VisualPrint $visualPrint)
    {
        $this->visualPrints[] = $visualPrint;

        return $this;
    }

    /**
     * Remove visualPrint.
     *
     * @param \BackBundle\Entity\VisualPrint $visualPrint
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVisualPrint(\BackBundle\Entity\VisualPrint $visualPrint)
    {
        return $this->visualPrints->removeElement($visualPrint);
    }

    /**
     * Get visualPrints.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisualPrints()
    {
        return $this->visualPrints;
    }

    /**
     * Add visualWeb.
     *
     * @param \BackBundle\Entity\VisualWeb $visualWeb
     *
     * @return Campaign
     */
    public function addVisualWeb(\BackBundle\Entity\VisualWeb $visualWeb)
    {
        $this->visualWebs[] = $visualWeb;

        return $this;
    }

    /**
     * Remove visualWeb.
     *
     * @param \BackBundle\Entity\VisualWeb $visualWeb
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVisualWeb(\BackBundle\Entity\VisualWeb $visualWeb)
    {
        return $this->visualWebs->removeElement($visualWeb);
    }

    /**
     * Get visualWebs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisualWebs()
    {
        return $this->visualWebs;
    }

    /**
     * Set commercial.
     *
     * @param \UserBundle\Entity\Commercial|null $commercial
     *
     * @return Campaign
     */
    public function setCommercial(\UserBundle\Entity\Commercial $commercial = null)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial.
     *
     * @return \UserBundle\Entity\Commercial|null
     */
    public function getCommercial()
    {
        return $this->commercial;
    }
}
