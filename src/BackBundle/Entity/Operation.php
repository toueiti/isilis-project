<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Operation
 *
 * @ORM\Table(name="operation")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\OperationRepository")
 */
class Operation
{
    const STATUS_IN_VALIDATION = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_ARCHIVE = 2;
    const STATUS_FAILED = 3;
    const STATUS_SUCCESS = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="ObjetOperation", inversedBy="operation", cascade={"persist"})
     * @ORM\JoinColumn(name="objectif_id", referencedColumnName="id", nullable=true)
     */
    private $objectif;

    /**
     * @var string
     *
     * @ORM\Column(name="cibleCom", type="string", length=255)
     */
    private $cibleCom;

    /**
     * @var string
     *
     * @ORM\Column(name="cibleIlig", type="string", length=255)
     */
    private $cibleIlig;

    /**
     * @var string
     *
     * @ORM\Column(name="modeTrans", type="string", length=255, nullable=true)
     */
    private $modeTrans;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="string", length=255, nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="object_vol", type="string", length=255, nullable=true)
     */
    private $object_vol;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var bool
     *
     * @ORM\Column(name="permanant", type="boolean")
     */
    private $permanant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDateOnline", type="date", nullable=true)
     */
    private $startDateOnline;

    /**
     * @var string
     *
     * @ORM\Column(name="endDateOnline", type="date", nullable=true)
     */
    private $endDateOnline;

    /**
     * @var bool
     *
     * @ORM\Column(name="support", type="boolean")
     */
    private $support = false;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="string", length=255)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="ordred", type="boolean", nullable=true)
     */
    private $ordred = false;

    /**
     * @ORM\ManyToOne(targetEntity="Bank", inversedBy="operation", cascade={"persist"})
     * @ORM\JoinColumn(name="bank_id", referencedColumnName="id", nullable=true)
     */
    protected $bank;

    /**
     * @ORM\ManyToOne(targetEntity="Caisse", inversedBy="operation", cascade={"persist"})
     * @ORM\JoinColumn(name="caisse_id", referencedColumnName="id", nullable=true)
     */
    protected $caisse;

    /**
     * @ORM\ManyToOne(targetEntity="Agence", inversedBy="operations", cascade={"persist"})
     * @ORM\JoinColumn(name="agence_id", referencedColumnName="id", nullable=true)
     */
    protected $agence;

    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="operation", cascade={"persist"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id", nullable=true)
     */
    protected $campaign;

    /**
     * @ORM\OneToMany(targetEntity="VisualPrint", mappedBy="operation", cascade={"persist"})
     *
     */
    protected $visualPrints;

    /**
     * @ORM\OneToMany(targetEntity="VisualWeb", mappedBy="operation", cascade={"persist"})
     *
     */
    protected $visualWebs;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creatAt", type="date", nullable=true)
     */
    protected $creatAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->visualPrints = new \Doctrine\Common\Collections\ArrayCollection();
        $this->visualWebs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creatAt = new \DateTime();
        $this->startDate = new \DateTime();
        $this->endDate = new \DateTime();
        $this->startDateOnline = new \DateTime();
        $this->endDateOnline = new \DateTime();
    }

    /*
     * @ORM\ManyToOne(targetEntity="ObjetOperation", inversedBy="operations", cascade={"persist"})
     * @ORM\JoinColumn(name="objet_id", referencedColumnName="id", nullable=true)
     */
    protected $objet;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="operation", cascade={"persist"})
     *
     */
    protected $messages;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Operation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set cibleCom.
     *
     * @param string $cibleCom
     *
     * @return Operation
     */
    public function setCibleCom($cibleCom)
    {
        $this->cibleCom = $cibleCom;

        return $this;
    }

    /**
     * Get cibleCom.
     *
     * @return string
     */
    public function getCibleCom()
    {
        return $this->cibleCom;
    }

    /**
     * Set cibleIlig.
     *
     * @param string $cibleIlig
     *
     * @return Operation
     */
    public function setCibleIlig($cibleIlig)
    {
        $this->cibleIlig = $cibleIlig;

        return $this;
    }

    /**
     * Get cibleIlig.
     *
     * @return string
     */
    public function getCibleIlig()
    {
        return $this->cibleIlig;
    }

    /**
     * Set modeTrans.
     *
     * @param string|null $modeTrans
     *
     * @return Operation
     */
    public function setModeTrans($modeTrans = null)
    {
        $this->modeTrans = $modeTrans;

        return $this;
    }

    /**
     * Get modeTrans.
     *
     * @return string|null
     */
    public function getModeTrans()
    {
        return $this->modeTrans;
    }

    /**
     * Set amount.
     *
     * @param float $amount
     *
     * @return Operation
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set objectVol.
     *
     * @param string|null $objectVol
     *
     * @return Operation
     */
    public function setObjectVol($objectVol = null)
    {
        $this->object_vol = $objectVol;

        return $this;
    }

    /**
     * Get objectVol.
     *
     * @return string|null
     */
    public function getObjectVol()
    {
        return $this->object_vol;
    }

    /**
     * Set comment.
     *
     * @param string $comment
     *
     * @return Operation
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set permanant.
     *
     * @param bool $permanant
     *
     * @return Operation
     */
    public function setPermanant($permanant)
    {
        $this->permanant = $permanant;

        return $this;
    }

    /**
     * Get permanant.
     *
     * @return bool
     */
    public function getPermanant()
    {
        return $this->permanant;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Operation
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return Operation
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set startDateOnline.
     *
     * @param \DateTime|null $startDateOnline
     *
     * @return Operation
     */
    public function setStartDateOnline($startDateOnline = null)
    {
        $this->startDateOnline = $startDateOnline;

        return $this;
    }

    /**
     * Get startDateOnline.
     *
     * @return \DateTime|null
     */
    public function getStartDateOnline()
    {
        return $this->startDateOnline;
    }

    /**
     * Set endDateOnline.
     *
     * @param \DateTime|null $endDateOnline
     *
     * @return Operation
     */
    public function setEndDateOnline($endDateOnline = null)
    {
        $this->endDateOnline = $endDateOnline;

        return $this;
    }

    /**
     * Get endDateOnline.
     *
     * @return \DateTime|null
     */
    public function getEndDateOnline()
    {
        return $this->endDateOnline;
    }

    /**
     * Set support.
     *
     * @param bool $support
     *
     * @return Operation
     */
    public function setSupport($support)
    {
        $this->support = $support;

        return $this;
    }

    /**
     * Get support.
     *
     * @return bool
     */
    public function getSupport()
    {
        return $this->support;
    }

    /**
     * Set active.
     *
     * @param string $active
     *
     * @return Operation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set bank.
     *
     * @param \BackBundle\Entity\Bank|null $bank
     *
     * @return Operation
     */
    public function setBank(\BackBundle\Entity\Bank $bank = null)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank.
     *
     * @return \BackBundle\Entity\Bank|null
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set caisse.
     *
     * @param \BackBundle\Entity\Caisse|null $caisse
     *
     * @return Operation
     */
    public function setCaisse(\BackBundle\Entity\Caisse $caisse = null)
    {
        $this->caisse = $caisse;

        return $this;
    }

    /**
     * Get caisse.
     *
     * @return \BackBundle\Entity\Caisse|null
     */
    public function getCaisse()
    {
        return $this->caisse;
    }

    /**
     * Set agence.
     *
     * @param \BackBundle\Entity\Agence|null $agence
     *
     * @return Operation
     */
    public function setAgence(\BackBundle\Entity\Agence $agence = null)
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * Get agence.
     *
     * @return \BackBundle\Entity\Agence|null
     */
    public function getAgence()
    {
        return $this->agence;
    }

    /**
     * Set campaign.
     *
     * @param \BackBundle\Entity\Campaign|null $campaign
     *
     * @return Operation
     */
    public function setCampaign(\BackBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign.
     *
     * @return \BackBundle\Entity\Campaign|null
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Add visualPrint.
     *
     * @param \BackBundle\Entity\VisualPrint $visualPrint
     *
     * @return Operation
     */
    public function addVisualPrint(\BackBundle\Entity\VisualPrint $visualPrint)
    {
        $this->visualPrints[] = $visualPrint;

        return $this;
    }

    /**
     * Remove visualPrint.
     *
     * @param \BackBundle\Entity\VisualPrint $visualPrint
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVisualPrint(\BackBundle\Entity\VisualPrint $visualPrint)
    {
        return $this->visualPrints->removeElement($visualPrint);
    }

    /**
     * Get visualPrints.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisualPrints()
    {
        return $this->visualPrints;
    }

    /**
     * Add visualWeb.
     *
     * @param \BackBundle\Entity\VisualWeb $visualWeb
     *
     * @return Operation
     */
    public function addVisualWeb(\BackBundle\Entity\VisualWeb $visualWeb)
    {
        $this->visualWebs[] = $visualWeb;

        return $this;
    }

    /**
     * Remove visualWeb.
     *
     * @param \BackBundle\Entity\VisualWeb $visualWeb
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVisualWeb(\BackBundle\Entity\VisualWeb $visualWeb)
    {
        return $this->visualWebs->removeElement($visualWeb);
    }

    /**
     * Get visualWebs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisualWebs()
    {
        return $this->visualWebs;
    }

    /**
     * Add message.
     *
     * @param \BackBundle\Entity\Message $message
     *
     * @return Operation
     */
    public function addMessage(\BackBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message.
     *
     * @param \BackBundle\Entity\Message $message
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMessage(\BackBundle\Entity\Message $message)
    {
        return $this->messages->removeElement($message);
    }

    /**
     * Get messages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * get nb message non lu
     */
    public function getNbMsgNonLu()
    {
        $nb = 0;
        foreach ($this->getMessages() as $msg) {
            if ($msg->getValideCaisse() && !$msg->getSeenCaisse()) {
                $nb++;
            }
        }
        return $nb;
    }

    /**
     * Set creatAt.
     *
     * @param \DateTime $creatAt
     *
     * @return Operation
     */
    public function setCreatAt($creatAt)
    {
        $this->creatAt = $creatAt;

        return $this;
    }

    /**
     * Get creatAt.
     *
     * @return \DateTime
     */
    public function getCreatAt()
    {
        return $this->creatAt;
    }

    /**
     * Set ordred.
     *
     * @param bool|null $ordred
     *
     * @return Operation
     */
    public function setOrdred($ordred = null)
    {
        $this->ordred = $ordred;

        return $this;
    }

    /**
     * Get ordred.
     *
     * @return bool|null
     */
    public function getOrdred()
    {
        return $this->ordred;
    }

    /**
     * Set objectif.
     *
     * @param \BackBundle\Entity\ObjetOperation|null $objectif
     *
     * @return Operation
     */
    public function setObjectif(\BackBundle\Entity\ObjetOperation $objectif = null)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif.
     *
     * @return \BackBundle\Entity\ObjetOperation|null
     */
    public function getObjectif()
    {
        return $this->objectif;
    }
}
