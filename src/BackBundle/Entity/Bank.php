<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Bank
 *
 * @ORM\Table(name="bank")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\BankRepository")
 */
class Bank
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE   = 1;
    const STATUS_ARCHIVE  = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="zipCode", type="string", length=10)
     */
    private $zipCode;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="contactName", type="string", length=255)
     */
    private $contactName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="contactRole", type="string", length=255)
     */
    private $contactRole;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="contactPhone", type="string", length=255)
     */
    private $contactPhone;

    /**
     * @var string
     * @Assert\Email(message="Entrez un email valide !")
     * @ORM\Column(name="contactEmail", type="string", length=255)
     */
    private $contactEmail;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @var string
     * @ORM\Column(name="active", type="string", length=10)
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="VisualPrint", mappedBy="bank", cascade={"persist"})
     * 
     */
    protected $visualPrints;

    /**
     * @ORM\OneToMany(targetEntity="VisualWeb", mappedBy="bank", cascade={"persist"})
     * 
     */
    protected $visualWebs;

    /**
     * @ORM\OneToMany(targetEntity="Caisse", mappedBy="bank", cascade={"persist"})
     * 
     */
    protected $caisses;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Commercial", inversedBy="bank")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id")
     */
    private $commercial;

    /**
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="bank")
     */
    private $campaign;

    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="bank")
     */
    private $operation;


    /**
     * @Assert\File(maxSize="6000000")
     */
    private $chartPath;

    /**
     * @var string
     * @ORM\Column(name="chart", type="string", length=255, nullable=true)
     */
    private $chart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creatAt", type="date", nullable=true)
     */
    protected $creatAt;

    public function getAbsoluteIcone()
    {
        return null === $this->icon ? null : $this->getUploadRootDir().'/'.$this->icon;
    }

    public function getWebIcone()
    {
        return null === $this->icon ? null : $this->getUploadDir().'/'.$this->icon;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory icon where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/icones';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(), $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->icon = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    // upload chart
    public function getAbsoluteChart()
    {
        return null === $this->chart ? null : $this->getUploadChartRootDir().'/'.$this->chart;
    }

    protected function getUploadChartRootDir()
    {
        // the absolute directory icon where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadChartDir();
    }

    protected function getUploadChartDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/charts';
    }

    public function uploadchart()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getChartPath()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $this->getChartPath()->move(
            $this->getUploadChartRootDir(),
            $this->getChartPath()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->chart = $this->getChartPath()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->chartPath = null;
    }
    // upload chart

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Bank
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address.
     *
     * @param string $address
     *
     * @return Bank
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zipCode.
     *
     * @param string $zipCode
     *
     * @return Bank
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Bank
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set icon.
     *
     * @param string $icon
     *
     * @return Bank
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set contactName.
     *
     * @param string $contactName
     *
     * @return Bank
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * Get contactName.
     *
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * Set contactRole.
     *
     * @param string $contactRole
     *
     * @return Bank
     */
    public function setContactRole($contactRole)
    {
        $this->contactRole = $contactRole;

        return $this;
    }

    /**
     * Get contactRole.
     *
     * @return string
     */
    public function getContactRole()
    {
        return $this->contactRole;
    }

    /**
     * Set contactPhone.
     *
     * @param string $contactPhone
     *
     * @return Bank
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * Get contactPhone.
     *
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * Set contactEmail.
     *
     * @param string $contactEmail
     *
     * @return Bank
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail.
     *
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->visualPrints = new ArrayCollection();
        $this->visualWebs   = new ArrayCollection();
        $this->campaign     = new ArrayCollection();
        $this->operation    = new ArrayCollection();
        $this->creatAt      = new \Datetime();
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets chartPath.
     *
     * @return UploadedFile
     */
    public function setChartPath(UploadedFile $chartPath = null)
    {
        $this->chartPath = $chartPath;
    }

    /**
     * Get chart.
     *
     * @return UploadedFile
     */
    public function getChartPath()
    {
        return $this->chartPath;
    }

    /**
     * Add visualPrint.
     *
     * @param \BackBundle\Entity\VisualPrint $visualPrint
     *
     * @return Bank
     */
    public function addVisualPrint(\BackBundle\Entity\VisualPrint $visualPrint)
    {
        $this->visualPrints[] = $visualPrint;

        return $this;
    }

    /**
     * Remove visualPrint.
     *
     * @param \BackBundle\Entity\VisualPrint $visualPrint
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVisualPrint(\BackBundle\Entity\VisualPrint $visualPrint)
    {
        return $this->visualPrints->removeElement($visualPrint);
    }

    /**
     * Get visualPrints.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisualPrints()
    {
        return $this->visualPrints;
    }

    /**
     * Add visualWeb.
     *
     * @param \BackBundle\Entity\VisualWeb $visualWeb
     *
     * @return Bank
     */
    public function addVisualWeb(\BackBundle\Entity\VisualWeb $visualWeb)
    {
        $this->visualWebs[] = $visualWeb;

        return $this;
    }

    /**
     * Remove visualWeb.
     *
     * @param \BackBundle\Entity\VisualWeb $visualWeb
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVisualWeb(\BackBundle\Entity\VisualWeb $visualWeb)
    {
        return $this->visualWebs->removeElement($visualWeb);
    }

    /**
     * Get visualWebs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisualWebs()
    {
        return $this->visualWebs;
    }

    /**
     * Add caiss.
     *
     * @param \BackBundle\Entity\Caisse $caiss
     *
     * @return Bank
     */
    public function addCaiss(\BackBundle\Entity\Caisse $caiss)
    {
        $this->caisses[] = $caiss;

        return $this;
    }

    /**
     * Remove caiss.
     *
     * @param \BackBundle\Entity\Caisse $caiss
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCaiss(\BackBundle\Entity\Caisse $caiss)
    {
        return $this->caisses->removeElement($caiss);
    }

    /**
     * Get caisses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCaisses()
    {
        return $this->caisses;
    }

    /**
     * Add campaign.
     *
     * @param \BackBundle\Entity\Campaign $campaign
     *
     * @return Bank
     */
    public function addCampaign(\BackBundle\Entity\Campaign $campaign)
    {
        $this->campaign[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign.
     *
     * @param \BackBundle\Entity\Campaign $campaign
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCampaign(\BackBundle\Entity\Campaign $campaign)
    {
        return $this->campaign->removeElement($campaign);
    }

    /**
     * Get campaign.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Add operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return Bank
     */
    public function addOperation(\BackBundle\Entity\Operation $operation)
    {
        $this->operation[] = $operation;

        return $this;
    }

    /**
     * Remove operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOperation(\BackBundle\Entity\Operation $operation)
    {
        return $this->operation->removeElement($operation);
    }

    /**
     * Get operation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set active.
     *
     * @param string $active
     *
     * @return Bank
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }



    /**
     * Set creatAt.
     *
     * @param \DateTime $creatAt
     *
     * @return Operation
     */
    public function setCreatAt($creatAt)
    {
        $this->creatAt = $creatAt;

        return $this;
    }

    /**
     * Get creatAt.
     *
     * @return \DateTime
     */
    public function getCreatAt()
    {
        return $this->creatAt;
    }

    /**
     * Set chart.
     *
     * @param string $chart
     *
     * @return Bank
     */
    public function setChart($chart)
    {
        $this->chart = $chart;

        return $this;
    }

    /**
     * Get chart.
     *
     * @return string
     */
    public function getChart()
    {
        return $this->chart;
    }

    /**
     * Set commercial.
     *
     * @param \UserBundle\Entity\Commercial|null $commercial
     *
     * @return Bank
     */
    public function setCommercial(\UserBundle\Entity\Commercial $commercial = null)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial.
     *
     * @return \UserBundle\Entity\Commercial|null
     */
    public function getCommercial()
    {
        return $this->commercial;
    }
}
