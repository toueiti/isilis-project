<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObjetCompaign
 *
 * @ORM\Table(name="objet_compaign")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\ObjetCompaignRepository")
 */
class ObjetCompaign
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="is_ative", type="string", length=255, nullable=true)
     */
    private $is_ative;

    /**
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="objet")
     */
    private $campaigns;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Commercial", inversedBy="objectCampaign")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id")
     */
    private $commercial;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ObjetCompaign
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->compaigns = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * to string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add campaign.
     *
     * @param \BackBundle\Entity\Campaign $campaign
     *
     * @return ObjetCompaign
     */
    public function addCampaign(\BackBundle\Entity\Campaign $campaign)
    {
        $this->campaigns[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign.
     *
     * @param \BackBundle\Entity\Campaign $campaign
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCampaign(\BackBundle\Entity\Campaign $campaign)
    {
        return $this->campaigns->removeElement($campaign);
    }

    /**
     * Get campaigns.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * Set name.
     *
     * @param string $is_active
     *
     * @return ObjetOperation
     */
    public function setIsActive($active)
    {
        $this->is_ative = $active;

        return $this;
    }

    /**
     * Get is_active.
     *
     * @return string
     */
    public function getIsActive()
    {
        return $this->is_ative;
    }

    /**
     * @return mixed
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * @param mixed $commercial
     */
    public function setCommercial($commercial)
    {
        $this->commercial = $commercial;
    }


}
