<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObjetOperation
 *
 * @ORM\Table(name="objet_operation")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\ObjetOperationRepository")
 */
class ObjetOperation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="is_ative", type="string", length=255, nullable=true)
     */
    private $is_ative;


    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="objet")
     */
    private $operations;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Commercial", inversedBy="objetOperation")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id")
     */
    private $commercial;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ObjetOperation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->operations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return ObjetOperation
     */
    public function addOperation(\BackBundle\Entity\Operation $operation)
    {
        $this->operations[] = $operation;

        return $this;
    }

    /**
     * Remove operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOperation(\BackBundle\Entity\Operation $operation)
    {
        return $this->operations->removeElement($operation);
    }

    /**
     * Get operations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Set name.
     *
     * @param string $is_active
     *
     * @return ObjetOperation
     */
    public function setIsActive($active)
    {
        $this->is_ative = $active;

        return $this;
    }

    /**
     * Get is_active.
     *
     * @return string
     */
    public function getIsActive()
    {
        return $this->is_ative;
    }

    /**
     * @return mixed
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * @param mixed $commercial
     */
    public function setCommercial($commercial)
    {
        $this->commercial = $commercial;
    }


}
