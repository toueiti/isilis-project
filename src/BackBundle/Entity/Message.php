<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\MessageRepository")
 */
class Message
{
    
    const CAISSE_SEND = 1;
    const AGENCE_SEND = 2;
    const ADMIN_SEND = 3;
    
    const CAISSE_RECIV = 1;
    const AGENCE_RECIV = 2;
    const ADMIN_RECIV = 3;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="source", type="integer")
     */
    private $source;

    /**
     * @var int
     *
     * @ORM\Column(name="dest", type="integer")
     */
    private $dest;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="valide_caisse", type="boolean")
     */
    private $valideCaisse = false;

    /**
     * @var string
     *
     * @ORM\Column(name="valide_agence", type="boolean")
     */
    private $valideAgence = false;

    /**
     * @var string
     *
     * @ORM\Column(name="seen_admin", type="boolean")
     */
    private $seenAdmin = false;

    /**
     * @var string
     *
     * @ORM\Column(name="seen_caisse", type="boolean")
     */
    private $seenCaisse = false;

    /**
     * @var string
     *
     * @ORM\Column(name="seen_agence", type="boolean")
     */
    private $seenAgence = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validatedAt", type="datetime", nullable=true)
     */
    private $validatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Operation", inversedBy="messages", cascade={"persist"})
     * @ORM\JoinColumn(name="operation_id", referencedColumnName="id", nullable=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $operation;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="messages", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Jointe", mappedBy="message")
     */
    private $jointes;

    /**
     * Get CAISSE_SEND.
     */
    public function getCAISSE_SEND() {
        return self::CAISSE_SEND;
    }

    /**
     * Get AGENCE_SEND.
     */
    public function getAGENCE_SEND() {
        return self::AGENCE_SEND;
    }

    /**
     * Get ADMIN_SEND.
     */
    public function getADMIN_SEND() {
        return self::ADMIN_SEND;
    }

    /**
     * Get CAISSE_RECIV.
     */
    public function getCAISSE_RECIV() {
        return self::CAISSE_RECIV;
    }

    /**
     * Get AGENCE_RECIV.
     */
    public function getAGENCE_RECIV() {
        return self::AGENCE_RECIV;
    }

    /**
     * Get ADMIN_RECIV.
     */
    public function getADMIN_RECIV() {
        return self::ADMIN_RECIV;
    }
    
    /**
     * constructeur
     */
    public function __construct() {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set source.
     *
     * @param int $source
     *
     * @return Message
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Message
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set operation.
     *
     * @param \BackBundle\Entity\Operation|null $operation
     *
     * @return Message
     */
    public function setOperation(\BackBundle\Entity\Operation $operation = null)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation.
     *
     * @return \BackBundle\Entity\Operation|null
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set user.
     *
     * @param \UserBundle\Entity\User|null $user
     *
     * @return Message
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \UserBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set valide.
     *
     * @param bool $valide
     *
     * @return Message
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide.
     *
     * @return bool
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Set dest.
     *
     * @param int $dest
     *
     * @return Message
     */
    public function setDest($dest)
    {
        $this->dest = $dest;

        return $this;
    }

    /**
     * Get dest.
     *
     * @return int
     */
    public function getDest()
    {
        return $this->dest;
    }

    /**
     * Set seenAdmin.
     *
     * @param bool $seenAdmin
     *
     * @return Message
     */
    public function setSeenAdmin($seenAdmin)
    {
        $this->seenAdmin = $seenAdmin;

        return $this;
    }

    /**
     * Get seenAdmin.
     *
     * @return bool
     */
    public function getSeenAdmin()
    {
        return $this->seenAdmin;
    }

    /**
     * Set seenCaisse.
     *
     * @param bool $seenCaisse
     *
     * @return Message
     */
    public function setSeenCaisse($seenCaisse)
    {
        $this->seenCaisse = $seenCaisse;

        return $this;
    }

    /**
     * Get seenCaisse.
     *
     * @return bool
     */
    public function getSeenCaisse()
    {
        return $this->seenCaisse;
    }

    /**
     * Set seenAgence.
     *
     * @param bool $seenAgence
     *
     * @return Message
     */
    public function setSeenAgence($seenAgence)
    {
        $this->seenAgence = $seenAgence;

        return $this;
    }

    /**
     * Get seenAgence.
     *
     * @return bool
     */
    public function getSeenAgence()
    {
        return $this->seenAgence;
    }

    /**
     * Set valideCaisse.
     *
     * @param bool $valideCaisse
     *
     * @return Message
     */
    public function setValideCaisse($valideCaisse)
    {
        $this->valideCaisse = $valideCaisse;

        return $this;
    }

    /**
     * Get valideCaisse.
     *
     * @return bool
     */
    public function getValideCaisse()
    {
        return $this->valideCaisse;
    }

    /**
     * Set valideAgence.
     *
     * @param bool $valideAgence
     *
     * @return Message
     */
    public function setValideAgence($valideAgence)
    {
        $this->valideAgence = $valideAgence;

        return $this;
    }

    /**
     * Get valideAgence.
     *
     * @return bool
     */
    public function getValideAgence()
    {
        return $this->valideAgence;
    }

    /**
     * Add jointe.
     *
     * @param \BackBundle\Entity\Jointe $jointe
     *
     * @return Message
     */
    public function addJointe(\BackBundle\Entity\Jointe $jointe)
    {
        $this->jointes[] = $jointe;

        return $this;
    }

    /**
     * Remove jointe.
     *
     * @param \BackBundle\Entity\Jointe $jointe
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeJointe(\BackBundle\Entity\Jointe $jointe)
    {
        return $this->jointes->removeElement($jointe);
    }

    /**
     * Get jointes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJointes()
    {
        return $this->jointes;
    }

    /**
     * Set validatedAt.
     *
     * @param \DateTime|null $validatedAt
     *
     * @return Message
     */
    public function setValidatedAt($validatedAt = null)
    {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    /**
     * Get validatedAt.
     *
     * @return \DateTime|null
     */
    public function getValidatedAt()
    {
        return $this->validatedAt;
    }
}
