<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Caisse
 *
 * @ORM\Table(name="caisse")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\CaisseRepository")
 */
class Caisse
{
    const STATUS_ACTIVE   = 1;
    const STATUS_SUSPENDU = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="zipCode", type="string", length=10)
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="string", length=10)
     */
    private $active;

    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\CaisseContact", inversedBy="caisse", cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id", nullable=true)
     */
    protected $contact;

    /**
     * @ORM\OneToMany(targetEntity="VisualPrint", mappedBy="caisse", cascade={"persist"})
     * 
     */
    protected $visualPrints;

    /**
     * @ORM\OneToMany(targetEntity="VisualWeb", mappedBy="caisse", cascade={"persist"})
     * 
     */
    protected $visualWebs;

    /**
     * @ORM\ManyToOne(targetEntity="Bank", inversedBy="caisses", cascade={"persist"})
     * @ORM\JoinColumn(name="bank_id", referencedColumnName="id")
     */
    protected $bank;

    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="caisse")
     */
    private $operation;

    /**
     * @ORM\ManyToOne(targetEntity="Agence", inversedBy="caisse")
     * @ORM\JoinColumn(name="agence_id", referencedColumnName="id")
     */
    private $agence;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\CaisseCollaborateur", mappedBy="caisse")
     */
    private $collaborateurs;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creatAt", type="date", nullable=true)
     */
    protected $creatAt;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_notif", type="boolean", nullable=true)
     */
    private $is_notif = false;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Caisse
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Commercial", inversedBy="caisse")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id")
     */
    private $commercial;

    /**
     * Set address.
     *
     * @param string $address
     *
     * @return Caisse
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zipCode.
     *
     * @param string $zipCode
     *
     * @return Caisse
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Caisse
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->visualPrint = new \Doctrine\Common\Collections\ArrayCollection();
        $this->visualWeb   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creatAt     = new \Datetime();
    }

    /**
     * Set contact.
     *
     * @param \UserBundle\Entity\CaisseContact|null $contact
     *
     * @return Caisse
     */
    public function setContact(\UserBundle\Entity\CaisseContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return \UserBundle\Entity\CaisseContact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Add visualPrint.
     *
     * @param \BackBundle\Entity\VisualPrint $visualPrint
     *
     * @return Caisse
     */
    public function addVisualPrint(\BackBundle\Entity\VisualPrint $visualPrint)
    {
        $this->visualPrints[] = $visualPrint;

        return $this;
    }

    /**
     * Remove visualPrint.
     *
     * @param \BackBundle\Entity\VisualPrint $visualPrint
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVisualPrint(\BackBundle\Entity\VisualPrint $visualPrint)
    {
        return $this->visualPrints->removeElement($visualPrint);
    }

    /**
     * Get visualPrints.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisualPrints()
    {
        return $this->visualPrints;
    }

    /**
     * Add visualWeb.
     *
     * @param \BackBundle\Entity\VisualWeb $visualWeb
     *
     * @return Caisse
     */
    public function addVisualWeb(\BackBundle\Entity\VisualWeb $visualWeb)
    {
        $this->visualWebs[] = $visualWeb;

        return $this;
    }

    /**
     * Remove visualWeb.
     *
     * @param \BackBundle\Entity\VisualWeb $visualWeb
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVisualWeb(\BackBundle\Entity\VisualWeb $visualWeb)
    {
        return $this->visualWebs->removeElement($visualWeb);
    }

    /**
     * Get visualWebs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisualWebs()
    {
        return $this->visualWebs;
    }

    /**
     * Set bank.
     *
     * @param \BackBundle\Entity\Bank|null $bank
     *
     * @return Caisse
     */
    public function setBank(\BackBundle\Entity\Bank $bank = null)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank.
     *
     * @return \BackBundle\Entity\Bank|null
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Add operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return Caisse
     */
    public function addOperation(\BackBundle\Entity\Operation $operation)
    {
        $this->operation[] = $operation;

        return $this;
    }

    /**
     * Remove operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOperation(\BackBundle\Entity\Operation $operation)
    {
        return $this->operation->removeElement($operation);
    }

    /**
     * Get operation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set agence.
     *
     * @param \BackBundle\Entity\Agence|null $agence
     *
     * @return Caisse
     */
    public function setAgence(\BackBundle\Entity\Agence $agence = null)
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * Get agence.
     *
     * @return \BackBundle\Entity\Agence|null
     */
    public function getAgence()
    {
        return $this->agence;
    }

    /**
     * Get active.
     *
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set active.
     *
     * @param string $active
     *
     * @return Caisse
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Add collaborateur.
     *
     * @param \UserBundle\Entity\CaisseCollaborateur $collaborateur
     *
     * @return Caisse
     */
    public function addCollaborateur(\UserBundle\Entity\CaisseCollaborateur $collaborateur)
    {
        $this->collaborateurs[] = $collaborateur;

        return $this;
    }
    /*
     * Remove collaborateur.
     *
     * @param \UserBundle\Entity\CaisseCollaborateur $collaborateur
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */

    public function removeCollaborateur(\UserBundle\Entity\CaisseCollaborateur $collaborateur)
    {
        return $this->collaborateurs->removeElement($collaborateur);
    }

    /**
     * Get collaborateurs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollaborateurs()
    {
        return $this->collaborateurs;
    }

    /**
     * Set creatAt.
     *
     * @param \DateTime $creatAt
     *
     * @return Operation
     */
    public function setCreatAt($creatAt)
    {
        $this->creatAt = $creatAt;

        return $this;
    }

    /**
     * Get creatAt.
     *
     * @return \DateTime
     */
    public function getCreatAt()
    {
        return $this->creatAt;
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/visuels';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $filename = date("Ymd_His").'_'.$this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(), $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return Campaign
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Add commercial.
     *
     * @param \UserBundle\Entity\Commercial $commercial
     *
     * @return Bank
     */
    public function addCommercial(\UserBundle\Entity\Commercial $commercial)
    {
        $this->commercial[] = $commercial;

        return $this;
    }

    /**
     * Remove commercial.
     *
     * @param \UserBundle\Entity\Commercial $commercial
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCommercial(\UserBundle\Entity\Commercial $commercial)
    {
        return $this->commercial->removeElement($commercial);
    }

    /**
     * Get commercial.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * Set commercial.
     *
     * @param \UserBundle\Entity\Commercial|null $commercial
     *
     * @return Bank
     */
    public function setCommercial(\UserBundle\Entity\Commercial $commercial = null)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Set isNotif.
     *
     * @param bool $isNotif
     *
     * @return Caisse
     */
    public function setIsNotif($isNotif)
    {
        $this->is_notif = $isNotif;

        return $this;
    }

    /**
     * Get isNotif.
     *
     * @return bool
     */
    public function getIsNotif()
    {
        return $this->is_notif;
    }
}
