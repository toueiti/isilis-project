<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Agence
 *
 * @ORM\Table(name="agence")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\AgenceRepository")
 */
class Agence
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE   = 1;
    const STATUS_ARCHIVE  = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="zipCode", type="string", length=10)
     */
    private $zipCode;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="active", type="string", length=10)
     */
    private $active;

    /**
     * @var string
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\AgenceContact", inversedBy="agence", cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id", nullable=true)
     */
    protected $contact;

    /**
     * @ORM\OneToMany(targetEntity="Caisse", mappedBy="agence")
     */
    private $caisse;

    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="agence")
     */
    private $operations;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Commercial", inversedBy="agence")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id")
     */
    private $commercial;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\AgenceCollaborateur", mappedBy="agence")
     */
    private $collaborateurs;

    public function __construct()
    {
        $this->caisse         = new ArrayCollection();
        $this->collaborateurs = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getAbsoluteIcone()
    {
        return null === $this->icon ? null : $this->getUploadRootDir().'/'.$this->icon;
    }

    public function getWebIcone()
    {
        return null === $this->icon ? null : $this->getUploadDir().'/'.$this->icon;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory icon where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/icones';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(), $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->icon = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return 
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address.
     *
     * @param string $address
     *
     * @return Agence
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zipCode.
     *
     * @param string $zipCode
     *
     * @return Agence
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Agence
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set icon.
     *
     * @param string $icon
     *
     * @return Agence
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set contact.
     *
     * @param \UserBundle\Entity\AgenceContact|null $contact
     *
     * @return Agence
     */
    public function setContact(\UserBundle\Entity\AgenceContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return \UserBundle\Entity\AgenceContact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Add caisse.
     *
     * @param \BackBundle\Entity\Caisse $caisse
     *
     * @return Agence
     */
    public function addCaisse(\BackBundle\Entity\Caisse $caisse)
    {
        $this->caisse[] = $caisse;

        return $this;
    }

    /**
     * Remove caisse.
     *
     * @param \BackBundle\Entity\Caisse $caisse
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCaisse(\BackBundle\Entity\Caisse $caisse)
    {
        return $this->caisse->removeElement($caisse);
    }

    /**
     * Get caisse.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCaisse()
    {
        return $this->caisse;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Agence
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return Agence
     */
    public function addOperation(\BackBundle\Entity\Operation $operation)
    {
        $this->operations[] = $operation;

        return $this;
    }

    /**
     * Remove operation.
     *
     * @param \BackBundle\Entity\Operation $operation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOperation(\BackBundle\Entity\Operation $operation)
    {
        return $this->operations->removeElement($operation);
    }

    /**
     * Get operations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Add collaborateur.
     *
     * @param \UserBundle\Entity\AgenceCollaborateur $collaborateur
     *
     * @return Agence
     */
    public function addCollaborateur(\UserBundle\Entity\AgenceCollaborateur $collaborateur)
    {
        $this->collaborateurs[] = $collaborateur;

        return $this;
    }

    /**
     * Remove collaborateur.
     *
     * @param \UserBundle\Entity\AgenceCollaborateur $collaborateur
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCollaborateur(\UserBundle\Entity\AgenceCollaborateur $collaborateur)
    {
        return $this->collaborateurs->removeElement($collaborateur);
    }

    /**
     * Get collaborateurs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollaborateurs()
    {
        return $this->collaborateurs;
    }

    /**
     * Set commercial.
     *
     * @param \UserBundle\Entity\Commercial|null $commercial
     *
     * @return Agence
     */
    public function setCommercial(\UserBundle\Entity\Commercial $commercial = null)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial.
     *
     * @return \UserBundle\Entity\Commercial|null
     */
    public function getCommercial()
    {
        return $this->commercial;
    }
}
