<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BackBundle\Form\VisualPrintType;
use BackBundle\Form\VisualWebType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Doctrine\ORM\EntityRepository;

class BankType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('zipCode')
            ->add('city')
            ->add('file',
                \Symfony\Component\Form\Extension\Core\Type\FileType::class,
                array(
                'required' => false
            ))
            ->add('chartPath',
                \Symfony\Component\Form\Extension\Core\Type\FileType::class,
                array(
                'required' => false
            ))
            ->add('contactName')
            ->add('contactRole')
            ->add('contactPhone', TelType::class, array(

                ))
            ->add('contactEmail')
            /* ->add('visualWebs', CollectionType::class,
              array(
              'entry_type' => VisualWebType::class,
              'allow_add' => true,
              'allow_delete' => true,
              'prototype' => true,
              'entry_options' => array(
              'with_file' => false,
              'attr' => array('class' => ''),
              )
              ))
              ->add('visualPrints', CollectionType::class,
              array(
              'entry_type' => VisualPrintType::class,
              'allow_add' => true,
              'allow_delete' => true,
              'prototype' => true,
              'entry_options' => array(
              'with_file' => false,
              'attr' => array('class' => ''),
              )
              )) */

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Bank'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_bank';
    }
}