<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FilterFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startdate', TextType::class,
                array('attr' => array(
                    'placeholder' => 'DU')))
            ->add('enddate', TextType::class,
                array('attr' => array(
                    'placeholder' => 'AU')));
    }

    public function getName()
    {
        return 'filter_form';
    }
}