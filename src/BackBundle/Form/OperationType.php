<?php

namespace BackBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class OperationType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $id_bank = (isset($options['id_bank']))? $options['id_bank'] :null;
        $builder
            ->add('name')
            ->add('objectif', EntityType::class, array(
                'class' => \BackBundle\Entity\ObjetOperation::class,
                'choice_label' => 'name',
                'placeholder' => "Objectif de l'opération"
            ))
            ->add('cibleCom')
            ->add('cibleIlig')
            ->add('modeTrans', ChoiceType::class, array(
                'choices' => array(
                    'Mode fichier' => 'mode_fichier',
                    'Mode platforme conseiller' => 'mode_platforme'
                ),
                'expanded' => true,
                'multiple' => false
            ))
            ->add('permanant')
            ->add('amount')
            ->add('object_vol')
            ->add('comment')
            ->add('startDate', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'form-control hasDatepicker'
                )
            ))
            ->add('endDate', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'form-control hasDatepicker'
                )
            ))
//            ->add('startDateOnline', DateTimeType::class, array(
//                'widget' => 'single_text',
//                'input' => 'datetime',
//                'format' => 'dd-MM-yyyy',
//                'attr' => array(
//                    'class' => 'form-control hasDatepicker'
//                )
//            ))
//            ->add('endDateOnline', DateTimeType::class, array(
//                'widget' => 'single_text',
//                'input' => 'datetime',
//                'format' => 'dd-MM-yyyy',
//                'attr' => array(
//                    'class' => 'form-control hasDatepicker'
//                )
//            ))
            ->add('support');
        if($options['commercial_id'] != 0) {
            $builder->add('bank', EntityType::class, array(
                'class' => 'BackBundle:Bank',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('b')
                        ->where('b.active = 1')
                        ->andWhere('b.commercial = ' . $options['commercial_id']);
                },
                'choice_label' => 'name',
                'placeholder' => 'Banque Désignée'
            ));
        }else{
            $builder->add('bank', EntityType::class, array(
                'class' => 'BackBundle:Bank',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('b')
                        ->where('b.active = 1');
                },
                'choice_label' => 'name',
                'placeholder' => 'Banque Désignée'
            ));
        }
        if(!is_null($id_bank)){
            $builder
                ->add('caisse', EntityType::class, array(
                'class' => 'BackBundle:Caisse',
                'query_builder' => function (EntityRepository $er) use ($id_bank) {
                    return $er->createQueryBuilder('b')
                        ->where('b.bank = :id_bank')
                        ->andWhere('b.active = 1')
                        ->setParameter('id_bank',$id_bank);
                },
                'choice_label' => 'name',
                'placeholder' => 'Caisse régional désignée'
            ))
                ->add('campaign', EntityType::class, array(
                    'class' => 'BackBundle:Campaign',
                    'query_builder' => function (EntityRepository $er) use ($id_bank) {
                        return $er->createQueryBuilder('b')
                            ->where('b.bank = :id_bank')
                            ->andWhere('b.active = 1')
                            ->setParameter('id_bank',$id_bank);
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Campagne'
                ));
        }else{
            $builder->add('caisse', EntityType::class, array(
                'class' => 'BackBundle:Caisse',
                'choice_label' => 'name',
                'placeholder' => 'Caisse régional désignée'
            ))
                ->add('campaign', EntityType::class, array(
                    'class' => 'BackBundle:Campaign',
                    'choice_label' => 'name',
                    'placeholder' => 'Campagne'
                ));
        }

        $builder
            ->add('visualWebs', CollectionType::class, array(
                'entry_type' => VisualWebType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'entry_options' => array(
                    'with_file' => true,
                    'attr' => array('class' => ''),
                )
            ))
            ->add('visualPrints', CollectionType::class, array(
                'entry_type' => VisualPrintType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'entry_options' => array(
                    'with_file' => true,
                    'attr' => array('class' => ''),
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Operation',
            'commercial_id'=> array(),
            'id_bank'=> null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_operation';
    }

}