<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VisualWebType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class,
                array(
                'required' => false,
                'choices' => array(
                    'JPG' => 'JPG',
                    'PDF' => 'PDF',
                    'PNG' => 'PNG'
                ),
                'attr' => array(
                    'class' => 'form-control vweb'
                ),
                'placeholder' => 'Type de fichier'
            ))
            ->add('largeur', TextType::class,
                array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control vweb',
                    'placeholder' => 'Largeur'
                )
            ))
            ->add('hauteur', TextType::class,
                array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control vweb',
                    'placeholder' => 'Hauteur'
                )
            ))
            ->add('unite', ChoiceType::class,
                array(
                'required' => false,
                'choices' => array(
                    'PX' => 'PX',
                    'Cm' => 'Cm'
                ),
                'attr' => array(
                    'class' => 'form-control vweb',
                ),
                'placeholder' => 'Unité'
        ));
        if ($options['with_file']) {
            $builder
                ->add('file',
                    \Symfony\Component\Form\Extension\Core\Type\FileType::class,
                    array(
                    'required' => false
            ));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'with_file' => false,
            'data_class' => 'BackBundle\Entity\VisualWeb'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_visualweb';
    }
}