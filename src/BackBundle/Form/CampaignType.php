<?php

namespace BackBundle\Form;

use BackBundle\Entity\ObjetCompaign;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;


class CampaignType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('cibleCom')
            ->add('cibleIlig')
            ->add('modeTrans', ChoiceType::class,
                array(
                    'choices' => array(
                        'Mode fichier' => 'mode_fichier',
                        'Mode platforme conseiller' => 'mode_platforme'
                    ),
                    'expanded' => true,
                    'multiple' => false
                ))
            ->add('amount')
            ->add('objet', EntityType::class, array(
                'class' => 'BackBundle:ObjetCompaign',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.is_ative = 1');
                },
                'choice_label' => 'name',
                'placeholder' => 'Objectif de la campagne'
            ))
            ->add('object_vol')
            ->add('description')
            ->add('comment', null, array('required' => false))
            ->add('permanent')
            ->add('startDate', DateTimeType::class,
                array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'form-control hasDatepicker'
                    )
                ))
            ->add('endDate', DateTimeType::class,
                array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'form-control hasDatepicker'
                    )
                ));
            if($options['commercial_id'] != 0) {
                $builder->add('bank', EntityType::class, array(
                    'class' => 'BackBundle:Bank',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('b')
                            ->where('b.active = 1')
                            ->andWhere('b.commercial = ' . $options['commercial_id']);
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Banque Désignée'
                ));
            }else{
                $builder->add('bank', EntityType::class, array(
                    'class' => 'BackBundle:Bank',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('b')
                            ->where('b.active = 1');
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Banque Désignée'
                ));
            }
        $builder->add('file')
            ->add('visualWebs', CollectionType::class,
                array(
                    'entry_type' => VisualWebType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'entry_options' => array(
                        'with_file' => true,
                        'attr' => array('class' => ''),
                    )
                ))
            ->add('visualPrints', CollectionType::class,
                array(
                    'entry_type' => VisualPrintType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'entry_options' => array(
                        'with_file' => true,
                        'attr' => array('class' => ''),
                    )
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Campaign',
            'commercial_id'=> array()
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_campaign';
    }


}