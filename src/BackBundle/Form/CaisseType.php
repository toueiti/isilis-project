<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Form\CaisseContactType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


class CaisseType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $id_commercial = (isset($options['id_commercial']))? $options['id_commercial'] :null;
        $builder
            ->add('name')
            ->add('address')
            ->add('zipCode')
            ->add('city')
            ->add('file');
        if(!is_null($id_commercial)){
          $builder
              ->add('bank', EntityType::class, array(
              'class' => 'BackBundle:Bank',
              'query_builder' => function (EntityRepository $er) use ($id_commercial) {
                  return $er->createQueryBuilder('b')
                      ->where('b.active = 1 and b.commercial = :id_commercial'  )
                      ->setParameter('id_commercial', $id_commercial);
              },
              'choice_label' => 'name',
              'placeholder' => "Banque d'appartenance"
          ));
        }else{
            $builder
                ->add('bank', EntityType::class, array(
                    'class' => 'BackBundle:Bank',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('b')
                            ->where('b.active = 1 '  );
                    },
                    'choice_label' => 'name',
                    'placeholder' => "Banque d'appartenance"
                ));
        }

        $builder
            ->add('contact', CaisseContactType::class,
                array(
                'label' => 'Contact'
            ))
            /* ->add('visualWebs', CollectionType::class,
              array(
              'entry_type' => VisualWebType::class,
              'allow_add' => true,
              'allow_delete' => true,
              'prototype' => true,
              'entry_options' => array(
              'with_file' => false,
              'attr' => array('class' => ''),
              )
              ))
              ->add('visualPrints', CollectionType::class,
              array(
              'entry_type' => VisualPrintType::class,
              'allow_add' => true,
              'allow_delete' => true,
              'prototype' => true,
              'entry_options' => array(
              'with_file' => false,
              'attr' => array('class' => ''),
              )
              )) */
            ->add('agence', EntityType::class,
                array(
                'class' => 'BackBundle:Agence'
                , 'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->where('a.active = 1');
                }
                , 'multiple' => false
                , 'required' => true
                , 'placeholder' => 'Agence affectée',
               ))
            ->add('commercial', EntityType::class,
                array(
                    'class' => \UserBundle\Entity\Commercial::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->where('c.del = 0');
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Commercial affecté'
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Caisse',
            'id_commercial' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_caisse';
    }
}