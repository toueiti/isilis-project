<?php

namespace CaisseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class OperationType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('objectif', EntityType::class,
                array(
                'class' => \BackBundle\Entity\ObjetOperation::class,
                'choice_label' => 'name',
                'placeholder' => "Objectif de l’opération"
            ))
            ->add('cibleCom')
            ->add('cibleIlig')
            ->add('modeTrans', ChoiceType::class, array(
                'choices' => array(
                    'Mode fichier' => 'mode_fichier',
                    'Mode platforme conseiller' => 'mode_platforme'
                ),
                'expanded' => true,
                'multiple' => false
            ))
            ->add('amount')
            ->add('object_vol')
            ->add('comment', TextareaType::class,
                array(
                'required' => false
            ))
            ->add('startDate', DateTimeType::class,
                array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'form-control hasDatepicker'
                )
            ))
            ->add('endDate', DateTimeType::class,
                array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'form-control hasDatepicker'
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Operation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'caissebundle_operation';
    }
}