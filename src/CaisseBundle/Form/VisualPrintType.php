<?php

namespace CaisseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VisualPrintType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('type', ChoiceType::class, array(
                    'choices'=>array(
                        'JPG'=>'JPG',
                        'PDF'=>'PDF',
                        'PNG'=>'PNG'
                    ),
                    'attr' => array(
                        'class' => 'form-control vweb'
                    ),
                    'placeholder'=> 'Type de fichier'
                ))
                ->add('largeur', TextType::class, array(
                    'label'=>'a',
                    'attr' => array(
                        'class' => 'form-control vweb',
                        'placeholder'=> 'Largeur'
                    )
                ))
                ->add('hauteur', TextType::class, array(
                    'label'=>'a',
                    'attr' => array(
                        'class' => 'form-control vweb',
                        'placeholder'=> 'Hauteur'
                    )
                ))
                ->add('unite', ChoiceType::class, array(
                    'choices'=>array(
                        'PX'=>'PX',
                        'Cm'=>'Cm'
                    ),
                    'attr' => array(
                        'class' => 'form-control vweb'
                    ),
                    'placeholder'=> 'Unité'
                ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\VisualPrint'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_visualprint';
    }


}
