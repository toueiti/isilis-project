<?php

namespace CaisseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use BackBundle\Form\VisualPrintType;
use BackBundle\Form\VisualWebType;

class CampaignType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cibleCom', ChoiceType::class,
                array(
                'choices' => array(
                    'Mobilité bancaire' => 'Mobilité bancaire',
                    'Conquête client' => 'Conquête client',
                    'RDV client' => 'RDV client',
                    'Fidélisation' => 'Fidélisation',
                    'Equipement' => 'Equipement',
                    'Parrainage' => 'Parrainage'
                ),
                'placeholder' => 'Cible de la campagne'
            ))
            ->add('description')
            ->add('comment')
            ->add('operation', CollectionType::class,
                array(
                'entry_type' => OperationType::class
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Campaign'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'caissebundle_campaign';
    }
}