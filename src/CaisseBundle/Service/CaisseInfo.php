<?php

namespace CaisseBundle\Service;

/**
 * Description of CaisseInfo
 *
 * @author hp
 */
class CaisseInfo
{
    protected $security;

    public function __construct($security)
    {
        $this->security = $security;
    }

    public function getCaisse()
    {
        $token = $this->security->getToken();
        $Roles = $token->getUser()->getRoles();
        if (in_array("ROLE_CAISSE_COLLABORATEUR", $Roles)) {
            $user   = $token->getUser();
            $collab = $user->getCaisseCollaborateur();
            return $collab->getCaisse();
        }
        $contact = $token->getUser()->getCaisseContact();
        return $contact->getCaisse();
    }

    public function getMessagesByCaisse($caisse, $em)
    {
        $result = $em->getRepository(\BackBundle\Entity\Message::class)->getMessagesByCaisse($caisse->getId());
        $output = array();
        $nbmsg  = 0;
        foreach ($result as $item) {
            $row       = array(
                'operation' => $item[0],
                'nbmsg' => $item['nbmsg']
            );
            $nbmsg     += $item['nbmsg'];
            $output [] = $row;
        }
        return array(
            'operations' => $output,
            'nbmsg' => $nbmsg
        );
    }
}