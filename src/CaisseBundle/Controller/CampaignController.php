<?php

namespace CaisseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BackBundle\Entity\Campaign;
use BackBundle\Form\CampaignType;
use BackBundle\Entity\VisualPrint;
use BackBundle\Entity\VisualWeb;
use BackBundle\Entity\Operation;
use BackBundle\Entity\Diffusion;

/**
 * @Route("/campaign")
 * @author toueiti slim <toueiti@gmail.com>
 * 
 */
class CampaignController extends Controller
{

    /**
     * @Route("/detail/{id}", name="caisse_campaign_detail")
     */
    public function detailAction(Campaign $campagn)
    {
        return $this->render('caisse/campaign/detail.html.twig',
                array(
                'campaign' => $campagn
        ));
    }

    /**
     * @Route("/new/custom", name="caisse_campaign_custom")
     */
    public function customAction(Request $request)
    {
        $caisse   = $this->get('caisse.caisse.info')->getCaisse();
        $campaign = new Campaign();
        $campaign->setBank($caisse->getBank());
        $campaign->setStartDateOnline(new \DateTime());
        $campaign->setEndDateOnline(new \DateTime());
        $campaign->setActive(true);
        $campaign->setPermanent(FALSE);
        $campaign->setAmount(0);
        $campaign->setCibleCom(' ');
        $campaign->setCibleIlig(' ');
        $campaign->setName(' ');
        $campaign->setModeTrans(' ');
        $campaign->setObjectVol(' ');
        $campaign->addOperation(new Operation());
        $form     = $this->createForm(\CaisseBundle\Form\CampaignType::class,
            $campaign);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                try {
                    $webs   = $request->get('webs');
                    $prints = $request->get('prints');
                    foreach ($campaign->getOperation() as $operation) {
                        $campaign->setAmount($operation->getAmount());
                        $campaign->setCibleCom($operation->getCibleCom());
                        $campaign->setCibleIlig($operation->getCibleIlig());
                        $campaign->setName($operation->getName());
                        $campaign->setModeTrans($operation->getModeTrans());
                        $campaign->setObjectVol($operation->getObjectVol());
                        $operation->setCampaign($campaign);
                        $operation->setCaisse($caisse);
                        $operation->setAgence($caisse->getAgence());
                        $operation->setBank($caisse->getBank());
                        $operation->setOrdred(TRUE);
                        $operation->setActive(Operation::STATUS_ACTIVE);
                        $operation->setPermanant(false);
                        $operation->setComment($campaign->getDescription());
                        $em->persist($operation);
                        if (is_array($webs)) {
                            foreach ($webs as $web) {
                                $vweb = $em->find(VisualWeb::class, $web);
                                if (is_object($vweb)) {
                                    $media = new VisualWeb();
                                    $media->setCaisse($vweb->getCaisse());
                                    $media->setHauteur($vweb->getHauteur());
                                    $media->setLargeur($vweb->getLargeur());
                                    $media->setType($vweb->getType());
                                    $media->setUnite($vweb->getUnite());
                                    $media->setOperation($operation);
                                    $em->persist($media);
                                }
                            }
                        }
                        if (is_array($prints)) {
                            foreach ($prints as $print) {
                                $vprint = $em->find(VisualPrint::class, $print);
                                if (is_object($vprint)) {
                                    $media = new VisualPrint();
                                    $media->setCaisse($vprint->getCaisse());
                                    $media->setHauteur($vprint->getHauteur());
                                    $media->setLargeur($vprint->getLargeur());
                                    $media->setType($vprint->getType());
                                    $media->setUnite($vprint->getUnite());
                                    $media->setOperation($operation);
                                    $em->persist($media);
                                }
                            }
                        }
                        $msg       = new \BackBundle\Entity\Message();
                        $msg->setSource($msg::CAISSE_SEND);
                        $msg->setDest($msg::ADMIN_RECIV);
                        $msg->setOperation($operation);
                        $msg->setBody($operation->getComment());
                        $msg->setUser($this->getUser());
                        $em->persist($msg);
                        /*                         * **************** Email send ********************* */
                        $diffusion = $em->getRepository(Diffusion::class)->findAll();
                        $cc        = array();
                        foreach ($diffusion as $item) {
                            $cc [] = $item->getEmail();
                        }
                        $message = (new \Swift_Message('ISILIS Plateforme'))
                            ->setFrom($this->container->getParameter('mailer_from'))
                            ->setTo($this->container->getParameter('mailer_admin'))
                            ->setCc($cc)
                            ->setBcc('toueiti@yahoo.fr')
                            ->setBody(
                            $this->container->get('templating')->render(
                                'back/email/operation_ordred.twig',
                                array('caisse' => $caisse
                                , 'operation' => $operation)
                            ), 'text/html'
                        );
                        //$this->container->get('mailer')->send($message);
                        /*                         * ******************** end email ****************** */
                    }
                    $em->persist($campaign);
                    $em->flush();
                    //$this->createOpreation($campaign, $caisse, $prints, $webs);
                    $this->get('session')->getFlashBag()->add('success',
                        'Mise à jour avec succès.');
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
            } else {
                $validator    = $this->get('validator');
                $errors       = $validator->validate($campaign);
                $errorsString = (string) $errors;
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !<br>'.$errorsString);
            }
            return $this->redirect($this->generateUrl('caisse_home'));
        }
        return $this->render('caisse/campaign/custom.html.twig',
                array(
                'form' => $form->createView(),
                'campaign' => $campaign
        ));
    }

    /**
     * créer opération à partir de la campaigne
     */
    public function createOpreation(Campaign $campaign,
                                    \BackBundle\Entity\Caisse $caisse, $prints,
                                    $webs)
    {
        $operation = new \BackBundle\Entity\Operation();
        $operation->setName($campaign->getName());
        $operation->setCampaign($campaign);
        $operation->setCaisse($caisse);
        $operation->setAgence($caisse->getAgence());
        $operation->setBank($caisse->getBank());
        $operation->setOrdred(TRUE);
        $operation->setActive(Operation::STATUS_IN_VALIDATION);
        $operation->setAmount($campaign->getAmount());
        $operation->setCibleCom($campaign->getCibleCom());
        $operation->setCibleIlig($campaign->getCibleIlig());
        $operation->setStartDate($campaign->getStartDate());
        $operation->setEndDate($campaign->getEndDate());
        $operation->setComment($campaign->getDescription());
        $operation->setPermanant(false);
        $em        = $this->getDoctrine()->getManager();
        $message   = new \BackBundle\Entity\Message();
        $message->setSource($message::CAISSE_SEND);
        $message->setDest($message::ADMIN_RECIV);
        $message->setOperation($operation);
        $message->setBody($operation->getComment());
        $message->setUser($this->getUser());
        if (is_array($webs)) {
            foreach ($webs as $web) {
                $vweb = $em->find(VisualWeb::class, $web);
                if (is_object($vweb)) {
                    $media = new VisualWeb();
                    $media->setCaisse($vweb->getCaisse());
                    $media->setHauteur($vweb->getHauteur());
                    $media->setLargeur($vweb->getLargeur());
                    $media->setType($vweb->getType());
                    $media->setUnite($vweb->getUnite());
                    $media->setCaisse($caisse);
                    $em->persist($media);
                }
            }
        }
        if (is_array($prints)) {
            foreach ($prints as $print) {
                $vprint = $em->find(VisualPrint::class, $print);
                if (is_object($vprint)) {
                    $media = new VisualPrint();
                    $media->setCaisse($vprint->getCaisse());
                    $media->setHauteur($vprint->getHauteur());
                    $media->setLargeur($vprint->getLargeur());
                    $media->setType($vprint->getType());
                    $media->setUnite($vprint->getUnite());
                    $media->setCaisse($caisse);
                    $em->persist($media);
                }
            }
        }
        try {
            $em->persist($message);
            $em->persist($operation);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        /*         * **************** Email send ********************* */
        $diffusion = $em->getRepository(Diffusion::class)->findAll();
        $cc        = array();
        foreach ($diffusion as $item) {
            $cc [] = $item->getEmail();
        }
        $message = (new \Swift_Message('ISILIS Plateforme'))
            ->setFrom($this->container->getParameter('mailer_from'))
            ->setTo($this->container->getParameter('mailer_admin'))
            ->setCc($cc)
            ->setBcc('toueiti@yahoo.fr')
            ->setBody(
            $this->container->get('templating')->render(
                'back/email/operation_ordred.twig',
                array('caisse' => $caisse
                , 'operation' => $operation)
            ), 'text/html'
        );
       // $this->container->get('mailer')->send($message);
        /*         * ******************** end email ****************** */
        return $operation;
    }
}