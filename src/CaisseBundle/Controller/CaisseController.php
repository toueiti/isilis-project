<?php

namespace CaisseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use CaisseBundle\Form\CaisseType;

class CaisseController extends Controller
{
    /**
     * @Route("/info", name="caisse_caisse_info")
     */
    public function infoAction(Request $request)
    {
        $caisse = $this->get('caisse.caisse.info')->getCaisse();
        $form = $this->createForm(CaisseType::class, $caisse);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($caisse);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Mise à jour avec succès.');
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
        return $this->render('caisse/caisse/info.html.twig', array(
            'caisse'=>$caisse,
            'form'=>$form->createView()
        ));
    }
}
