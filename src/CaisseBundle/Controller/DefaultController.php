<?php

namespace CaisseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BackBundle\Entity\Campaign;
use BackBundle\Entity\ObjetCompaign;

class DefaultController extends Controller
{

    /**
     * @Route("/home", options={"expose"=true}, name="caisse_home")
     */
    public function indexAction(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $years     = $em->getRepository(Campaign::class)->getYears();
        $form      = $this->createFormFilter($years);
        $form->handleRequest($request);
        $output    = array();
        $obj       = (object) '';
        $obj->id   = 0;
        $output [] = $obj;
        $caisse    = $this->get('caisse.caisse.info')->getCaisse();
        if ($form->isSubmitted()) {
            $data      = $request->get('form');
            //return new Response(json_encode($data));
            $campaigns = $em->getRepository(Campaign::class)->search($data, $caisse->getBank()->getId());
           // $campaigns = array_merge($output, $campaigns);
            return $this->render('caisse/default/search_result.html.twig',
                    array(
                    'campaigns' => $campaigns
            ));
        }
        $campaigns = $em->getRepository(Campaign::class)->serachByBankId($caisse->getBank()->getId());
       // $campaigns = array_merge($output, $campaigns);
        return $this->render('caisse/default/index.html.twig',
                array(
                'form' => $form->createView(),
                'campaigns' => $campaigns
        ));
    }

    /**
     * formulaire de recherche compagne
     * @author toueiti slim <toueiti@gmail.com>
     */
    public function createFormFilter($years)
    {
        $em        = $this->getDoctrine()->getManager();
        $caisse     = $this->get('caisse.caisse.info')->getCaisse();
        $campaignes = $em->getRepository(Campaign::class)->serachByBankId($caisse->getBank()->getId());
        return $form       = $this->createFormBuilder()
            ->add('type', EntityType::class,
                array(
                'class' => ObjetCompaign::class,
                'choice_label' => 'name',
                'placeholder' => 'Type de campagne'
            ))
            ->add('compaign', EntityType::class,
                array(
                'class' => Campaign::class,
                'choices' => $campaignes,
                'choice_label' => 'name',
                'placeholder' => 'Intitulé de campagne'
            ))
            ->getForm();
    }
}