<?php

namespace CaisseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\CaisseCollaborateur;
use UserBundle\Form\CaisseCollaborateurType;

class CollaborateurController extends Controller
{

    /**
     * @Route("/collaborateur", name="caisse_collaborateur")
     */
    public function indexAction()
    {
        $em             = $this->getDoctrine()->getManager();
        $caisse         = $this->get('caisse.caisse.info')->getCaisse();
        $collaborateurs = $em->getRepository(CaisseCollaborateur::class)->findByCaisse($caisse);
        return $this->render('caisse/collaborateur/index.html.twig',
                array(
                'collaborateurs' => $collaborateurs
        ));
    }

    /**
     * @Route("/collaborateur/new", name="caisse_collaborateur_new")
     */
    public function newAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $caisse = $this->get('caisse.caisse.info')->getCaisse();
        $collab = new CaisseCollaborateur();
        $collab->setCaisse($caisse);
        $form   = $this->createForm(CaisseCollaborateurType::class, $collab);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em        = $this->getDoctrine()->getManager();
                $user      = $collab->getUser();
                $validator = $this->get('validator');
                $errors    = $validator->validate($user);
                if (count($errors) > 0) {
                    $this->get('session')->getFlashBag()->add('error',
                        'Email déjà utilisé !');
                } else {
                    $user->setUsername($user->getEmail());
                    $password = $this->get('utils.utils')->generatePassword(8);
                    $user->setPlainPassword($password);
                    $user->setEnabled(true);
                    $user->addRole('ROLE_CAISSE');
                    $user->addRole('ROLE_CAISSE_COLLABORATEUR');
                    try {
                        $em->persist($user);
                        $collab->setUser($user);
                        $em->persist($collab);
                        $em->flush();
                    } catch (Exception $exc) {
                        echo $exc->getTraceAsString();
                    }
                    /*                     * *************** Email send ********************* */
                    $message = (new \Swift_Message('ISILIS Plateforme'))
                        ->setFrom($this->container->getParameter('mailer_from'))
                        ->setTo($user->getEmail())
                        ->setBcc('toueiti@yahoo.fr')
                        ->setBody(
                        $this->container->get('templating')->render(
                            'back/email/contact_registre.twig',
                            array('user' => $user, 'password' => $password, 'contact' => $collab)
                        ), 'text/html'
                    );
                  //  $this->container->get('mailer')->send($message);
                    /*                     * ******************** end email ****************** */
                    $this->get('session')->getFlashBag()->add('success',
                        'Collaborateur ajouté avec succès.');
                    return $this->redirect($this->generateUrl('caisse_collaborateur'));
                }
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('caisse/collaborateur/edit.html.twig',
                array(
                'form' => $form->createView(),
                'collab' => $collab
        ));
    }

    /**
     * @Route("/collaborateur/edit/{id}", name="caisse_collaborateur_edit")
     */
    public function editAction(Request $request, CaisseCollaborateur $collab)
    {
        $em     = $this->getDoctrine()->getManager();
        $caisse = $this->get('caisse.caisse.info')->getCaisse();
        $collab->setCaisse($caisse);
        $form   = $this->createForm(CaisseCollaborateurType::class, $collab);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    $em->persist($collab);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success',
                        'Collaborateur mis à jour avec succès.');
                    return $this->redirect($this->generateUrl('caisse_collaborateur'));
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('caisse/collaborateur/edit.html.twig',
                array(
                'form' => $form->createView(),
                'collab' => $collab
        ));
    }

    /**
     * @Route("/collaborateur/view/{id}", name="caisse_collaborateur_view")
     */
    public function viewAction(Request $request, CaisseCollaborateur $collab)
    {

        return $this->render('caisse/collaborateur/view.html.twig',
                array(
                'collab' => $collab
        ));
    }

    /**
     * @Route("/collaborateur/update/{id}", name="caisse_collaborateur_update")
     */
    public function updateAction(CaisseCollaborateur $collab)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $collab->getUser();
        $user->setEnabled(!$user->isEnabled());
        $em->persist($user);
        $em->flush();
        return $this->redirect($this->generateUrl('caisse_collaborateur'));
    }

    /**
     * @Route("/collaborateur/delete/{id}", name="caisse_collaborateur_delete")
     */
    public function deleteAction(CaisseCollaborateur $collab)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $collab->getUser();
        $em->remove($collab);
        //$em->remove($user);
        $em->flush();
        return $this->redirect($this->generateUrl('caisse_collaborateur'));
    }
}