<?php

namespace CaisseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Entity\VisualPrint;
use BackBundle\Entity\VisualWeb;
use CaisseBundle\Form\VisualPrintType;
use CaisseBundle\Form\VisualWebType;

/**
 * Gestion des média
 * @author slim toueiti
 */
class MediaController extends Controller
{
    /**
     * @Route("/media", name="caisse_media")
     */
    public function indexAction()
    {
        $contact = $this->getUser()->getCaisseContact();
        $caisse = $contact->getCaisse();
        return $this->render('caisse/media/index.html.twig', array(
            'caisse'=>$caisse
        ));
    }
    
    /**
     * @Route("/media/web/new", name="caisse_media_web_new")
     */
    public function newWebAction(Request $request) 
    {
        $contact = $this->getUser()->getCaisseContact();
        $caisse = $contact->getCaisse();
        $media = new VisualWeb();
        $media->setCaisse($caisse);
        $form = $this->createForm(VisualWebType::class, $media);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($media);
                $em->flush();
                return $this->redirect($this->generateUrl('caisse_media'));
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
        return $this->render('caisse/media/new_web.html.twig', array(
            'media'=>$media,
            'form'=>$form->createView()
        ));
    }
    
    /**
     * @Route("/media/print/new", name="caisse_media_print_new")
     */
    public function newPrintAction(Request $request) 
    {
        $contact = $this->getUser()->getCaisseContact();
        $caisse = $contact->getCaisse();
        $media = new VisualPrint();
        $media->setCaisse($caisse);
        $form = $this->createForm(VisualPrintType::class, $media);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($media);
                $em->flush();
            return $this->redirect($this->generateUrl('caisse_media'));
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
        return $this->render('caisse/media/new_print.html.twig', array(
            'media'=>$media,
            'form'=>$form->createView()
        ));
    }
    
    /**
     * @Route("/media/web/delete/{id}", name="caisse_media_web_delete")
     */
    public function deleteWebAction(VisualWeb $media) {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($media);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return $this->redirect($this->generateUrl('caisse_media'));
    }
    
    /**
     * @Route("/media/print/delete/{id}", name="caisse_media_print_delete")
     */
    public function deletePrintAction(VisualPrint $media) {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($media);
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return $this->redirect($this->generateUrl('caisse_media'));
    }
}
