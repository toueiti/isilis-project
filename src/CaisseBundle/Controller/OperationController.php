<?php

namespace CaisseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use BackBundle\Entity\Operation;
use CaisseBundle\Form\OperationType;
use BackBundle\Entity\Campaign;
use BackBundle\Entity\Message;
use BackBundle\Entity\VisualPrint;
use BackBundle\Entity\VisualWeb;
use BackBundle\Entity\Diffusion;

/**
 * @Route("/operation")
 *
 */
class OperationController extends Controller
{

    /**
     * @Route("/order/{id}", name="caisse_operation_order")
     */
    public function orderAction(Request $request, Campaign $campaign)
    {
        $caisse    = $this->get('caisse.caisse.info')->getCaisse();
        $operation = new Operation();
        $operation->setCampaign($campaign);
        $operation->setCaisse($caisse);
        $operation->setAgence($caisse->getAgence());
        $operation->setBank($caisse->getBank());
        $operation->setOrdred(TRUE);
        $operation->setPermanant(false);
        $operation->setActive(Operation::STATUS_ACTIVE);
        $form      = $this->createForm(OperationType::class, $operation);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em      = $this->getDoctrine()->getManager();
                $message = new Message();
                $message->setSource($message::CAISSE_SEND);
                $message->setDest(Message::ADMIN_RECIV);
                $message->setOperation($operation);
                $message->setBody($operation->getComment());
                $message->setUser($this->getUser());
                $webs    = $request->get('webs');
                if (is_array($webs)) {
                    foreach ($webs as $web) {
                        $vweb = $em->find(VisualWeb::class, $web);
                        if (is_object($vweb)) {
                            $media = new VisualWeb();
                            $media->setHauteur($vweb->getHauteur());
                            $media->setLargeur($vweb->getLargeur());
                            $media->setType($vweb->getType());
                            $media->setUnite($vweb->getUnite());
                            $media->setCaisse($caisse);
                            $em->persist($media);
                        }
                    }
                }
                $prints = $request->get('prints');
                if (is_array($prints)) {
                    foreach ($prints as $print) {
                        $vprint = $em->find(VisualPrint::class, $print);
                        if (is_object($vprint)) {
                            $media = new VisualPrint();
                            $media->setHauteur($vprint->getHauteur());
                            $media->setLargeur($vprint->getLargeur());
                            $media->setType($vprint->getType());
                            $media->setUnite($vprint->getUnite());
                            $media->setCaisse($caisse);
                            $em->persist($media);
                        }
                    }
                }
                try {
                    if ($operation->getComment() != NULL)
                            $em->persist($message);
                    $em->persist($operation);
                    $em->flush();
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }
                /*                 * **************** Email send ********************* */
                $diffusion      = $em->getRepository(Diffusion::class)->findAll();
                $cc = array();
                foreach ($diffusion as $item){
                    $cc [] = $item->getEmail();
                }
                $message = (new \Swift_Message('ISILIS Plateforme'))
                    ->setFrom($this->container->getParameter('mailer_from'))
                    ->setTo($this->container->getParameter('mailer_admin'))
                    ->setCc($cc)
                    ->setBcc('toueiti@yahoo.fr')
                    ->setBody(
                    $this->container->get('templating')->render(
                        'back/email/operation_ordred.twig',
                        array('caisse' => $caisse
                        , 'operation' => $operation)
                    ), 'text/html'
                );
                //$this->container->get('mailer')->send($message);
                /*                 * ******************** end email ****************** */
                $this->get('session')->getFlashBag()->add('success',
                    'Opération ajoutée avec succès.');
                return $this->redirect($this->generateUrl('caisse_home'));
            } else {
                $this->get('session')->getFlashBag()->add('error',
                    'Erreur dans le formulaire !');
            }
        }
        return $this->render('caisse/operation/order.html.twig',
                array(
                'form' => $form->createView(),
                'campaign' => $campaign
        ));
    }

    /**
     * @Route("/mes", name="caisse_operation_list")
     */
    public function listAction()
    {
        $caisse            = $this->get('caisse.caisse.info')->getCaisse();
        $em                = $this->getDoctrine()->getManager();
        $activeOperations  = $em->getRepository(Operation::class)->filterActiveOpertaionByCaisse($caisse->getId());
        $doneOperations    = $em->getRepository(Operation::class)->filterDoneOpertaionByCaisse($caisse->getId());
        //dump($doneOperations);die;
        $inValidOperations = $em->getRepository(Operation::class)->filterInActivationOpertaionByCaisse($caisse->getId());
        return $this->render('caisse/operation/list.html.twig',
                array(
                'active_oprs' => $activeOperations,
                'done_oprs' => $doneOperations,
                'in_valid_oprs' => $inValidOperations
        ));
    }

    /**
     * @Route("/detail/{id}", name="caisse_operation_detail")
     */
    public function detailAction(Operation $operation)
    {
        $em       = $this->getDoctrine()->getManager();
        $messages = $em->getRepository(Message::class)->getCaisseMessagesByOperationId($operation->getId());
        foreach ($messages as $message) {
            if (!$message->getSeenCaisse()) {
                $message->setSeenCaisse(true);
                $em->persist($message);
            }
        }
        try {
            $em->flush();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return $this->render('caisse/operation/detail.html.twig',
                array(
                'opr' => $operation,
                'messages' => $messages
        ));
    }
}