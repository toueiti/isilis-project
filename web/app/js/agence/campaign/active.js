$(document).ready(function () {
    $(document).on('click', '.img-detail', function () {
        $(".wait").css("display", "block");
        $(this).addClass('opened');
        $('.detaill').css('display', 'none');
        var id = $(this).data('id');
        var target = $(this).data('target');

        var url_campaign = Routing.generate('agence_campaign_detail', {id: id});
        $.get(url_campaign, function (data) {
            $('.target-detail-' + target).html(data);
            $('.target-detail-' + target).slideDown("slow");
            $(".wait").css("display", "none");
        });
    });
    $(document).ready(function () {
        $('#filter_campaign_banque.active').change(function () {
            $(".wait").css("display", "block");
            var id = $(this).val();
            var url_campaign = Routing.generate('filter_campaign', {id: id});
            $.get(url_campaign, function (data) {
                $('.filter-result').html(data);
                $(".wait").css("display", "none");
            })
        });
        $('#filter_campaign_banque.done').change(function () {
            $(".wait").css("display", "block");
            var id = $(this).val();
            var url_campaign = Routing.generate('filter_campaign_done', {id: id});
            $.get(url_campaign, function (data) {
                $('.filter-result').html(data);
                $(".wait").css("display", "none");
            })
        });
    })

    $(document).on("click", ".close-detail", function () {
        $(this).parent().css('display', 'none');
    })
});


