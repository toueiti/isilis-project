$(function() {
    $("#modal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).load(link.attr("href"));
    });
    
    $(document).on("hidden.bs.modal", function (e) {
        $(e.target).empty();
    });
});