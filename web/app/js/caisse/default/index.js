url_search = Routing.generate('caisse_home')
$(function () {
    $('body').find('.img-detail').on('click', function (e) {
        e.preventDefault();
        jQuery(".bloc-detail").slideUp();
        jQuery(".bloc-detail").removeClass("arrow-right arrow-left");
        $('.bloc-detail').html('');
        target = $(this).attr('data-target');
        $('#' + target).load($(this).attr('href'));
        $('#' + target).slideDown();
        if (jQuery(this).data("pos") == "right") {
            $('#' + target).addClass("arrow-right");
        } else {
            $('#' + target).addClass("arrow-left");
        }
        return false;
    });

    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).load(link.attr("href"));
    });

    $(document).on("hidden.bs.modal", function (e) {
        $(e.target).empty();
    });

    $('.search').on('change', function () {
        $.ajax({
            type: 'post',
            url: url_search,
            data: $('#form-search').serialize(),
            success: function (data) {
                $('#search-result').html(data);
            }
        });
    });
});

function getCampaigns(url, target) {
    $('.bloc-detail').html('');
    //target = $(this).attr('data-target');
    $('#' + target).load(url);
    return false;
}
lightbox.option({
    'alwaysShowNavOnTouchDevices': false,
    'resizeDuration': 200,
    'wrapAround': true,
    'positionFromTop': 200,
    'showImageNumberLabel': false
});

$(".menu-xs-trigger").click(function () {
    $('.right-sidebar').addClass("toggled");
});
$(".menu-xs-close").click(function () {
    $('.right-sidebar').removeClass("toggled");
});

$(document).on("click", ".close-detail", function () {
    console.log($(this).parents(".bloc-detail:first").attr("id"));
    $(this).parents(".bloc-detail:first").slideUp();
});