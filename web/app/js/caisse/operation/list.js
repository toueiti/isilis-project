$(function () {
    $('body').find('.opr-active-link').on('click', function (e) {
        $('.opr-active-link').removeClass("active");
        e.preventDefault();
        $('#active-opr-detail').load($(this).attr('href'));
        $(this).addClass("active");
        return false;
    });

    $('body').find('.opr-done-link').on('click', function (e) {
        $('.opr-done-link').removeClass("active");
        e.preventDefault();
        $('#done-opr-detail').load($(this).attr('href'));
        $(this).addClass("active");
        $('.tinymce').summernote();
        return false;
    });

    $('body').find('.opr-invalid-link').on('click', function (e) {
        $('.opr-invalid-link').removeClass("active");
        e.preventDefault();
        $('#invalid-opr-detail').load($(this).attr('href'));
        $(this).addClass("active");
        return false;
    });

    $(".opr-active-link-xs").click(function (e) {
        e.preventDefault();

        if ($(this).hasClass("active")) {
            $('.opr-active-link-xs').removeClass("active");
            $('#active-opr-detail').addClass("d-none");
            $(".list-group-item").removeClass("d-none");
            $(".operation_date").removeClass("d-none");
        } else {
            $(this).parents(".list-group-item:first").find(".operation_date").addClass("d-none");
            $('.opr-active-link-xs').removeClass("active");
            $(".list-group-item").removeClass("d-none");
            $('#active-opr-detail').load($(this).attr('href'));
            $(this).addClass("active");
            $('#active-opr-detail').removeClass("d-none");
            $(".list-group-item").not($(this).parents(".list-group-item:first")).addClass("d-none");
            $(this).find("i").removeClass("d-none");
        }
        return false;

    });

    $('body').find('.opr-done-link-xs').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass("active")) {
            $('.opr-done-link-xs').removeClass("active");
            $('#done-opr-detail').addClass("d-none");
            $(".list-group-item").removeClass("d-none");
            $(".operation_date").removeClass("d-none");
        } else {
            $(this).parents(".list-group-item:first").find(".operation_date").addClass("d-none");
            $('.opr-done-link').removeClass("active");
            $(".list-group-item").removeClass("d-none");
            $('#done-opr-detail').load($(this).attr('href'));
            $(this).addClass("active");
            $('#done-opr-detail').removeClass("d-none");
            $(".list-group-item").not($(this).parents(".list-group-item:first")).addClass("d-none");
            $(this).find("i").removeClass("d-none");
        }

        return false;
    });
    $('body').find('.opr-invalid-link-xs').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass("active")) {
            $('.opr-invalid-link-xs').removeClass("active");
            $('#invalid-opr-detail').addClass("d-none");
            $(".list-group-item").removeClass("d-none");
            $(".operation_date").removeClass("d-none");
        } else {
            $(this).parents(".list-group-item:first").find(".operation_date").addClass("d-none");
            $('.opr-done-link').removeClass("active");
            $(".list-group-item").removeClass("d-none");
            $('#invalid-opr-detail').load($(this).attr('href'));
            $(this).addClass("active");
            $('#invalid-opr-detail').removeClass("d-none");
            $(".list-group-item").not($(this).parents(".list-group-item:first")).addClass("d-none");
            $(this).find("i").removeClass("d-none");
        }

        return false;
    });
    $('.isi-navbar-brand a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(".list-group-item").removeClass("d-none");
    });

});