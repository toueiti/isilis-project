var vweb_counter = $('.vweb-count').length;
var vprint_counter = $('.vprint-count').length;

$(document).ready(function () {
    //$('#switch').click();
    $('.datepicker').each(function () {
        var t = $(this);
        t.datepicker({
            dateFormat: 'dd-mm-yy',
        });
    });       
    
    if($('#backbundle_campaign_permanent').is(':checked')){
        $('#date-content').hide();
        $('#backbundle_campaign_permanent').val(true);
    }
    
    $('#backbundle_campaign_permanent').on('change.bootstrapSwitch', function(e) {
        if(e.target.checked){
            $('#date-content').hide();
        } else {
            $('#date-content').show();
        }
    });

    $('.datepickerbtn').each(function () {
        var t = $(this).parent().parent().find('.datepicker');
        $(this).on("click", function () {
            t.datepicker("show");
        });
    });

    /*$("#switch").click(function () {
        var chek = $(this).parent().find('input');
        if ($(chek).prop("checked") == true) {
            $(chek).val(false);
        } else {
            $(chek).val(true);
        }
    });*/
    $('.add-vweb').click(function (e) {
        e.preventDefault();
        var proto_type = '<div class="col">' + $('#vwebs-proto').find('.proto-type').data('prototype') + '</div>';
        var proto_largeur = '<div class="col">' + $('#vwebs-proto').find('.proto-largeur').data('prototype') + '</div>';
        var proto_hauteur = '<div class="col">' + $('#vwebs-proto').find('.proto-hauteur').data('prototype') + '</div>';
        var proto_unite = '<div class="col">' + $('#vwebs-proto').find('.proto-unite').data('prototype') + '</div>';
        var proto_file = '<div class="col">' + $('#vwebs-proto').find('.proto-file').data('prototype') + '</div>';
        var newForm = '<div id="form-vweb' + vweb_counter + '" class="form-row">'
                + proto_type
                + proto_largeur
                + proto_hauteur
                + proto_unite
                + proto_file
                + '<div class="col"><button type="button" onclick="delete_vweb(' + vweb_counter + ')" class="btn btn-danger">x</button></div>'
                + '</div>';
        newForm = newForm.replace(/__name__/g, vweb_counter);
        vweb_counter++;
        $('#vweb-fields-list').append(newForm);
    });

    $('.add-vprint').click(function (e) {
        e.preventDefault();
        var proto_type = '<div class="col">' + $('#vprints-proto').find('.proto-type').data('prototype') + '</div>';
        var proto_largeur = '<div class="col">' + $('#vprints-proto').find('.proto-largeur').data('prototype') + '</div>';
        var proto_hauteur = '<div class="col">' + $('#vprints-proto').find('.proto-hauteur').data('prototype') + '</div>';
        var proto_unite = '<div class="col">' + $('#vprints-proto').find('.proto-unite').data('prototype') + '</div>';
        var proto_file = '<div class="col">' + $('#vprints-proto').find('.proto-file').data('prototype') + '</div>';
        var newForm = '<div id="form-vprint' + vprint_counter + '" class="form-row">'
                + proto_type
                + proto_largeur
                + proto_hauteur
                + proto_unite
                + proto_file
                + '<div class="col"><button type="button" onclick="delete_vprint(' + vprint_counter + ')" class="btn btn-danger">x</button></div>'
                + '</div>';
        newForm = newForm.replace(/__name__/g, vprint_counter);
        vprint_counter++;
        $('#vprint-fields-list').append(newForm);
    });
});

function delete_vweb(key)
{
    $('#form-vweb' + key).remove();
    vweb_counter--;
}
function delete_vprint(key)
{
    $('#form-vprint' + key).remove();
    vprint_counter--;
}
