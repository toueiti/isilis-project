var vweb_tab = new Array();
var vprint_tab = new Array();
$(document).ready(function () {
    $('#form-vweb').bind('submit', function () {
        $.ajax({
            type: 'post',
            url: Routing.generate('vweb_new'),
            data: $('form').serialize(),
            success: function(data) {
                if(data == '0'){
                    alert('Erreur non ajouté !');
                }
                else{
                    newElmt = '<div class="form-row">';
                    newElmt += '<div class="col">' + $('#backbundle_visualweb_type').val() + '</div>';
                    newElmt += '<div class="col">' + $('#backbundle_visualweb_largeur').val() + '</div>';
                    newElmt += '<div class="col">' + $('#backbundle_visualweb_hauteur').val() + '</div>';
                    newElmt += '<div class="col">' + $('#backbundle_visualweb_unite').val() + '</div>';
                    newElmt += '<div class="col"><a onclick="deleteVweb('+ data +')" href="'+ Routing.generate('vweb_delete') +'"><i class="fa fa-times"></i></a>';
                    newElmt += '</div>';
                    $('#form-vweb').append(newElmt);
                    vweb_tab[vweb_tab.length] = data;
                    $('#vwebs').val(vweb_tab.toString());
                }
            }
        });
        return false;
    });
    
    $('#form-vprint').bind('submit', function () {
        $.ajax({
            type: 'post',
            url: Routing.generate('vprint_new'),
            data: $('form').serialize(),
            success: function(data) {
                if(data == '0'){
                    alert('Erreur non ajouté !');
                }
                else{
                    newElmt = '<div class="form-row">';
                    newElmt += '<div class="col">' + $('#backbundle_visualprint_type').val() + '</div>';
                    newElmt += '<div class="col">' + $('#backbundle_visualprint_largeur').val() + '</div>';
                    newElmt += '<div class="col">' + $('#backbundle_visualprint_hauteur').val() + '</div>';
                    newElmt += '<div class="col">' + $('#backbundle_visualprint_unite').val() + '</div>';
                    newElmt += '<div class="col"><a onclick="deleteVprint('+ data +')" href="'+ Routing.generate('vprint_delete') +'"><i class="fa fa-times"></i></a>';
                    newElmt += '</div>';
                    $('#form-vprint').append(newElmt);
                    vprint_tab[vprint_tab.length] = data;
                    $('#vprints').val(vprint_tab.toString());
                }
            }
        });
        return false;
    });
});


