var vweb_counter = 1;
var vprint_counter = 1;
$(document).ready(function () {

    $('#backbundle_operation_caisse').html('<option selected>Caisse régionale désignée</option>');
    $('#backbundle_operation_campaign').html('<option selected>Campagne</option>');

    $('#backbundle_operation_bank').change(function () {
        var id = $(this).val();
//        alert(id);
        /* caisse data by bank */
        var url_caisse = Routing.generate('caisse_list_bank', {id: id});
        $.get(url_caisse, function (data) {
            $('#backbundle_operation_caisse').html(data);
        })
        /* campaign data by bank */
        var url_campaign = Routing.generate('campaign_list_bank', {id: id});
        $.get(url_campaign, function (data) {
            $('#backbundle_operation_campaign').html(data);
        })
    })

    $('.datepicker').each(function () {
        var t = $(this);
        t.datepicker({
            dateFormat: 'dd-mm-yy',
        });
    });

    $('.datepickerbtn').each(function () {
        var t = $(this).parent().parent().find('.datepicker');
        $(this).on("click", function () {
            t.datepicker("show");
        });
    });

    $("#switch1, #switch2").click(function () {
        var chek = $(this).parent().find('input');
        if ($(chek).prop("checked") == true) {
            $(chek).val(false);
        } else {
            $(chek).val(true);
        }
    });
    $('.add-vweb').click(function (e) {
        e.preventDefault();
        var proto_type = '<div class="col">' + $('#vwebs-proto').find('.proto-type').data('prototype') + '</div>';
        var proto_largeur = '<div class="col">' + $('#vwebs-proto').find('.proto-largeur').data('prototype') + '</div>';
        var proto_hauteur = '<div class="col">' + $('#vwebs-proto').find('.proto-hauteur').data('prototype') + '</div>';
        var proto_unite = '<div class="col">' + $('#vwebs-proto').find('.proto-unite').data('prototype') + '</div>';
        var proto_file = '<div class="col">' + $('#vwebs-proto').find('.proto-file').data('prototype') + '</div>';
        var newForm = '<div id="form-vweb' + vweb_counter + '" class="form-row">'
                + proto_type
                + proto_largeur
                + proto_hauteur
                + proto_unite
                + proto_file
                + '<div class="col"><button type="button" onclick="delete_vweb(' + vweb_counter + ')"><i class="fa fa-times"></i></button></div>'
                + '</div>';
        newForm = newForm.replace(/__name__/g, vweb_counter);
        vweb_counter++;
        $('#vweb-fields-list').append(newForm);
    });

    $('.add-vprint').click(function (e) {
        e.preventDefault();
        var proto_type = '<div class="col">' + $('#vprints-proto').find('.proto-type').data('prototype') + '</div>';
        var proto_largeur = '<div class="col">' + $('#vprints-proto').find('.proto-largeur').data('prototype') + '</div>';
        var proto_hauteur = '<div class="col">' + $('#vprints-proto').find('.proto-hauteur').data('prototype') + '</div>';
        var proto_unite = '<div class="col">' + $('#vprints-proto').find('.proto-unite').data('prototype') + '</div>';
        var proto_file = '<div class="col">' + $('#vprints-proto').find('.proto-file').data('prototype') + '</div>';
        var newForm = '<div id="form-vprint' + vprint_counter + '" class="form-row">'
                + proto_type
                + proto_largeur
                + proto_hauteur
                + proto_unite
                + proto_file
                + '<div class="col"><button type="button" onclick="delete_vprint(' + vprint_counter + ')"><i class="fa fa-times"></i></button></div>'
                + '</div>';
        newForm = newForm.replace(/__name__/g, vprint_counter);
        vprint_counter++;
        $('#vprint-fields-list').append(newForm);
    });
});

function delete_vweb(key)
{
    $('#form-vweb' + key).remove();
    vweb_counter--;
}
function delete_vprint(key)
{
    $('#form-vprint' + key).remove();
    vprint_counter--;
}