
$(document).ready(function () {
    $('.notifParameter').change(function () {
        var ligne = $(this);
        var id = ligne.val();
        if($(this).is(':checked')){
            var url = Routing.generate("parametre_notif_active", {id: id});
        }else{
            var url = Routing.generate("parametre_notif_desacive", {id: id});
        }
        $.ajax({
            url: url,
            beforeSend: function () {
                ligne.append('<div class="spinner"><i class="fa fa-spinner fa-pulse"></i></div>');
            },
            success: function (data) {
                ligne.find('.spinner').remove();
            }
        });
    })

    // $('#exporter').click(function (e) {
    //     e.preventDefault();
    //     var req = new XMLHttpRequest();
    //     req.open("GET", Routing.generate('export_contact', true);
    //     req.responseType = "blob";
    //
    //     req.onload = function (event) {
    //         var blob = req.response;
    //         console.log(blob.size);
    //         var link=document.createElement('a');
    //         link.href=window.URL.createObjectURL(blob);
    //         link.download='contact-caisse.csv';
    //         link.click();
    //     };
    //
    //     req.send();
    // });
});

