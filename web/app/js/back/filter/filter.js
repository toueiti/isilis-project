$(document).ready(function () {
    if ($('.enddate').val() != '') {
        $('.filter-date').html($('.startdate').val() + ' AU ' + $('.enddate').val());
    }
    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy',
    }).val();
});

function getfDate( element ) {
    dateFormat = 'dd/mm/yy';
    var date;
    try {
        date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
        date = null;
    }    
    return date;
}