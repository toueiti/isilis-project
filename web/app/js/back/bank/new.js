var vweb_counter = 1;
var vprint_counter = 1;

$(document).ready(function () {
    $('.add-vweb').click(function (e) {
        e.preventDefault();
        var proto_type = '<div class="col">' + $('#vwebs-proto').find('.proto-type').data('prototype') + '</div>';
        var proto_largeur = '<div class="col">' + $('#vwebs-proto').find('.proto-largeur').data('prototype') + '</div>';
        var proto_hauteur = '<div class="col">' + $('#vwebs-proto').find('.proto-hauteur').data('prototype') + '</div>';
        var proto_unite = '<div class="col">' + $('#vwebs-proto').find('.proto-unite').data('prototype') + '</div>';
        //var proto_file = '<div class="col">' + $('#vwebs-proto').find('.proto-file').data('prototype') + '</div>';
        var newForm = '<div id="form-vweb'+vweb_counter+'" class="form-row">' 
                + proto_type 
                + proto_largeur
                + proto_hauteur 
                + proto_unite
        //+ proto_file
                + '<div class="col"><button type="button" onclick="delete_vweb('+vweb_counter+')"><i class="fa fa-times"></i></button></div>'
                + '</div>';
        newForm = newForm.replace(/__name__/g, vweb_counter);
        vweb_counter ++;
        $('#vweb-fields-list').append(newForm);
    });
    
    $('.add-vprint').click(function (e) {
        e.preventDefault();
        var proto_type = '<div class="col">' + $('#vprints-proto').find('.proto-type').data('prototype') + '</div>';
        var proto_largeur = '<div class="col">' + $('#vprints-proto').find('.proto-largeur').data('prototype') + '</div>';
        var proto_hauteur = '<div class="col">' + $('#vprints-proto').find('.proto-hauteur').data('prototype') + '</div>';
        var proto_unite = '<div class="col">' + $('#vprints-proto').find('.proto-unite').data('prototype') + '</div>';
        //var proto_file = '<div class="col">' + $('#vprints-proto').find('.proto-file').data('prototype') + '</div>';
        var newForm = '<div id="form-vprint'+vprint_counter+'" class="form-row">' 
                + proto_type 
                + proto_largeur
                + proto_hauteur 
                + proto_unite
        //+ proto_file
                + '<div class="col"><button type="button" onclick="delete_vprint('+vprint_counter+')"><i class="fa fa-times"></i></button></div>'
                + '</div>';
        newForm = newForm.replace(/__name__/g, vprint_counter);
        vprint_counter ++;
        $('#vprint-fields-list').append(newForm);
    });
    
    /**
     * set preview image before upload
     */
    $("#backbundle_bank_file").on('change', function () {
        if (typeof (FileReader) != "undefined") {
            var image_holder = $("#image-holder");
            image_holder.empty();
            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "img-thumbnail"
                }).appendTo(image_holder);
            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    });
    
    /**
     * set preview image before upload
     */
    $("#backbundle_bank_chartPath").on('change', function () {
        if (typeof (FileReader) != "undefined") {
            var image_holder = $("#image-charte");
            image_holder.empty();
            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "img-thumbnail"
                }).appendTo(image_holder);
            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    });
});

function delete_vweb(key)
{
    $('#form-vweb'+key).remove();
    vweb_counter --;
}
function delete_vprint(key)
{
    $('#form-vprint'+key).remove();
    vprint_counter --;
}