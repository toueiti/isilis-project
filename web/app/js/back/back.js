$(document).ready(function () {
    $(".btn-left-menu").click(function (e) {
        e.stopPropagation();
        $(".menu-black.menu-black-left").removeClass("toggled");
        $('.right-sidebar').addClass("toggled");
    });
    $(".menu-xs-close").click(function () {
        $('.right-sidebar').removeClass("toggled");
    });
    $(".menu-xs-trigger").click(function (e) {
        e.stopPropagation();
        $('.right-sidebar').removeClass("toggled");
        $(".menu-black.menu-black-left").addClass("toggled");
    });
    $(window).click(function () {
        $(".right-sidebar").removeClass("toggled");
        $(".menu-black.menu-black-left").removeClass("toggled");
    });
});