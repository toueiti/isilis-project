-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 04 Juin 2018 à 00:13
-- Version du serveur :  5.7.22-0ubuntu0.16.04.1
-- Version de PHP :  7.2.4-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `isilis`
--

-- --------------------------------------------------------

--
-- Structure de la table `agence`
--

CREATE TABLE `agence` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL,
  `zipCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `agence`
--

INSERT INTO `agence` (`id`, `contact_id`, `name`, `address`, `zipCode`, `city`, `active`, `icon`) VALUES
(9, 9, 'Pouce Bleu', '19 rue de ponthièvre', '75008', 'Paris', '2', 'agence.png'),
(10, 10, 'Pource bleu', '17 rue hoche', '92400', 'Courbevoie', '2', 'Agency-Logo-1-5269.gif'),
(11, 11, 'pouce bleu', '20 rue hoche', '92400', 'courbevoie', '2', 'Agency-Logo-1-5269.gif'),
(12, 12, 'Agence007', '19 rue Hoche', '92400', 'Courbevoie', '1', 'logo_agence007.png'),
(13, 13, 'testa', 'addresse', '262621', 'ville', '2', 'images.jpg'),
(14, 14, 'testa', 'addresse', '262621', 'ville', '2', 'images.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `agence_collaborateur`
--

CREATE TABLE `agence_collaborateur` (
  `id` int(11) NOT NULL,
  `agence_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fonction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `agence_contact`
--

CREATE TABLE `agence_contact` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fonction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `agence_contact`
--

INSERT INTO `agence_contact` (`id`, `user_id`, `name`, `fonction`, `phone`) VALUES
(9, 49, 'Maxime Trabattoni', 'CEO', '0651084510'),
(10, 51, 'Florian Sellier', 'Dirigeant', '0235650683'),
(11, 54, 'florian sellier', 'direicteur', '0236660672'),
(12, 57, 'Maxime', 'Chef de projet', '0235650683'),
(13, 61, 'contact', 'fonction', '+33 56565'),
(14, 62, 'contact', 'fonction', '+33 56565');

-- --------------------------------------------------------

--
-- Structure de la table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `commercial_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL,
  `zipCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactRole` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactPhone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `creatAt` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bank`
--

INSERT INTO `bank` (`id`, `commercial_id`, `name`, `address`, `zipCode`, `city`, `icon`, `contactName`, `contactRole`, `contactPhone`, `contactEmail`, `active`, `creatAt`) VALUES
(7, 4, 'Binance', '212 rue de la cryptomonnaie', '75002', 'Paris', 'logobanque.png', 'Michel Berger', 'directeur', '0666666666', 'braxton.miatetima@gmail.com', '1', '2018-06-01'),
(8, 4, 'Société générale', 'Porte de versailles', '75000', 'Paris', '246x0w.jpg', 'Jhon Doe', 'Commercial', '+33621212121', 'sayari.slim@yopmail.com', '1', '2018-06-01'),
(9, 4, 'Caisse d\'épargne', '130 Rue de Courcelles', '75017', 'Paris', 'caisse-depargne.jpg', 'François Pérol', 'Directeur', '3241', 'braxton.miatetima@gmail.com', '1', '2018-06-01'),
(10, 4, 'BNP PARIBAS', '130 Boulevard Massena, C.c. \'le Kiosque De Choisy\'', '75013', 'Paris', 'bnp-paribas.jpg', 'Julie Durand', 'directeur', '0890 706 956', 'juliedurand@yopmail.com', '1', '2018-06-03'),
(11, 4, 'Crédit Agricole', '24 Avenue de la République,', '94700', 'Maisons Alfort', 'credit-agricole.jpg', 'Jean Dupuis', 'directeur', '01 43 76 29 40', 'jeandupuis@yopmail.com', '1', '2018-06-03');

-- --------------------------------------------------------

--
-- Structure de la table `caisse`
--

CREATE TABLE `caisse` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `agence_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL,
  `zipCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `creatAt` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `caisse`
--

INSERT INTO `caisse` (`id`, `contact_id`, `bank_id`, `agence_id`, `name`, `address`, `zipCode`, `city`, `active`, `creatAt`) VALUES
(29, 29, 7, 9, 'Ile de France', '16 rue hoche', '92400', 'Courbevoie', '1', '2018-06-01'),
(30, 30, 8, 9, 'Côte Azur', 'Nice', '06600', 'Nice', '1', '2018-06-01'),
(31, 31, 7, 9, 'Sud-Est', '88 rue de courcelles', '75008', 'Paris', '1', '2018-06-01'),
(32, 32, 7, 9, 'Sud-Ouest', '19 rue Hoche', '33000', 'Bordeaux', '1', '2018-06-01'),
(33, 33, 9, 12, 'Caisse d\'épargne', '139 rue de Paris', '75017', 'Paris', '1', '2018-06-01'),
(34, 34, 9, 12, 'BNP Paribas - Montreuil Valmy 3', '257 Rue de Paris', '93100', 'Montreuil', '0', '2018-06-03');

-- --------------------------------------------------------

--
-- Structure de la table `caisse_collaborateur`
--

CREATE TABLE `caisse_collaborateur` (
  `id` int(11) NOT NULL,
  `caisse_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fonction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `caisse_collaborateur`
--

INSERT INTO `caisse_collaborateur` (`id`, `caisse_id`, `user_id`, `name`, `fonction`, `phone`) VALUES
(4, 32, 59, 'Philippe Maestre', 'Assistant Marketing', '0648270241'),
(5, 30, 63, 'Sayari Slim', 'Dev', '55425421');

-- --------------------------------------------------------

--
-- Structure de la table `caisse_contact`
--

CREATE TABLE `caisse_contact` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fonction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `caisse_contact`
--

INSERT INTO `caisse_contact` (`id`, `user_id`, `name`, `fonction`, `phone`) VALUES
(29, 52, 'Maxime', 'Directeur ile de france', '0655254212'),
(30, 53, 'Fabrice', 'Agent', '+33623232323'),
(31, 55, 'laurent lulu', 'directeur', '0643532792'),
(32, 56, 'alexandre arnaud', 'Directeur Agence', '0648270241'),
(33, 60, 'Trabattoni Maxime', 'directeur', '0666823610'),
(34, 64, 'Marie Fresne', 'Directrice général', '0 820 82 00 01');

-- --------------------------------------------------------

--
-- Structure de la table `campaign`
--

CREATE TABLE `campaign` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `objet_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cibleCom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cibleIlig` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modeTrans` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_vol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `comment` longtext COLLATE utf8_unicode_ci NOT NULL,
  `permanent` tinyint(1) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_date_online` date NOT NULL,
  `end_date_online` date NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creatAt` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `campaign`
--

INSERT INTO `campaign` (`id`, `bank_id`, `objet_id`, `name`, `cibleCom`, `cibleIlig`, `modeTrans`, `amount`, `object_vol`, `description`, `comment`, `permanent`, `start_date`, `end_date`, `start_date_online`, `end_date_online`, `path`, `active`, `creatAt`) VALUES
(14, 7, 9, 'Parrainage', 'Nouveaux clients', 'jeunes - de 25 ans', 'mode_fichier', '20', '150', 'Offre parrainage', 'Offre valable jusqu\'à 10 filleuls par mois', 1, '2018-06-01', '2018-06-01', '2018-06-01', '2018-06-01', '20180601_114041_parrainage-offre.jpg', '2', '2018-06-01'),
(15, 8, 9, 'Campagne d\'été', 'Agences de voyage', 'critère 01', 'mode_fichier', '500', 'Objectif volumétrie', 'descriptif commercial', 'Remarques/ Notes explicatives', 1, '2018-06-01', '2018-06-01', '2018-06-01', '2018-06-01', '20180601_131325_Composition_1.gif', '2', '2018-06-01'),
(16, 7, 9, 'Binance', 'Jeunes de 28 ans', 'aucun', 'mode_fichier', '50€', '15000 personnes', 'TEST', 'test', 0, '2018-06-01', '2018-06-01', '2018-06-01', '2018-06-01', '20180601_134212_campagne.png', '2', '2018-06-01'),
(17, 9, 9, 'Jeunes Diplômés 2018', '16-25 ans', 'aucun', 'mode_fichier', '80 €', '20 000 personnes', 'Un coup de main de 80€ pour vos vacances', 'test', 0, '2018-06-01', '2018-06-14', '2018-06-01', '2018-06-14', '20180601_141646_ce_jeunes_diplomes_2018_header (1).jpg', '1', '2018-06-01'),
(18, 8, 10, 'Etre en Panne', '30 à 50 ans', 'aucun', 'mode_fichier', '0€', '1500', 'Campagne d\'acquisition sur blog et forum', 'Quand pensez vous livrer la version print ?', 1, '2018-06-03', '2018-06-03', '2018-06-03', '2018-06-03', '20180603_230133_societe-generale-1.jpg', '1', '2018-06-03');

-- --------------------------------------------------------

--
-- Structure de la table `commercial`
--

CREATE TABLE `commercial` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fonction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `commercial`
--

INSERT INTO `commercial` (`id`, `user_id`, `name`, `fonction`, `phone`) VALUES
(4, 50, 'florent de la vegas', 'commercial', '0651084510'),
(5, 58, 'Albert Dupontel', 'Commercial', '0648270241');

-- --------------------------------------------------------

--
-- Structure de la table `connection_history`
--

CREATE TABLE `connection_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_login` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `connection_history`
--

INSERT INTO `connection_history` (`id`, `user_id`, `last_login`) VALUES
(182, 48, '2018-06-01'),
(183, 48, '2018-06-01'),
(184, 48, '2018-06-01'),
(185, 48, '2018-06-01'),
(186, 48, '2018-06-01'),
(187, 48, '2018-06-01'),
(188, 48, '2018-06-01'),
(189, 50, '2018-06-01'),
(190, 50, '2018-06-01'),
(191, 48, '2018-06-01'),
(192, 48, '2018-06-01'),
(193, 54, '2018-06-01'),
(194, 53, '2018-06-01'),
(195, 48, '2018-06-01'),
(196, 48, '2018-06-01'),
(197, 50, '2018-06-01'),
(198, 50, '2018-06-01'),
(199, 56, '2018-06-01'),
(200, 50, '2018-06-01'),
(201, 53, '2018-06-01'),
(202, 53, '2018-06-01'),
(203, 48, '2018-06-01'),
(204, 48, '2018-06-01'),
(205, 56, '2018-06-01'),
(206, 48, '2018-06-01'),
(207, 56, '2018-06-01'),
(208, 56, '2018-06-01'),
(209, 48, '2018-06-01'),
(210, 56, '2018-06-01'),
(211, 48, '2018-06-01'),
(212, 48, '2018-06-01'),
(213, 57, '2018-06-01'),
(214, 56, '2018-06-01'),
(215, 57, '2018-06-01'),
(216, 48, '2018-06-01'),
(217, 57, '2018-06-01'),
(218, 56, '2018-06-01'),
(219, 48, '2018-06-01'),
(220, 57, '2018-06-01'),
(221, 56, '2018-06-01'),
(222, 48, '2018-06-01'),
(223, 49, '2018-06-01'),
(224, 53, '2018-06-01'),
(225, 48, '2018-06-01'),
(226, 49, '2018-06-01'),
(227, 48, '2018-06-01'),
(228, 48, '2018-06-01'),
(229, 49, '2018-06-01'),
(230, 48, '2018-06-01'),
(231, 53, '2018-06-01'),
(232, 48, '2018-06-01'),
(233, 56, '2018-06-01'),
(234, 49, '2018-06-01'),
(235, 53, '2018-06-01'),
(236, 57, '2018-06-01'),
(237, 48, '2018-06-01'),
(238, 48, '2018-06-01'),
(239, 57, '2018-06-01'),
(240, 57, '2018-06-01'),
(241, 56, '2018-06-01'),
(242, 57, '2018-06-01'),
(243, 53, '2018-06-01'),
(244, 48, '2018-06-01'),
(245, 56, '2018-06-01'),
(246, 48, '2018-06-01'),
(247, 56, '2018-06-01'),
(248, 56, '2018-06-01'),
(249, 57, '2018-06-01'),
(250, 48, '2018-06-01'),
(251, 48, '2018-06-01'),
(252, 56, '2018-06-01'),
(253, 48, '2018-06-01'),
(254, 48, '2018-06-01'),
(255, 56, '2018-06-01'),
(256, 56, '2018-06-01'),
(257, 57, '2018-06-01'),
(258, 48, '2018-06-02'),
(259, 53, '2018-06-02'),
(260, 53, '2018-06-02'),
(261, 48, '2018-06-02'),
(262, 48, '2018-06-02'),
(263, 48, '2018-06-02'),
(264, 48, '2018-06-03'),
(265, 56, '2018-06-03'),
(266, 57, '2018-06-03'),
(267, 53, '2018-06-03');

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(48, 'admin', 'admin', 'admin_admin@symfony.com', 'admin_admin@symfony.com', 1, NULL, '$2y$13$ftmJi/KxTyW1RVtDYIndFOlhIRPNWk5uCJDhcj/PQiIdqsKGGcqH2', '2018-06-03 01:02:06', NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}'),
(49, 'contact@poucebleu.fr', 'contact@poucebleu.fr', 'contact@poucebleu.fr', 'contact@poucebleu.fr', 1, NULL, '$2y$13$c/fxK1ngMOcliweEpyDkuu0Kg/Z9NTqN8pDRBz/U.AqVlUjJDHpyu', '2018-06-01 13:33:53', NULL, NULL, 'a:1:{i:0;s:11:"ROLE_AGENCE";}'),
(50, 'maxime.trabattoni@gmail.com', 'maxime.trabattoni@gmail.com', 'maxime.trabattoni@gmail.com', 'maxime.trabattoni@gmail.com', 1, NULL, '$2y$13$ckgJVOw8Ot3lv6Pf/jRlx.EpYu/99lr9VXtNiuAnNkUzl1T/qcCHC', '2018-06-01 11:00:44', NULL, NULL, 'a:1:{i:0;s:14:"ROLE_COMERCIAL";}'),
(51, 'florian@agence007.com', 'florian@agence007.com', 'florian@agence007.com', 'florian@agence007.com', 1, NULL, '$2y$13$rddqkiqTPhDPJVJqkp3Ut.t5DyWqkiSog19LIm2douvWmeuSJEKHy', NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_AGENCE";}'),
(52, 'maxime@agence007.com', 'maxime@agence007.com', 'maxime@agence007.com', 'maxime@agence007.com', 1, NULL, '$2y$13$gJzYnqupWWcQ6LLKAKdqjOoy8IoJF5FgofkkAPVUYw3D1WZ244jz2', NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_CAISSE";}'),
(53, 'sayari.slim@gmail.com', 'sayari.slim@gmail.com', 'sayari.slim@gmail.com', 'sayari.slim@gmail.com', 1, NULL, '$2y$13$HG1pdEu9rHu8uoHxbr0dwez3uC3wgYK2fZFFnY44AK/ejzVazBpci', '2018-06-03 23:13:30', NULL, NULL, 'a:1:{i:0;s:11:"ROLE_CAISSE";}'),
(54, 'floriansellier@yahoo.fr', 'floriansellier@yahoo.fr', 'floriansellier@yahoo.fr', 'floriansellier@yahoo.fr', 1, NULL, '$2y$13$nhFtXyz1HcC.bgPEX4laI.afW9FSYFtVBYwISti5v6dT4j/EUK2t2', '2018-06-01 10:29:48', NULL, NULL, 'a:1:{i:0;s:11:"ROLE_AGENCE";}'),
(55, 'max@lookingforguru.com', 'max@lookingforguru.com', 'max@lookingforguru.com', 'max@lookingforguru.com', 1, NULL, '$2y$13$yDuXZx6WaCvLs1CL5jSVgeaCWERIrO0xMoQVmgYWslcjrixd6EB.a', NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_CAISSE";}'),
(56, 'alexarnaud92@gmail.com', 'alexarnaud92@gmail.com', 'alexarnaud92@gmail.com', 'alexarnaud92@gmail.com', 1, NULL, '$2y$13$IWnImsyv8AJv0CZAF6gVzufisRvb4Nv36tJF6Iv0n6mDxUa8mvkom', '2018-06-03 21:36:22', NULL, NULL, 'a:1:{i:0;s:11:"ROLE_CAISSE";}'),
(57, 'mathilde@agence007.com', 'mathilde@agence007.com', 'mathilde@agence007.com', 'mathilde@agence007.com', 1, NULL, '$2y$13$jf07RdzTEhchaDZuZYRa3OhpDO61tWbnC9CJyvUw.HGIJ8Ckv1W96', '2018-06-03 21:56:23', NULL, NULL, 'a:1:{i:0;s:11:"ROLE_AGENCE";}'),
(58, 'alex@agence007.com', 'alex@agence007.com', 'alex@agence007.com', 'alex@agence007.com', 1, NULL, '$2y$13$AkeaYVnqofSwGZ.oXU12uuAnPcC/nZ25Ae/VzwumMXXFK8Vp0GboC', NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_COMERCIAL";}'),
(59, 'p.maestre@ozon-it.com', 'p.maestre@ozon-it.com', 'p.maestre@ozon-it.com', 'p.maestre@ozon-it.com', 1, NULL, '$2y$13$26ahsb7nseY7mQK7T/7pz.oIxrO3u1J5mseVRgThG2QC7zniPQZc.', NULL, NULL, NULL, 'a:2:{i:0;s:11:"ROLE_CAISSE";i:1;s:25:"ROLE_CAISSE_COLLABORATEUR";}'),
(60, 'max@yopmail.com', 'max@yopmail.com', 'max@yopmail.com', 'max@yopmail.com', 1, NULL, '$2y$13$hEWblE2/HFvSAtJW6jBkjeuvb..Gdj8dXLHGFA9OI7A8oFnqcbk/W', NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_CAISSE";}'),
(61, 'email@yopmail.com', 'email@yopmail.com', 'email@yopmail.com', 'email@yopmail.com', 1, NULL, '$2y$13$zFrx8TvDRlTny8K2B6K7OeYKJS2rRkGraNdCd5W/23ajPAwr/uKAG', NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_AGENCE";}'),
(62, 'email564654@yopmail.com', 'email564654@yopmail.com', 'email564654@yopmail.com', 'email564654@yopmail.com', 1, NULL, '$2y$13$02NjXGoHHhq2B9d2/PwT2Oo4j1RC1L2uzhWar2JgWh2.e.xuPYmXm', NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_AGENCE";}'),
(63, 'sayari@gmail.com', 'sayari@gmail.com', 'sayari@gmail.com', 'sayari@gmail.com', 1, NULL, '$2y$13$TG23avO4oZeeOvlQgnKWOuOl8vzrG4gUiAeGXiAFZT7KAjpdWmSme', NULL, NULL, NULL, 'a:2:{i:0;s:11:"ROLE_CAISSE";i:1;s:25:"ROLE_CAISSE_COLLABORATEUR";}'),
(64, 'mariefresne@yopmail.com', 'mariefresne@yopmail.com', 'mariefresne@yopmail.com', 'mariefresne@yopmail.com', 1, NULL, '$2y$13$R1T63egtGG.eqnmryIUUIO91K3nWdQngervJI5D/6CJFW9qwYZh72', NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_CAISSE";}');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `operation_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `source` int(11) NOT NULL,
  `dest` int(11) NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `valide_caisse` tinyint(1) NOT NULL,
  `valide_agence` tinyint(1) NOT NULL,
  `seen_admin` tinyint(1) NOT NULL,
  `seen_caisse` tinyint(1) NOT NULL,
  `seen_agence` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`id`, `operation_id`, `user_id`, `source`, `dest`, `body`, `valide_caisse`, `valide_agence`, `seen_admin`, `seen_caisse`, `seen_agence`, `createdAt`) VALUES
(20, 5, 56, 1, 3, '-', 0, 0, 1, 1, 1, '2018-06-01 11:46:25'),
(21, 5, 56, 1, 3, '<p>Bonjour,</p><p>Pourriez-vous rajouter le visuel de la carte gold en haut à droite ?</p>', 1, 0, 1, 1, 1, '2018-06-01 11:47:39'),
(22, 5, 56, 1, 3, '<p>test<br></p>', 1, 0, 1, 1, 1, '2018-06-01 11:47:51'),
(23, 5, 56, 1, 3, '<p>test 2</p>', 1, 0, 1, 1, 1, '2018-06-01 11:49:43'),
(24, 5, 56, 1, 3, '<p>Salut ça va&nbsp;</p><p><br></p>', 1, 1, 1, 1, 1, '2018-06-01 12:07:08'),
(25, 5, 48, 3, 1, 'ok merci', 1, 0, 1, 1, 1, '2018-06-01 12:08:30'),
(26, 5, 48, 3, 1, 'yoyoyoy', 1, 0, 1, 1, 1, '2018-06-01 12:08:35'),
(27, 5, 56, 1, 3, '<p>hello fab&nbsp;</p><p><br></p>', 1, 0, 1, 1, 0, '2018-06-01 12:25:11'),
(28, 5, 49, 3, 1, 'plop', 1, 0, 1, 1, 0, '2018-06-01 12:26:02'),
(29, 5, 49, 3, 1, 'ah', 1, 0, 1, 1, 0, '2018-06-01 12:26:32'),
(30, 5, 56, 1, 3, '<p>lalalalalallalallalala&nbsp;</p>', 1, 1, 1, 1, 0, '2018-06-01 12:27:21'),
(31, 5, 48, 3, 1, 'ola', 1, 0, 1, 1, 0, '2018-06-01 12:28:52'),
(32, 5, 48, 3, 1, 'test encore', 1, 0, 1, 1, 0, '2018-06-01 12:48:26'),
(33, 7, 53, 1, 3, 'aucune remarque', 0, 0, 1, 1, 0, '2018-06-01 13:05:13'),
(34, 7, 53, 1, 3, '<p>merci pour cette remarque !</p>', 1, 0, 1, 1, 0, '2018-06-01 13:26:52'),
(35, 7, 53, 1, 3, '<p>Message de retour.</p>', 1, 0, 1, 1, 0, '2018-06-01 13:31:12'),
(36, 7, 53, 1, 3, '<p>Message de retour.</p>', 1, 0, 1, 1, 0, '2018-06-01 13:32:05'),
(37, 8, 53, 1, 3, 'sddf', 0, 0, 1, 1, 0, '2018-06-01 13:38:29'),
(38, 9, 56, 1, 3, 'test', 0, 0, 1, 1, 0, '2018-06-01 13:58:00'),
(39, 9, 56, 1, 3, '<p>je n\'aime pas cette bannière&nbsp;</p>', 1, 0, 1, 1, 0, '2018-06-01 13:58:37'),
(40, 7, 53, 1, 3, '<p>Hello Max</p>', 1, 0, 1, 1, 0, '2018-06-01 14:29:11'),
(41, 10, 57, 2, 3, '<p>super ce visuel <br></p>', 1, 1, 1, 0, 1, '2018-06-01 14:33:38'),
(42, 10, 57, 2, 3, '<p>j\'aime beaucoup <br></p>', 0, 1, 1, 0, 1, '2018-06-01 14:35:20'),
(43, 7, 53, 1, 3, '<p>test html <b>content </b>merci de<a href="http://ne pas tenir" target="_blank"> ne pas tenir </a>compte de ce message !</p><p><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOQAAADPCAYAAADlPH14AAAAAXNSR0IArs4c6QAAHOZJREFUeAHtXQnYHUWVZU0kQADZEsISCGtYhtUAUQiChEXZxBiQYCaAEQaBUTZRZJEZIQKCg4OgSBRENhmHxSBr2HcYQDYBCYGEPUAIgbDonJO8/mnqr/e67u3l9XLv952/u6tv3ao6fW9XdXW9/uebL3/ZCkW8A4zNvygrwRgwBjoxsBhOTgX+CfwDOBgwMQaMgS4xcDLKnQkwICNYUHbpYlixzWZgEJr/AbA9cDfwDPBDYA4wAVgQMDEGjIGCGDgG5TzQKutAbNlD9gfWBJ4G/gwsDJgYA8ZAAQxcjTL+tVXOktiyt9yxdTwA2xeA64HPtNJsYwwYAzkxsADscmb1X2L2L8X+8a009owrA08BDMo+gIkxYAzkxMCKsMshKoMukp2ww6Hqx8CmrcTlsX0dOK11bBtjwBjIgYGNYZMBuQWwC8Cg+yLwKjANmB+IZBR23gb6Rgm2NQaaygCHlnnIjJbR9bGdAvD4fwEOY28F1gXWAyiXAW8BO/PAxBgwBvJhgD3hTS3T7P0uAthrzgKmAvsDkfDcGdGBbY2BpjKwUI4N/xVsHwd8C+AqneHAhQCHrd8F+NqDs65fAZjG8ybGQKMZyGvISlJ/CkwHzgLeBG4D+LqDveaDwIfAlsDeANNXAUyMAWMgRwZWgu1HAD5DDgYiYfAdD7CHXgG4A3gFMDEGGs1AnkNWEsuX/8OAs4EbgLEAg+95gMKAfQ4YDLwIpJEfI3O08MBnZw8k8tnVpN4MVNoP8g5IXvr3gLEA3z2eBIwEuGrncWAMcCHAXvKPgFY45D0c6LTqp9M5bbl55BsMo9sBawPLAMsCnwVmAxxpEBxN3APcCfBxwGQeA3Xyg8Ku6WiUxFcgfNXxzxhGYL+T7I6TG7ZR+AnS47Z8+2u2yVuG5FVRiZ8DzwK+urdL42TZX4FjgeWApkvV/aCr129+lD4UuB14A1gQcIVB+BLA3oBOyf0+QFz648ANbp8DlzEgWaeJACe4fHWWpL3fsjUE2yZKlf0g8+vF58QRwCiAv3fkPp2NQZcka0DhCcDVXQxpDNS3gUeByDm/if24HImD6FynbdkCcjzqzSDqVGfNuVmweRDg8omkWktV/SDzi8LhUojj8LnxNuAEgJMvSwCRfAc7+0UHre1Z2HISZmXgiNY+Hfh0IJK+2JkOhJRfloBcFPX9fWCdQ9rVTuc6lLEk0ASpoh/kdl1GwPJsgMEyATgeOBE4F5gMRA7Doek5wP8AfFbi889dwOEAg47PlZy8oHD4Sns7AUzjRMahAJ+XaDeSA7AT2U/aliEg+6G+dwjqPA26nJ3+LXAl8DDwMZDU1uj8fdBdCqi7VM0Pcr8e66KEvwGRI/i2HEqNARYCdgV+DNwMUJfByQXoDFbKqgDTBwAM1JkAHYuBeQxAWQBIKjNej24HJNt9DRCvU7v9W6A3EpgfcGUVJJA73rDa5Y+nPwi96EaH3dpJ1fygsAvAi85ekAvI6TDHOzgTx88DUwA6DCcyvgaMBj4AIifaBPubtY6vam256oe9L3viQQBlTyDKE7LtdkD+LrC+J0GPI4Qk4YzzM0BI26+HHh23jlI1Pyj0GtCRfg5cAXBSZkQL22DLwDsZiDsQA2w14PMAez+euxxYGviodcy08wAGMMmPhMOxuK2k/W4G5L6BdT0lalzgdjD0XgOS2s7z0cgCu7WSKvlB14g/CCVz5vQV4GbgXOAw4MvAiBa+i+27AIeq7AEvA+g4fEZikEbPWjx+GNgeiGRb7IQ4YVynWwHJHv3NgPreCJ0FAKnwZschf7ytvn3e0IZLjZdcv0p+0HUqt0YNTgfiw68hOB4D/AS4G3gZmBzDA9inM/0CoJPx+SeeH4dz5Xr89Tldp7RuBeSkgLryprPe3Jbp/lwSUAa5eRzw8akrtfu5quQH3WcLNVgLOB84AbgUYJDxeZE95raAr0f4NtKjwPqRR4fPmNF5ybYbAfmVwLr+BnpphDc68hrCx/g0BZUob5X8oES0zVtZc2rMWa7E/tbAIq1aLo8th1JHAFcDHObSseikfNXBoe9pwEYAhYEd4niuTtEBOT/qyR7erYfvOE3viCLmyuX467Ptpr0EvcXmZan036r4QWlJXh8144QPnXQG4DoKJ3H4rMjg/RwQST/sjAOeBZ4E2Mu6eUOOiw7I3QPreQ/0spAdYSSEB+ocm0WBXbSxBsrmMD+0vXG9ov2gizTlW/TCMH+n8iLwghR9IXhziTtCu/2shpAc/k8LLJO9JPmsqnCBSTs+k9KL9oOqcpxY74HQCH0Z7rsoRV6ILwgcZlBiy8MV+Gzua7svbVS42VJpVskPxMT5JlXERgrKwOfJvgWVlbYYDrFD5FEosVfLSq4VGDpIoFsm1Sr5QZl4y7QuS8Da24DvTh+aVlQPyQmTWYF1nQC9LKU/jH0IhHIyNMvCC7BVJT9Q0VGVHpKvQuhsVRAOBRcNrOhtgXqhajOh+EioMvSqNmytkh8ILkO1VD+D6nISIvSu306vqB7yBkFd+TyUtUgmPB7KuvAc7VXND1RUVKGH3BctG6BqXfGZOFzlhE6ITIcSbzRZy/0Cg1ygPlig303VKvmBmqeyBySXeXHBQFVkO1S0T2BlJYETaHKumtTurhLjXdKtmh+oaSp7QO6Blq2ubl3xGXcSFMkVSHkIF09w2B4qXN5XdqmaH5SdT3X9HkBO95lwtifN1fEdF/EM+aKgbuPUrCRnnCqoB39xs1Cyya5qVM0P1GSVuYf8Elq1sdMyPnPx1xNllLVRKclL/r/l2AiJ7X6oh8tzjlUTm66aH4gbGM9Q5oA8Kl7R1v6Z2M7xpJchaQthJZ4W6kvUpbaHS4wXrFs1P0hFT1kDcjO0alunZTNx/EsnrUyHmwsqw2E3f9WSlzwnNMwvNpRRqugHqXgsa0Ae6WkVg5Grdcoqkh6SrzzyFOlyvLL2kFX0gzyva1dsc/LF/WnN+0gb2KrNRdj6Jm2S0vKc1FncU+dO9ZncaktemxEw3Kl837mI37zqJLVbRT+QtrGXfhl7yMNRS7deFyAtj5fovQhRJnBo5da5kylpD9bJlu+cxv66PkNdTKuiH6SmS+JEqQsLMLACdLgiIy78QTI/glVmWV9YOU3ASIqYLlFu6a6nyJNXlqr6QWo+yhaQh6JFfZ1W/QnHkml8J3shh9JfTbyac634bpGQSJl6yKr6gYRvr26ZAnJJ1JCr+V2Z4CaU8FgakPyESd4iLaMsPWSV/SD1NS1TQB6I1rg/sZqMtKy+OZOarA4G1ulwzndKGiw+G0lp0jKkN5Wk8rXnq+wH2jb35CtLQPKnNRymuFKF3nE5VJpfWpeINFgktiNdaRn9kVHajqisrLZV9oNMOChLQI5Fa5Z3WvQIjsu6TC5eVWnvyLxvxA3ktK8pY+Wc6hJqdiwUq+oHoW3sqFeGgORPazjF7UoVekfWeVW34gHHbwXopFXRlLFK2kJT5K+6H6Ro+idZyxCQe6I6Qz6p0ty9Kfh7iZNW1kNNrzKrgMZoytC0JaumVN0PMuGhDAHpWzx8OlrHDydXQTROLH0loeFBU4amLZq6+fJU3Q98bRKndTsgt0eNN3Jq/TqOz3PSyny4krByH0C/iJtNlQKyDn4gdAO/ercD8mhPtc5CGn8NURWR9iqaoaSGC005gzQFZZCnDn6QAQ2y9ZeZFBgz8jnsbxM75i7v6gzIKom0h9T0XBo+NOUsqykoZZ66+EFKGuZl72YP6Xtm4FBVM12fCRkKI1xVEvoN1sh8Ub2/ppxlokoWuK2DH2RGV7cCci20YDenFXyu4mROlUTTo8wpqIGacniDKfL7OnXxg8wuabcC8gi0wC37YqQ9n1nLijFUt4CcH7QVuVqnLn6Qmbe5QZGZ4Q6GOHEwxnO+KgsB4lXXDPE0PVe8zNB9bTmam0xoneJ6dfKDeLtS7XcjIA9Djfs4tf4zjvmfoKomGufVBoqUG205mpuMtG7Ur5MfaNrvzVN0QPIZZbynJqd40qqQpHFevocsQrTlFDFkrZsfZHY9iw7Ig1DzxZ3a343jW520qhzWsYfkrz7ylrr5QWZ8FRmQi6DWvp9YVbV35EXQ9Cbankt60bXluDdMablJ+nX0g6Q2B58vMiDHolbLOTV7EsdXOmlVOtT0JkUsmyOH2nI0bZJcs7FQrpsfSNrfUbeogGz305pTUbt/dKxhuU9qnPfDgpqkLSfPHrKufpDZJS0qIL+GGq/m1JpfRrvASavaoSYgtT2XlBttOXkGZF39QHpt2uoXFZC+5VFnoFba55y2DSr4hMZ5tT2XtGnacjQ3mdC61dUPQtufqFdEQI5ELTZ0asJ/CXCOk1bFQ43zansuKT/acjQ3mZC61dkPQtofpFNEQPp+WnM2asd/nlN10QSktueScqUtR7pYPrRedfaDUA4S9fIOyGGowQinFlxBwn8rV3Xhus/FFI34WJFHk0VbDr/8lrXU2Q8y5SrvgPQ9M/wWLXg501Z0xxjfpzEopaINlKLK6SstKEC/zn4Q0PxwlTwDcm1UYzenKnzFwVcddRBtT1LUax5tOVkHZN39IFNfzjMgj0BN3R7kCqQ9nWkLumdMG5Bl7yG17Wp3JeruB+3arUrPKyBXRG328dSoysvk3OZoexJtz+WWn3SsLUfbLl99muAHvnar0/IKyMNQI/cnVjch7X51TcuXUduTFNVDRv+UVcpclgHZBD8I4Xd/KE0BtkhSziMgl0Kh4z0F16l3ZPO0AantuTyUJiZpytK2y61MU/zAbbd7fAgSfgXwq/BcmdYPaCt5BCR/WuO+Dvg/pF3XthbVPKHtSYrqIcmqpixtu9yr2BQ/cNsdPz4YB/FXfFxU75tx7smTdUA26ac12p6EQ8miRFMWF4CnlSb5gY8rfijsNIBvFLgg4gXgduDrAANyAOCVrANyHEpZ1inpORxf5qTV4dB9Rq5Dm9iGLHyiSX7gXneOMK4FdgbWAc4DVgImApMAfqpmDOCVLMiPDPOu8L3oILblnUIzdIqZKOWuljtNr6UlQFOWtl1RHZvmB1G7ueXz4V+A1YHPA+yMtgLeBy4GKL8Ehs/d8/xJS37c5CgcrBpPwP5rwG+ctLocarnTBImWM01ZfHfsvj+WlN80P4i44SMMg5EjRAbc2wB7yM2By4F3AcqDwLaA1394N8tKfA+r/wXj72VVQMnsaJ+1NEGibbq2LDqLdlTTND+Irg1HgmsA6wJvAJsBdwFTgQOASBiYnPQcCEyLEqOtN0qjk4LtjtDdwNGfheNfOGl1OsyKuzJyom1bE/2A149D1bEAA4/BSHkA4D6fKXljnA3sBHCmlbLkvM2n/2qJ/7QV/1Tur6E0w1Ws0XHde0jNpfL1jnX3A/K0K/ACcBUPIGsBmwJ3AuwR+d0onuey0WEAhUPaXpLFkJWrD7Z2LH+I49OdtLodam9m2mGkhj9tWZqbTVP9gNeF/n9r7AJtg/0jAPaGHJ6eD4wHGBd7AAzOF4FeonWquCHfXfEPUGChdZYsuCsrP5pJnab6Aa8hb0acwDwX2BaYBKwG/DfAXnM7YDRwILAlcA7glbRONRRWd3Es8648wUmr46GWO42za/nTliXtWZvsB7w2qwADgb2AzQBOiN0L3AosAkwGOETlJCc7qtMAr6QdsrJbdi/6NUh7zFtavRI160TJgMtXnqxoy5LOsDbZD6JrujJ2+Oz4CnACsBvwEsB4GAw8DDwKcMEA30t6RXuXp7GVgG94rJ7iSatjkgXkvKvadD8gC+z1GJDTgdUBvmFgMC4IjARuAv4I8J0kddpKmoD8d1hd2LHMWaXbnbS6Hkp7kW7wUEQP2XQ/4HW9A2BAngjw+XEUwE+cfgQcAnAWdhwwB8hFPgur7wB81ojDfZ7Mo/CLnDLj5XfaXzPjyrCtncprd843+ZFx1XrMcWjUrh7t0iU9fxP8YBNwuHcPo/4d9oJxPvn+kT3inn719qnaZ8h/g0lO58blCRxcFU+o+b62h9T2Who6NWVJArLufsDlcNcBvPF8H+CI8jsAh6BxuQEHHKZyIudggLGgEk1A9kNJ7IZdmYAE3iWaInUNyNB2NcEPdoczMxgp5GUIcD7A0ZY7/OQzI4elzwOdhEG9BjDQUXoPx/doApKFLuMYexHHv3fS6n4o6UniXGh6rXh+yT4vvlRCA7IJfrAFyJsNsPc7B/gLcCNwAnA0EMm3sXMyEAUjg3gYwHeO27T2Q2Lt6BAl2OsR6n+v5+iTnZ9hl6sQmiShjutywpm3okRTVki7muIHfCx7GrgC+CFwAXBA6zgKSK7GGQHw1Q/xVYDB+AzwV+AxgDdhBieFAf13gD1qNKLkxBivFSdFRcLXHPGHV+7PANznSZFRoXJZJnW2Rr1dLkKOjxO2V6vO3jGkPq4OV5wkSVP84HgQ8S4wAOAEGQNyNYCcLQ1QJgGvAhGPfOY8FeBED29cewPsZaPzvu2TOD8UmJuB2xBhlB/lUTwbaezSmya8QBrR9FpFlvNeQmFN8oOJ4OL7wH7Aha3tEthSuN0A2IEHEPrDaIATQew0OJKcBpCviQCDNi70A36V70GAiwjeBESyE7Td6ObFY5ddpJSlh+TFcPkIOT6pILLoGCH1cXWeSqhf0/xgX/DBx7GJQMTVTOz3A65ppTHYNgLWBBiYkR63vN4M1BEtcOjKnvMKgI96HMn0CE+EytEexYlIY2WaKByGaORTF0BjIDCPtidO6iGb5ge/A98cxp8OcCKP1+8uYDDAmxPlemAsMAbgYoAfAfcClMWBIcCXgNWAocBbwGnAucCnJDQgGdVf+FTOedPAHCs3VZIctx0v2kBpZ69duracTjeapvrBJJBM9AUeAr4IHABQGJyDWuAkzoYAh6D9gSeBO4CrAAqvCQObveMtQC8JDchxvXLOW4nwrCe9KUnagOQdtggJvbZuXTq1q+l+8DHI4iKAdYDDAPaIFwJx4aMCA5bvMI8EGIQXAzcBw4ETgSmAV0IvWj9Pbq5q/5YnPe+k1ZUFfB35WOcQ4TPD+QmKnRy3U9ZQzjvZCDm3cIiSR6dTu5rqB5uCp32AHQD2cJcCo4AJwF4Ae837gCnASwB7P440BgI7A3sDU4HxAH0rtWgnUuIPt1XafyeAMc6e8ZlC2q6zAmxnobKiom5sy2UdCm+aH8wBF5zkIr4BLAJEwuH7fwD3A3zL4PrBG0i7F/gpwMmeICnqbh1UmYop8QK8D8QvUkgTiuK8T0hlPDohNyNPtlomcZRxBnAuwOFqXO7EAfGDeGLa/QXSGmh4fk5/S0U7lCyqnLekBdVYn4sCzgbcYMytyRaQ6agVv8xFcWXvITVtSsei5e5hINQ5fo0ck3tydXdnfxS/maIKfDckmdQJKULjvFXuIc0PQryiYToXob3uA3TI8Zo58BSt1AgpP9LhDF0RsjkKicqUbDmbWAUpkx9kxpcNWdNRqXne6pOuyODcfB+mEU2vrynH8ngYsID0kCJI0jhvX4H9NKragNTcZNLU0/LGGLCAjJGh2C1zQEpfx0TN17QpymvblAxYQKYjcIYie9l7yNCJL0XTLUsSAxaQSQx1Ps9lUlIp6hlS00N+gMZwhYlJlxiwgExH/HRF9jL3kJobjIICy9KOAQvIdsyEpWsCUjvZElajT7Q0PaQF5Cf8dWXPAjId7RoH7peuyODc/GGsVDQ3GGkZpt+BAQvIDuQEnOJaR+l61qI+CKYJSM0NJoAmUwllwAIylKn2etJeZdH2pjI9owlIaVsyrbAZcz6wY4SoGHhRmIvrh/sI82jUNQH5nKYgy5MdA9ZDpufyGYWJInpJTUBq2qJovmVpx4AFZDtmwtOfDlft0SziObJ/T2nhOxaQ4VzlomkBmZ5WjRPzfz/kLUsLC+CqI1s2JyQta3ULyPSManrIIgJyGWHTNO0QFmHqSQxYQCYxlHz+71Dhx64kkndA8gNc0jI0Pb2kzaYbwIAFZABJCSr8MtnUBB33tDRY3PxJx0tAIfRrEJGtJ6Id23aPAQvIbLh/WGgm74CUPj+y+g8J22DqOTBgAZkNqQ8KzSwv1JeqD5RmgL4FpIK0rLNYQGbDqDQgB2VTbFsrUvv8DaQtm2tLZ3EnLCCz4VoakCtkU2xbKyu2PeM/Ia2/34qlpmbAAjI1hXMNcA2o5Jf20h5MWkupfRuuShnOSd8CMjti7xOYKlsPeaeg7qaaIwMWkNmRe7PAVF/oaiZeQotYNVQRevxM/m0CfVPNkQELyOzIvVFoag2hvkRdYpv/vWmmxLjp5seABWR23D4CU68LzEmCRmB2vuWgzIUBoSLp2UNtmp6SAQtIJXGebPxc/2RPerukvAJSavemdhW09OIZsIDMlvPrBebWEehKVCV2+QmS2yXGTTdfBiwgs+X3TzAX+r8EN8626B5rm/TsJe9cDZVO/8I82YJpZMqABWSmdM73KsyFPpPx5T2f97IWSUAW9Z+4sm5jbe1ZQGZ/aS8RmJQET4jZhaG0QYgidGYBkwJ1Ta0gBiwgsyf6Cpj8KNDsloF6oWobQZHvOEPkKijZcDWEqQJ1LCCzJ3sGTPJZMkRGhigJdCT2zhbYNdWCGLCAzIfonwWa5ZB16UDdELXQgORicludE8JowToWkPkQzrWh9wSYJv+hQZRkjj96Hpak1DofesMINGdqWTFgAZkVk73thDr9N3tnVaXsjVwhn+3gL1MkE0+qylgmHQNVDMhFdE0NnuxQmu+V7XKkPNortXfCdkhaqXeyOGVcYI5jofdhoG6Z1ariByIOqxiQ0t/6RYTk8c4vsu3bcoHAob4TThqvwYFOmvRwC2TgDGuSPACFiUlKFTlfFT+oCJ36ar6MrFw3KsU++iJT5bwsoK5cwpbm51i3BJRBvrJ+zQKTXZOq+UHXiMqz4C/DuDQQI32+H+yGrIxC3wKierTbnqes3O4BtllmnV5zVNEPlJe3vNnYgzwGtHPopHQ+N+W1fjSJtV2gwI8pJ9VxbJIh5/zqOH4TSLLLrxn0dfJW9bDKflBVzudbHjXnMjA+X60NHA5MA5IcL+k815qOBjgh0AeQfm4fWdTyn8iZVL/3ocMeL0QYjE8CSTbfgM4qIQZLqFNHPyghzclVuggqHwCzgSSH05yf07J/KrZFyYIoiIu5Q+p7JvQGtKkYe7r9gLeBJFscKg8Hqip19IOO1yLkvVVHAzmeZA9J5CHsHYsWzrruBbwDJL2iOAQ6BwHXAo8DHB3wxT+/lcPnJ+4nyWtQ2AHgqpwqS938oOO1KHNAdqx4RU8yKPcHXgGOAjq9duK1YfARUmEQfxXgkNakQgx0cogKNaNSVeUw8xhgK+CpjGvOX5nwWZWTVxaMGZNbhDkLyCJY9pdxB5I3BI4GXvCrBKdyBvkPwKbADwA+I5sYA8aAkgEOT/cE+LOt14GkyRqe56TXXcBxwEDApAYM8B97mpSLAV6TdQAOO/lahj/PWgrgah6+wiCeBe4B3gNMjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjwBgwBowBY8AYMAaMAWPAGDAGjAFjoCIM/D9fyyI/eAq7+gAAAABJRU5ErkJggg==" data-filename="404.png" style="width: 228px;"><br></p>', 1, 0, 1, 1, 0, '2018-06-01 14:48:12'),
(44, 10, 57, 2, 3, '<p>Peut-on changer la couleur de mot "utile" ?</p>', 0, 1, 1, 0, 1, '2018-06-01 14:51:15'),
(47, 7, 53, 1, 3, '<p>let\'s go</p>', 1, 0, 1, 1, 0, '2018-06-01 15:00:37'),
(48, 8, 53, 1, 3, '<p>Agence 007</p>', 1, 0, 1, 1, 0, '2018-06-01 15:00:53'),
(50, 8, 53, 1, 3, '<p>test 01</p>', 1, 0, 1, 1, 0, '2018-06-01 15:03:23'),
(51, 8, 53, 1, 3, '<p>test 02</p>', 1, 0, 1, 1, 0, '2018-06-01 15:04:15'),
(52, 8, 53, 1, 3, '<p>test 03</p>', 1, 0, 1, 1, 0, '2018-06-01 15:04:54'),
(54, 8, 53, 1, 3, '<p>test 04</p>', 1, 0, 1, 1, 0, '2018-06-01 15:42:19'),
(55, 8, 53, 1, 3, '<p>test 05</p>', 1, 0, 1, 1, 0, '2018-06-01 15:46:01'),
(56, 8, 53, 1, 3, '<p>test 06</p>', 1, 0, 1, 1, 0, '2018-06-01 15:49:30'),
(57, 11, 53, 1, 3, 'Mes remarque', 0, 0, 1, 1, 0, '2018-06-01 15:52:12'),
(58, 12, 53, 1, 3, 'azeaze', 0, 0, 1, 1, 0, '2018-06-01 15:52:48'),
(59, 8, 53, 1, 3, '<p>test 07</p>', 1, 0, 1, 1, 0, '2018-06-01 15:54:45'),
(60, 8, 53, 1, 3, '<p>test 08</p>', 1, 0, 1, 1, 0, '2018-06-01 15:56:13'),
(61, 8, 53, 1, 3, '<p>test</p>', 1, 0, 1, 1, 0, '2018-06-01 15:58:26'),
(62, 8, 53, 1, 3, '<p>test 09</p>', 1, 0, 1, 1, 0, '2018-06-01 16:01:26'),
(63, 8, 53, 1, 3, '<p>test 10</p>', 1, 0, 1, 1, 0, '2018-06-01 16:04:06'),
(64, 8, 53, 1, 3, '<p>test 11</p>', 1, 0, 1, 1, 0, '2018-06-01 16:08:53'),
(65, 8, 53, 1, 3, '<p>test 12</p>', 1, 0, 1, 1, 0, '2018-06-01 16:10:30'),
(66, 8, 53, 1, 3, '<p>test 13</p>', 1, 0, 1, 1, 0, '2018-06-01 16:12:14'),
(67, 8, 53, 1, 3, '<p>test 14</p>', 1, 0, 1, 1, 0, '2018-06-01 16:15:44'),
(68, 8, 53, 1, 3, '<p>test 15</p>', 1, 0, 1, 1, 0, '2018-06-01 16:16:25'),
(69, 8, 53, 1, 3, '<p>test 16</p>', 1, 0, 1, 1, 0, '2018-06-01 16:19:07'),
(70, 8, 53, 1, 3, '<p>test 17</p>', 1, 0, 1, 1, 0, '2018-06-01 16:22:36'),
(71, 8, 53, 1, 3, '<p>test 18</p>', 1, 0, 1, 1, 0, '2018-06-01 16:23:16'),
(72, 8, 53, 1, 3, '<p>test 19</p>', 1, 0, 1, 1, 0, '2018-06-01 16:25:00'),
(73, 8, 53, 1, 3, '<p>test 20</p>', 1, 0, 1, 1, 0, '2018-06-01 16:25:31'),
(74, 8, 53, 1, 3, '<p>test correct</p>', 1, 0, 1, 1, 0, '2018-06-01 16:27:18'),
(75, 8, 53, 1, 3, '<p>yes !</p>', 1, 0, 1, 1, 0, '2018-06-01 16:29:07'),
(76, 8, 53, 1, 3, '<p><b>N</b>o :/&nbsp;</p>', 1, 0, 1, 1, 0, '2018-06-01 16:31:32'),
(77, 8, 53, 1, 3, '<p>test 21</p>', 1, 1, 1, 1, 0, '2018-06-01 16:32:19'),
(78, 8, 53, 1, 3, '<p>test 22 it\'s&nbsp; okaaaaaay</p>', 1, 1, 1, 1, 0, '2018-06-01 16:32:35'),
(79, 11, 53, 1, 3, '<p>ok</p>', 1, 0, 1, 1, 0, '2018-06-01 16:39:25'),
(80, 11, 53, 1, 3, '<p>test</p>', 1, 0, 1, 1, 0, '2018-06-01 16:42:05'),
(81, 8, 53, 1, 3, '<p>test</p>', 1, 0, 1, 1, 0, '2018-06-01 16:44:49'),
(82, 8, 53, 1, 3, '<p>test</p>', 1, 0, 1, 1, 0, '2018-06-01 16:44:49'),
(83, 8, 53, 1, 3, '<p>okay</p>', 1, 0, 1, 1, 0, '2018-06-01 16:45:16'),
(84, 8, 53, 1, 3, '<p>okay</p>', 1, 0, 1, 1, 0, '2018-06-01 16:45:16'),
(85, 8, 53, 1, 3, '<p>okay</p>', 1, 0, 1, 1, 0, '2018-06-01 16:46:33'),
(86, 8, 53, 1, 3, '<p>okay</p>', 1, 0, 1, 1, 0, '2018-06-01 16:46:33'),
(87, 8, 53, 1, 3, '<p>adadad</p>', 1, 1, 1, 1, 0, '2018-06-01 16:47:45'),
(88, 8, 53, 1, 3, '<p>adadad</p>', 1, 0, 1, 1, 0, '2018-06-01 16:47:45'),
(89, 8, 48, 3, 1, 'test', 1, 0, 1, 0, 0, '2018-06-01 16:55:00'),
(90, 8, 48, 3, 1, 'test', 1, 0, 1, 0, 0, '2018-06-01 16:55:57'),
(91, 11, 53, 1, 3, '<p>test 01</p>', 1, 0, 1, 1, 0, '2018-06-01 16:58:03'),
(92, 11, 53, 1, 3, '<p>so okay</p>', 1, 0, 1, 1, 0, '2018-06-01 17:03:17'),
(93, 8, 53, 1, 3, '<p>latest message to test</p>', 1, 0, 1, 1, 0, '2018-06-01 17:03:59'),
(94, 8, 53, 1, 3, '<p><span style="color: rgb(255, 255, 255); background-color: rgb(115, 160, 202);">latest <b>message </b>to test</span><br></p>', 1, 0, 1, 1, 0, '2018-06-01 17:04:30'),
(95, 8, 53, 1, 3, '<p>so so so</p>', 1, 0, 1, 1, 0, '2018-06-01 17:06:40'),
(96, 11, 53, 1, 3, '<p>go go go</p>', 1, 0, 1, 1, 0, '2018-06-01 17:06:51'),
(97, 8, 53, 1, 3, '<p>nice and great</p>', 1, 0, 1, 1, 0, '2018-06-01 17:07:05'),
(98, 10, 57, 2, 3, '<p>re&nbsp;</p><p><br></p>', 0, 1, 1, 0, 1, '2018-06-01 17:40:55'),
(99, 10, 57, 2, 3, '<p>hello</p>', 0, 1, 1, 0, 1, '2018-06-03 21:56:47'),
(100, 5, 56, 1, 3, '<p>hello max <br></p>', 1, 1, 1, 1, 0, '2018-06-03 23:40:49'),
(101, 10, 57, 2, 3, '<p>Bonsoir</p>', 0, 1, 1, 0, 1, '2018-06-03 23:48:25');

-- --------------------------------------------------------

--
-- Structure de la table `objet_compaign`
--

CREATE TABLE `objet_compaign` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `objet_compaign`
--

INSERT INTO `objet_compaign` (`id`, `name`) VALUES
(9, 'Trafique ciblé'),
(10, 'acquisition client'),
(11, 'Epargne');

-- --------------------------------------------------------

--
-- Structure de la table `objet_operation`
--

CREATE TABLE `objet_operation` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `objet_operation`
--

INSERT INTO `objet_operation` (`id`, `name`) VALUES
(5, 'Test0106182');

-- --------------------------------------------------------

--
-- Structure de la table `operation`
--

CREATE TABLE `operation` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `caisse_id` int(11) DEFAULT NULL,
  `agence_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `objectif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cibleCom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cibleIlig` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modeTrans` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_vol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci NOT NULL,
  `permanant` tinyint(1) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `startDateOnline` date DEFAULT NULL,
  `endDateOnline` date DEFAULT NULL,
  `support` tinyint(1) NOT NULL,
  `active` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordred` tinyint(1) DEFAULT NULL,
  `creatAt` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `operation`
--

INSERT INTO `operation` (`id`, `bank_id`, `caisse_id`, `agence_id`, `campaign_id`, `name`, `objectif`, `cibleCom`, `cibleIlig`, `modeTrans`, `amount`, `object_vol`, `comment`, `permanant`, `startDate`, `endDate`, `startDateOnline`, `endDateOnline`, `support`, `active`, `ordred`, `creatAt`) VALUES
(5, 7, 32, 9, 14, 'Opération Binance Sud Ouest fidelité', NULL, 'carte gold', '- de 25 ans', NULL, '30', NULL, '-', 1, '2018-06-01', '2018-06-07', NULL, NULL, 0, '0', 1, '2018-06-01'),
(6, 7, 29, 9, 14, '50€ offert', 'Test0106182', 'test', 'test', 'mode_fichier', '50', '1000', 'test', 1, '2018-06-02', '2018-06-14', '2018-06-01', '2018-06-14', 0, '1', 0, '2018-06-01'),
(7, 7, 30, 9, 14, 'Opération 01-06-2018', NULL, 'Abonnement', 'critère 01', NULL, '500', NULL, 'aucune remarque', 1, '2018-06-02', '2018-06-29', NULL, NULL, 0, '4', 1, '2018-06-01'),
(8, 8, 30, 9, 15, 'opération 007', NULL, '007', '007', NULL, '500 €', NULL, 'sddf', 1, '2018-06-06', '2018-06-21', NULL, NULL, 0, '1', 1, '2018-06-01'),
(9, 8, 32, 9, 15, 'test', NULL, 'mastercard', '50€', NULL, '50€', NULL, 'test', 1, '2018-06-01', '2018-06-13', NULL, NULL, 0, '0', 1, '2018-06-01'),
(10, 9, 33, 12, 17, 'Jeunes Diplômés', 'Test0106182', 'jeune 16 à 25 ans', 'aucun', 'mode_fichier', '80€', '1500', 'non', 1, '2018-06-01', '2018-06-14', '2018-06-01', '2018-06-14', 0, '0', 0, '2018-06-01'),
(11, 9, 30, 9, 17, 'Jeune', NULL, 'Produit cible', 'Critére', NULL, '150', NULL, 'Mes remarque', 1, '2018-06-01', '2018-06-30', NULL, NULL, 0, '0', 1, '2018-06-01'),
(12, 7, 30, 9, 16, 'test operation', NULL, 'cible test', 'another test', NULL, 'azazeaze', NULL, 'azeaze', 1, '2018-06-01', '2018-06-30', NULL, NULL, 0, '0', 1, '2018-06-01');

-- --------------------------------------------------------

--
-- Structure de la table `visual_print`
--

CREATE TABLE `visual_print` (
  `id` int(11) NOT NULL,
  `caisse_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `operation_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `largeur` int(11) DEFAULT NULL,
  `hauteur` int(11) DEFAULT NULL,
  `unite` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `visual_print`
--

INSERT INTO `visual_print` (`id`, `caisse_id`, `bank_id`, `operation_id`, `campaign_id`, `type`, `largeur`, `hauteur`, `unite`, `path`) VALUES
(55, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 29, NULL, NULL, NULL, 'JPG', 1405, 2128, 'PX', NULL),
(57, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, NULL, NULL, NULL, 14, 'JPG', 669, 188, 'PX', '20180601_103645_Parrainage_full_header.jpg'),
(62, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL),
(64, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL, NULL),
(65, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL),
(66, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, NULL, NULL, NULL, 17, NULL, NULL, NULL, NULL, NULL),
(68, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL),
(70, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, NULL, NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `visual_web`
--

CREATE TABLE `visual_web` (
  `id` int(11) NOT NULL,
  `caisse_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `operation_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `largeur` int(11) DEFAULT NULL,
  `hauteur` int(11) DEFAULT NULL,
  `unite` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `visual_web`
--

INSERT INTO `visual_web` (`id`, `caisse_id`, `bank_id`, `operation_id`, `campaign_id`, `type`, `largeur`, `hauteur`, `unite`, `path`) VALUES
(53, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 29, NULL, NULL, NULL, 'JPG', 300, 250, 'PX', NULL),
(55, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, NULL, NULL, NULL, 14, 'JPG', 1665, 340, 'PX', '20180601_103645_bannière-parrainage-happ-e-parrainoo.png'),
(60, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL),
(62, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL, NULL),
(63, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL),
(64, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, NULL, NULL, NULL, 17, NULL, NULL, NULL, NULL, NULL),
(66, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL),
(68, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, NULL, NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `agence`
--
ALTER TABLE `agence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_64C19AA9E7A1254A` (`contact_id`);

--
-- Index pour la table `agence_collaborateur`
--
ALTER TABLE `agence_collaborateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_3D2300A9A76ED395` (`user_id`),
  ADD KEY `IDX_3D2300A9D725330D` (`agence_id`);

--
-- Index pour la table `agence_contact`
--
ALTER TABLE `agence_contact`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7096CE0CA76ED395` (`user_id`);

--
-- Index pour la table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D860BF7A7854071C` (`commercial_id`);

--
-- Index pour la table `caisse`
--
ALTER TABLE `caisse`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_B2A353C8E7A1254A` (`contact_id`),
  ADD KEY `IDX_B2A353C811C8FB41` (`bank_id`),
  ADD KEY `IDX_B2A353C8D725330D` (`agence_id`);

--
-- Index pour la table `caisse_collaborateur`
--
ALTER TABLE `caisse_collaborateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_950000CDA76ED395` (`user_id`),
  ADD KEY `IDX_950000CD27B4FEBF` (`caisse_id`);

--
-- Index pour la table `caisse_contact`
--
ALTER TABLE `caisse_contact`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_76DB90D9A76ED395` (`user_id`);

--
-- Index pour la table `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1F1512DD11C8FB41` (`bank_id`),
  ADD KEY `IDX_1F1512DDF520CF5A` (`objet_id`);

--
-- Index pour la table `commercial`
--
ALTER TABLE `commercial`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7653F3AEA76ED395` (`user_id`);

--
-- Index pour la table `connection_history`
--
ALTER TABLE `connection_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5CB09668A76ED395` (`user_id`);

--
-- Index pour la table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307F44AC3583` (`operation_id`),
  ADD KEY `IDX_B6BD307FA76ED395` (`user_id`);

--
-- Index pour la table `objet_compaign`
--
ALTER TABLE `objet_compaign`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `objet_operation`
--
ALTER TABLE `objet_operation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1981A66D11C8FB41` (`bank_id`),
  ADD KEY `IDX_1981A66D27B4FEBF` (`caisse_id`),
  ADD KEY `IDX_1981A66DD725330D` (`agence_id`),
  ADD KEY `IDX_1981A66DF639F774` (`campaign_id`);

--
-- Index pour la table `visual_print`
--
ALTER TABLE `visual_print`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_420A452A27B4FEBF` (`caisse_id`),
  ADD KEY `IDX_420A452A11C8FB41` (`bank_id`),
  ADD KEY `IDX_420A452A44AC3583` (`operation_id`),
  ADD KEY `IDX_420A452AF639F774` (`campaign_id`);

--
-- Index pour la table `visual_web`
--
ALTER TABLE `visual_web`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_921E7BAD27B4FEBF` (`caisse_id`),
  ADD KEY `IDX_921E7BAD11C8FB41` (`bank_id`),
  ADD KEY `IDX_921E7BAD44AC3583` (`operation_id`),
  ADD KEY `IDX_921E7BADF639F774` (`campaign_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `agence`
--
ALTER TABLE `agence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `agence_collaborateur`
--
ALTER TABLE `agence_collaborateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `agence_contact`
--
ALTER TABLE `agence_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `caisse`
--
ALTER TABLE `caisse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT pour la table `caisse_collaborateur`
--
ALTER TABLE `caisse_collaborateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `caisse_contact`
--
ALTER TABLE `caisse_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT pour la table `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `commercial`
--
ALTER TABLE `commercial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `connection_history`
--
ALTER TABLE `connection_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;
--
-- AUTO_INCREMENT pour la table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT pour la table `objet_compaign`
--
ALTER TABLE `objet_compaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `objet_operation`
--
ALTER TABLE `objet_operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `operation`
--
ALTER TABLE `operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `visual_print`
--
ALTER TABLE `visual_print`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT pour la table `visual_web`
--
ALTER TABLE `visual_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `agence`
--
ALTER TABLE `agence`
  ADD CONSTRAINT `FK_64C19AA9E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `agence_contact` (`id`);

--
-- Contraintes pour la table `agence_collaborateur`
--
ALTER TABLE `agence_collaborateur`
  ADD CONSTRAINT `FK_3D2300A9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_3D2300A9D725330D` FOREIGN KEY (`agence_id`) REFERENCES `agence` (`id`);

--
-- Contraintes pour la table `agence_contact`
--
ALTER TABLE `agence_contact`
  ADD CONSTRAINT `FK_7096CE0CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `bank`
--
ALTER TABLE `bank`
  ADD CONSTRAINT `FK_D860BF7A7854071C` FOREIGN KEY (`commercial_id`) REFERENCES `commercial` (`id`);

--
-- Contraintes pour la table `caisse`
--
ALTER TABLE `caisse`
  ADD CONSTRAINT `FK_B2A353C811C8FB41` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
  ADD CONSTRAINT `FK_B2A353C8D725330D` FOREIGN KEY (`agence_id`) REFERENCES `agence` (`id`),
  ADD CONSTRAINT `FK_B2A353C8E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `caisse_contact` (`id`);

--
-- Contraintes pour la table `caisse_collaborateur`
--
ALTER TABLE `caisse_collaborateur`
  ADD CONSTRAINT `FK_950000CD27B4FEBF` FOREIGN KEY (`caisse_id`) REFERENCES `caisse` (`id`),
  ADD CONSTRAINT `FK_950000CDA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `caisse_contact`
--
ALTER TABLE `caisse_contact`
  ADD CONSTRAINT `FK_76DB90D9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `campaign`
--
ALTER TABLE `campaign`
  ADD CONSTRAINT `FK_1F1512DD11C8FB41` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
  ADD CONSTRAINT `FK_1F1512DDF520CF5A` FOREIGN KEY (`objet_id`) REFERENCES `objet_compaign` (`id`);

--
-- Contraintes pour la table `commercial`
--
ALTER TABLE `commercial`
  ADD CONSTRAINT `FK_7653F3AEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `connection_history`
--
ALTER TABLE `connection_history`
  ADD CONSTRAINT `FK_5CB09668A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307F44AC3583` FOREIGN KEY (`operation_id`) REFERENCES `operation` (`id`),
  ADD CONSTRAINT `FK_B6BD307FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `operation`
--
ALTER TABLE `operation`
  ADD CONSTRAINT `FK_1981A66D11C8FB41` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
  ADD CONSTRAINT `FK_1981A66D27B4FEBF` FOREIGN KEY (`caisse_id`) REFERENCES `caisse` (`id`),
  ADD CONSTRAINT `FK_1981A66DD725330D` FOREIGN KEY (`agence_id`) REFERENCES `agence` (`id`),
  ADD CONSTRAINT `FK_1981A66DF639F774` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`);

--
-- Contraintes pour la table `visual_print`
--
ALTER TABLE `visual_print`
  ADD CONSTRAINT `FK_420A452A11C8FB41` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
  ADD CONSTRAINT `FK_420A452A27B4FEBF` FOREIGN KEY (`caisse_id`) REFERENCES `caisse` (`id`),
  ADD CONSTRAINT `FK_420A452A44AC3583` FOREIGN KEY (`operation_id`) REFERENCES `operation` (`id`),
  ADD CONSTRAINT `FK_420A452AF639F774` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `visual_web`
--
ALTER TABLE `visual_web`
  ADD CONSTRAINT `FK_921E7BAD11C8FB41` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_921E7BAD27B4FEBF` FOREIGN KEY (`caisse_id`) REFERENCES `caisse` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_921E7BAD44AC3583` FOREIGN KEY (`operation_id`) REFERENCES `operation` (`id`),
  ADD CONSTRAINT `FK_921E7BADF639F774` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
